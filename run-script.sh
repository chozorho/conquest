#!/usr/bin/env bash
#
cp -n client-Makefiles/Makefile Makefile
make init
./bin/static/release/game-loop && exit || echo "bin/static/rel failed, trying dyn libs"
LD_LIBRARY_PATH=./dyn-libs-for-static/ ./bin/static/release/game-loop && exit || echo "dyn libs failed, trying libstd static"
./bin/libstd-static/release/game-loop && exit || echo "libstd static by itself failed, trying dynamic libs with it"
LD_LIBRARY_PATH=./dyn-libs-for-static/ ./bin/libstd-static/release/game-loop && exit || echo "All four trials failed :("
echo "Contact the developer and say the run-script failed - give as much detail a possible about your OS and version num!"
echo "(Note to self: going to have to see which libraries are missing on this platform)"
