# Multi-Gen Game
## Version 0.0.4
### Documentation 0.0.4-d
### Branch "master"

Branch Notes
============

This is the product of the lengthy process of porting the MGG to SDL2.

Special thanks to contributors:
-   TrebledJ
-   mk
-   Candbot

Updates added in version 0.0.4 include:
-   animations for units walking in a given direction (toggle these on or off by pressing the 'a' key during a TMB);
-   resizable windows (currently, this is only supported on Linux and MacOS versions);
-   quicker evaluation of ``move range'' to provide better responsiveness during a TMB;
-   new creative-commons music, e.g. a victory theme, etc.;
-   a new UI design, courtesy of the new Contributor \textbf{Candbot} 님.


Terms & Definitions
===================

1.  MGG - Multi-Gen Game. An elaborate, customizable, single-player, 2D grand strategy game, currently written in C++ using the Allegro 5 game libraries. Remark: this is a *working title*. The origin of the name will be explained in detail in a future edition. For now, it suffices to say that the game will take place in a fictional world over a period of centuries (see IGT), i.e. multiple generations.

2.  IGT - In-Game Time. This refers to the passage of time in the *in-game* calendar system. In the game, there are 18 months per year and 10 days per month. (This is subject to change in future versions and can be easily adjusted by changing the DAYS\_PER\_MONTH and MONTHS\_PER\_YEAR values in src/game-loop.hpp). Dates are represented (e.g. in the upper-right corner of the WMSL) as yyy.mm.dd.

3.  WMSL - World-Map Strategy Layer. This is the “outer layer” of the game, the one that is shown any time a game is launched. It is a system in which the user can make strategic decisions about unit placement, respond to Events, and pause and unpause the game (using the space bar). While unpaused, the game “moves forward in time” (see IGT) at a certain rate, modeling the passage of time in a fictional universe.

4.  TMB - Tactical-Map Battle. This is a turn-based mini-game that is periodically launched by the WMSL at deterministic moments, or pseudo-randomly depending on context.

5.  Unit - a character in the game. A Unit will represent a member of the Army.

6.  UC - Unit Class. This refers to the *occupation* or *unit type* for a particular Unit. At the moment, the only examples are Archer and Soldier. This will be greatly expanded later.

7.  Stat - one of finitely-many attributes that *all player Units* possess, like Health Points, Strength, Speed, etc. These values serve the primary purpose of affecting a player unit's performance *during a TMB*. As of version 0.0.4, there are roughly 14 different stats.

8.  Trait - Whereas Stats are basic things like Health, Strength, etc. (things modified by the StatChange class), Traits are like "personality" traits that can be gained/discovered through Events (which, so far, have not been developed very much). This is similar in concept to Traits in other grand strategy games, in that Events will occur pseudorandomly at the WMSL when the game is unpaused. However, the idea that is more unique is that they may affect Stat Growths when a Unit levels up.

9.  Spawn Pool - This refers to a general region where retired units can be sent to train new units. As currently defined, this is a *player-owned* Province.

10.  Lead Developer - I’ve been using this term, and you may be wondering who it refers to. I often write documentation & emails in the third person (or occasionally the royal We), to sound more official. To be clear, I ([name redacted]) am the lead developer.


The WMSL
========

When the WMSL is unpaused, IGT will “tick” by, at an adjustable rate. To adjust this rate, try pressing the ‘+’ or ‘-’ keys when the game is *unpaused*. The number displayed in the upper-right corner approximately equals
10 \* (pace in ticks/sec).
 The maximum pace is 10 ticks per second, making 100 the maximum number displayed.<sup>[1]</sup>

The World Map is composed of **Provinces**, each of which contains **Settlements**. While the game is still active/running, some Provinces are player-owned, others are enemy-owned, and some may be contested (have player units and enemy units both present, both trying to conquer).There are three ways for the game to end:

-   The player conquers all provinces (prompting a Victory screen with a Score value).

-   All player units are debilitated (prompting a Defeat screen).

-   The player deliberately exits the game (“Cold exit”). A future release will auto-save the game state, but this has not been implemented.

**The WMSL also has music enabled by default. To pause or unpause the music, press the ‘m’ key**.

The WMSL, the foundation of the game is, like many other grand-strategy games, composed of different tabs (or, in the new UI, "menu headers" that on the left-hand panel).

‘Map’ Tab
---------

When this tab is selected, the world map is displayed. The user may click on a province to get data about that province (name, settlements, etc.). This is very rudimentary so far.

‘Army’ Tab
----------

All player units are displayed in a scrollable list under the Army tab. This is the heart of the game, where the user can choose to move units into other provinces.

When a new game initiates, the unit list is read from a file, namely ‘res/world-map-2.conf’. You may adjust this file by adding or deleting lines that contain unit names.

When the game is unpaused, clicking a unit will simply display data for that unit. However, when it is paused, the user may left-click to see data OR right-click in order to add the selected unit into a temporary “group” along with other selected units. Then, when the group has been set up, the user may click the *T* symbol in the lower-right corner in order to set a destination Province for that group. (When paused, the user can move units to different provinces instantaneously.) Note: as currently written, the game will land these units in the “Field” of the Province. There is no way to order units in the field to invade or inspect a specific Settlement yet, but this may be implemented later.

**The following paragraph describes how the game *currently* works. It is subject to change in future releases.**

As currently written, when a Province contains some player units and some enemy units, it becomes **contested**. While it is contested and the game is un-paused, TMBs will periodically (pseudo-randomly) initiate. When the player has won all of these TMBs, (s)he has conquered the Province.

‘Research’, ‘Homefront’ Tabs
----------------------------

(Requirements & design pending)

The TMB
=======

Terrain Tile Codes
------------------

Each TMB takes place on a map. Map data is read from files, each of which contains a space-delimited table of integers. Each integer (corresponding to a tile in the map) represents the **terrain type** of that tile. A key for these terrain type codes should be inside src/oo-tmb.cpp and is reproduced below:
0 = Ocean<sup>[2]</sup>
1 = Plains
2 = Forest
3 = Hills
4 = Ice
5 = Sand
6 = Mountains

Mechanics
---------

All TMBs are turn-based. The user may click the icon/sprite for their units<sup>[3]</sup>, at which moment the right-hand menu updates and gives you options for giving that unit orders. Go through these options carefully. When you’ve finished giving orders to each of your units, the turn updates (the enemy takes its turn, issuing orders to its units). Then it is the player turn again and the cycle continues.

Types of TMBs
-------------

There are different variants of TMBs, each with their own victory and defeat conditions (feel free to study them yourself). The most common (and the only one currently used in Alpha Testing) is the RoutTMB, in which the only way to win is to leave all opponent units debilitated.

Omega Mode
----------

Some open-source games have an Omega Mode that allows the player to have invincibility or special powers. This game offers a slightly different cheat: allowing the AI to play through a TMB *in place of* the user. This can be activated by pressing three keys in sequence: ‘g’, ‘o’, ‘d’. It can be *deactivated* by pressing the ‘m’ key.

Known Bugs
==========

**The game window is too big** - if you are running from inside a VM, you may have to adjust the resolution or make it full-screen in order to see the full game window.

**The logo/splash screen takes minutes to finish loading** - this bug appears to result from running the program inside a VM (the processor being emulated). To resolve it, try reducing the LOOP\_ITER value in src/game-loop.hpp. (You can safely experiment with this as long as the value is greater than 721.)

**After invading a province, I get a grey/empty/non-responsive screen** - believe it or not, this one is not a bug; it’s a feature. (:D) But in all seriousness, when this happens, you’re looking at a *new* window. The game expects you to make a choice, and after you choose to Fight, the TMB is added to the list of pending battles. Try clicking back to the original window and un-pausing in order to *jar* the system back into motion and load the battles.

**When pressing the ‘P’ button to invade a Province, the game crashes** - this is a serious error that only seems to happen on VMs. To minimize the risk of this error, try un-pausing the game for a few seconds *before* you pause and select your army. In fact, you may have to un-pause for long enough so that the first TMB (an automatic victory) is launched. If this still doesn’t work, then try scrolling up and down through the unit list before selecting (right-clicking) unit members. If you want to report this error, let the developer know and provide as much information as possible (OS and version, what graphics card you have, what libGL libraries you have, the call stack when running through GDB, etc.). In the unfortunate event that you cannot ever avoid this error, you may still compile the source and call ‘./bin/static/release/oo-tmb 0‘ to test a basic TMB. (Even if the WMSL doesn’t work, the TMB should be much more stable.)

Help Wanted
===========

The project could use help in a variety of areas, e.g.:

-   Tutorials;

-   Artwork (unit sprites, logo, etc.) and/or music;

-   A new map for the WMSL (only if you dislike the current one);

-   More maps for Tactical Map Battles;

-   Support for other operating systems;

-   Play testing and bug filing / fixing;

-   Code refactoring \& optimization;

-   Play testing and bug report filing;

Bounties
========

Bounties (for more serious endeavors) will be listed here. Note the following:
*   If you are the first to complete one of the active tasks, you are eligible to receive the bounty *and* to have your (user)name listed as a Major Contributor to the project.
*   Bounty prices will start low (maybe even at zero!) and rise over time.
*   Bounties will close the moment someone redeems them. If no one redeems them, their status will be renewed or closed on the next patch update.

-   B00000: Automatic boundary detection (Open, $0.00 + Major Contributor Status)
In the current framework, the "adjacent Province" system is hard-coded for each World Map. If a program were developed to process/analyze the world-map image and find adjacent provinces automatically, this would be most impressive and useful. If you can solve this problem, please contact the Lead Developer.
-   B00001: Windows package (Closed)
-   B00002: .deb package (Open, $0.00)
Release a version with proper Linux file paths. (Again, if you succeed, show the Lead Developer. The game needs to be at least as stable as the desktop version.)
-   B00003: Android Release (Open, $0.00 + Major Contributor Status)
Release a version of the game for Android, that runs just as smoothly as on desktop. Show the lead Developer exactly how you did it. The Lead Developer must replicate your steps successfully in order for you to redeem this bounty.
-   B00004: Solve TMB Responsiveness on Windows (Closed)
-   B00005: Teach the Lead Developer DirectFB (Open, $0.00 + Major Contributor Status)
Somebody teach the Lead Developer how to use the Linux framebuffer. Or, write better documentation on DirectFB. But no one will. He's just ranting in the form of an Issue.
-   B00006: Solve Multithreaded TMB Timer Failure (Closed)
-   B00007: Efficient Binary Deserializer (Open, $0.00 + Major Contributor Status)
Implement binary data processing, for serialization of the Unit data (and perhaps Province/Settlement data while you're at it). This is the long-term goal. There are intermediate goals, like (a) reading text data with stream-like objects; (b) using RegEx; (c) using Boost to serialize object data; or (d) some combination of the above. But the long-term goal would be to use an existing data parser, ideally for binary data that the client cannot easily hack / modify.
-   B00008: libpng warning: iCCP: CRC Error (Closed)


How to Compile (Platform-specific)
==================================
GNU/Linux
---------
In order to *compile* any recent edition of the game, you will need the SDL2 development libraries `SDL2`, `SDL2\_image`, `SDL2\_ttf`, and `SDL2\_mixer`) installed on your computer.
Then you may compile it using the Makefile or using CMake.

However, it should *not* be **necessary** to compile the MGG itself in order to test it. There is a run-script (conveniently called `run-script.sh`) that will try running the game executables in at least four different ways. Note, however, that if the game crashes, this script will simply try to run it again. Thus, *when using this script*, if the game spontaneously reloads and shows a title screen, that means that there was actually a crash - and it will be difficult to diagnose the error after the fact.

Windows
-------
The binary exectuable was generated using Visual C++ on Windows 10. It *should* run on any 64-bit Windows machine, so compilation should be optional. However, if you insist on doing so<sup>[6]</sup>}, try following the steps given here:\\
[https://www.wikihow.com/Set-Up-SDL-with-Visual-Studio](https://www.wikihow.com/Set-Up-SDL-with-Visual-Studio)


Mac
---
There is no **official** build for Mac; if you would like to volunteer to create one, contact the Lead Developer. Other contributors have confirmed that the code can compile using clang on MacOS (either from a terminal, or using XCode). However, the Lead Developer does not own a Mac machine. Should you have questions / complications compiling the code, ask the lead developer to put you in contact with one of the Mac OS experts.


Configuration
=============
**NOTE: The configuration for the game is in the process of being rewritten in JSON so that it can be parsed efficiently in a standard, human-readable way. Thus, the following information may or may not hold true for version 0.0.4.**
The main configuration file is named `res/world-map-3.conf`. It has the following format:
```
<millisecond time delay (int) - may now be obsolete>
(<number of provinces (int)>)
Province 0 (Optional description)
Name: <Province_Name> (Ownership_status) (Ownership_char)
Settlements:
```
Then follows a list of Settlements, each having the form
```
<Settlement Name>
<Ownership Status - 0 for Player, 1 for Enemy>
```
Then, nested inside each Settlement is the Unit data. Each Unit is listed on its own line, having the following format:
```
<Unit_Name> <UC_ID> (Team <TeamID - 0 for Player, 1 for Enemy>) <x> <y>
```
Finally at the end of the list of Units, there must be a
```
End_Unit
```
token. The proceeding line contains name of the next Settlement, or (at the end of the list of Settlements), an
```
End
```
token. A default configuration file for the *old world map* (i.e., version 0.0.3) is shown in an appendix for reference.

Credits
=======

Major Contributors
------------------
Special thanks to all Major Contributors. These include, in alphabetical order by username:
*    Candbot (UI feedback and design),
*    MK (SGDT logo, additional artwork),
*    MrMcApple (co-development of original Java demo, 2017),
*    Slpee (concept, original design/vision),
*    The Seagull (a new title for the game),
*    TrebledJ (co-development of C++ code, 2019 -- present)
*    Zunethia (primary testing, artwork)

Minor Contributors
------------------
Thanks also to all developers not affiliated with the project who have given brief advice along the way, including:
*    David Holmes
*    Ten

Music Credits
-------------
**Main WMSL Theme**
Lobo Loco, "Snowmelt Yukkon River"
Creative Commons by Attribution. No changes.

**Victory Theme**
XTakeRuX, "Destructing Own Kingdom"
Creative Commons by Attribution. No changes.

**'One Player Unit Remaining' Theme**
Meydän, "Pure Water"
Creative Commons by Attribution. No changes.




[1] You may wonder what the minimum pace is. The remarkable fact is: there *is* none - you should be able to set the speed arbitrarily slow, though we’re not sure why you’d want to.

[2] currently, no Units can directly cross bodies of water. There may be flying units in a future release.

[3] You may wonder, *how do I know which units belong to which side*? Your units have GREEN HP text and the enemy units have red HP text.

[6] and the Lead Developer has no clue why anyone would prefer to do this on Windows rather than Linux, but so be it.



Appendix A: Example Configuration File for Ver. 0.0.3
=========================================
```
5
(4)
Province 0 (Left)
Name: Mars (0) (P)
Settlements:
Field
0
End_Unit
Phobos
0
Kathleen 0 (Team 0) 3 4
Graham 0 (Team 0) 4 5
End_Unit
Deimos
0
Linus 0 (Team 0) 3 4
alexge50 0 (Team 0) 4 5
End_Unit
Olympus_Mons
0
Iron_Park 0 (Team 0) 3 4
Nalee 0 (Team 0) 4 5
Chau 0 (Team 0) 4 5
End_Unit
Valles_Marineris
0
Gold_Unit 0 (Team 0) 4 5
End_Unit
End
Province 1 (Top)
Name: Jupiter (2) (C)
Settlements:
Field
0
End_Unit
Io
0
Rockington 0 (Team 0) 1 2
Arin 0 (Team 0) 2 3
Wozar 0 (Team 0) 4 5
Deflated_Pickle 0 (Team 0) 3 4
End_Unit
Ganymede
1
Caribou 1 (Team 1) 2 3
End_Unit
Europa
1
End_Unit
Callisto
1
End_Unit
End
Province 2 (Bottom)
Name: Pluto (2) (C)
Settlements:
Field
0
End_Unit
Charon
1
End_Unit
Styx
0
Carol 0 (Team 0) 4 5
Laureen 0 (Team 0) 4 5
Rosemary 0 (Team 0) 4 5
Jiarui 0 (Team 0) 4 5
Sramika 0 (Team 0) 4 5
Flo 0 (Team 0) 4 5
Minji 0 (Team 0) 4 5
Alex 0 (Team 0) 4 5
Eric 0 (Team 0) 4 5
Sam 0 (Team 0) 4 5
End_Unit
Hydra
0
Kurt 0 (Team 0) 2 3
Krist 0 (Team 0) 3 4
Dave 0 (Team 0) 4 5
End_Unit
Nix
0
Ginger 0 (Team 0) 2 3
Jack 0 (Team 0) 4 5
End_Unit
End
Province 3 (Right)
Name: Saturn (1) (E)
Settlements:
Field
1
End_Unit
Titan
1
Robot 1 (Team 1) 3 4
End_Unit
Enceladus
0
End_Unit
Mimas
0
End_Unit
Iapetus
1
Bully 1 (Team 1) 1 2
Block_Dude 1 (Team 1) 3 4
Dinco 1 (Team 1) 4 4
End_Unit
End
```

