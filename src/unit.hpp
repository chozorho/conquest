#pragma once
#define DEFAULT_AGE 55
#define NUM_DIRECTIONS 4
#define STAT_MOD_GROWTH_RANGE 12
#define WEAPON_CAPACITY 5
#define BOW_CAPACITY 16

#include "animation.hpp"
#include "entity.hpp"
#include "global.hpp"
#include "item.hpp"
#include "weapon.hpp"

#include <algorithm>
#include <array>
#include <iostream>
#include <list>
#include <unordered_map>

enum DIRECTION {
  NORTH, EAST, SOUTH, WEST
};

enum Directive {
  ASSAULT, GUARD, SKIRMISH, ASSASSINATE, SEEK
};

enum UC {
  ARCHER, SOLDIER, THIEF, MAGE, KNIGHT, DUNCE
};

class Trait {
  public:
    Trait(StatChange *fullChange) {
      modifiers = fullChange;
    }
    Trait(int statNum, int delta) {
      std::array<int, NUM_STATS> copyChange = {0};
      copyChange[statNum] = delta;
      modifiers = new StatChange(copyChange);
    }
    ~Trait() {
      delete modifiers;
    }
  
  protected:
    StatChange *modifiers;
};

/* Middle Subclass: Unit. */
class Unit : public Entity {
  public:
    /* CONSTRUCTOR */
    Unit(UC role, int x, int y, const char *img,
        int maxHP_, int health_, int strength_, int magicalStrength_,
        int hitSpeed_, int speed_, int luck_, int dodge_, int armour_, int accuracy_,
        int constitution_, int initiative_, int team_, int magicalResistance_)
      : Entity(/*render_obj, */x, y, img) , team(team_) {
      stats[MAX_HP] = maxHP_;
      stats[HP] = health_;
      stats[STR] = strength_;
      stats[MAGIC_STR] = magicalStrength_;
      stats[HIT_SPEED] = hitSpeed_;
      stats[SPEED] = speed_;
      stats[LUCK] = luck_;
      stats[DODGE] = dodge_;
      stats[ARMOUR] = armour_;
      stats[ACC] = accuracy_;
      stats[CONS] = constitution_;
      stats[INIT] = initiative_ % 19;
      stats[MAGIC_RES] = magicalResistance_;
      stats[SKILL] = 0;
      
      /* Unit details */
      occupation = role;
      level = 1;
      exp = 0;
      next_lv_milestone = EXP_NEEDED[level-1];
      age = DEFAULT_AGE;
      prov_id = -1;

      weapons.fill(nullptr);
      std::stringstream nameSetup/*= new std::stringstream()*/;
      nameSetup << "Unit(Team " << team << "):HP " << stats[HP] << ";SPD "
                << stats[SPEED] << ";ACC " << stats[ACC];
      nameSetup.flush();
      name = nameSetup.str();
      /*            name = "Unit(Team "+team+"):HP "+stats[SPEED]+";SPD "+stats[SPEED]+";ACC "+accuracy;*/
      /*            boss = false;*/
      canUseAbility = true;
      current_loc = ""; /*nullptr*/
      currentDir = EAST;
    }

    Unit(UC role, int x, int y, const char *img,
        int maxHP_, int health_, int strength_, int magicalStrength_,
        int hitSpeed_, int speed_, int luck_, int dodge_, int armour_, int accuracy_,
        int constitution_, int initiative_, int team_, int magicalResistance_,
        Weapon *primaryWeapon)
      : Unit(role, x, y, img, maxHP_, health_, strength_, magicalStrength_,
             hitSpeed_, speed_, luck_, dodge_, armour_, accuracy_,
             constitution_, initiative_, team_, magicalResistance_) {
      /* one weapon */
      weapons[0] = primaryWeapon;
    }

    Unit(UC role, int x, int y, const char *img,
         int maxHP_, int health_, int strength_, int magicalStrength_,
         int hitSpeed_, int speed_, int luck_, int dodge_, int armour_, int accuracy_,
         int constitution_, int initiative_, int team_, int magicalResistance_,
         const std::vector<Weapon *> &initWeapons)
      : Unit(role, x, y, img, maxHP_, health_, strength_, magicalStrength_,
             hitSpeed_, speed_, luck_, dodge_, armour_, accuracy_,
             constitution_, initiative_, team_, magicalResistance_) {
      /* weapons */
      for (size_t i = 0; i < WEAPON_CAPACITY && i < initWeapons.size(); i++)
        weapons[i] = initWeapons[i];
    }
    
    /* A more concise, user-friendly constructor,
     * which sets base stats as a function of the 
     * UC. */
    Unit(UC job, int x, int y, int team_, const char *legal_name)
      :   Entity(x, y, nullptr) {
      occupation = job;
      level = 1;
      exp = 0;
      next_lv_milestone = EXP_NEEDED[level-1];
      age = DEFAULT_AGE;
      prov_id = -1;

      weapons.fill(nullptr);
      std::stringstream nameSetup/*= new std::stringstream()*/;
      nameSetup << "Unit(Team " << team << "):HP " << stats[HP] << ";SPD " << stats[SPEED] << ";ACC " << stats[ACC];
      nameSetup.flush();
      name = nameSetup.str();
      /*            name = "Unit(Team "+team+"):HP "+stats[HP]+";SPD "+stats[SPEED]+";ACC "+accuracy;*/
      /*            boss = false;*/
      canUseAbility = true;
      current_loc = ""; /*nullptr*/
      switch (job) {
        case ARCHER:
          {
            stats[MAX_HP] = 10;
            stats[HP] = 10;
            stats[STR] = 5;
            stats[MAGIC_STR] = 3;
            stats[HIT_SPEED] = 9;
            stats[SPEED] = 6;
            stats[LUCK] = 5;
            stats[DODGE] = 10;
            stats[ARMOUR] = 0;
            stats[ACC] = 10;
            stats[CONS] = 4;
            stats[INIT] = generator() % 19;
            stats[MAGIC_RES] = 5;
            stats[SKILL] = 0;
            
            team = team_;
            
            /* Weapon processing */
            Bow *bow = new Bow(80, 3, 5, /*Armour Piercing*/ 2,
                               /*Capacity*/BOW_CAPACITY, 3); 
            weapons[0] = bow;
  /*          bow = new Bow(80,5,/ *3,* /new Quiver(2,3), 3);*/
  /*          innerArrows = new Quiver(2, 13);*/
  
            /* Name processing */
            std::stringstream nameFormation;
            nameFormation << legal_name << " - Archer" /*<< name.substr(4)*/;
            nameFormation.flush();
            name = nameFormation.str();
            currentDir = EAST;
            break;
          }
        case SOLDIER:
          {
            stats[MAX_HP] = 10;
            stats[HP] = 10;
            stats[STR] = 5;
            stats[MAGIC_STR] = 1;
            stats[HIT_SPEED] = 10;
            stats[SPEED] = 6;
            stats[LUCK] = 4;
            stats[DODGE] = 4;
            stats[ARMOUR] = 3;
            stats[ACC] = 1;
            stats[CONS] = 8;
            stats[INIT] = generator() % 19;
            stats[MAGIC_RES] = 3;
            stats[SKILL] = 0;
            
            team = team_;
  
            /* Weapon processing */
            Sword *sword = new Sword(4,8,7);    
            weapons[0] = sword;
            
            /* Name processing */
            std::stringstream nameFormation;
            nameFormation << legal_name << " - Soldier" /*<< name.substr(4)*/;
            nameFormation.flush();
            name = nameFormation.str();
            currentDir = EAST;
            break;
          }
        default:
          {
            break;
          }
      }
    }

    virtual ~Unit() {
      for (size_t delete_ind=0; delete_ind < inventory.size(); delete_ind++) {
        delete inventory[delete_ind];
      }
      for (size_t delete_ind=0; delete_ind < WEAPON_CAPACITY; delete_ind++) {
        if (weapons[delete_ind] != nullptr) {
          delete weapons[delete_ind];
        }
      }
    }

    const std::vector<Trait *>& getTraits() const {
      return traits;
    }

    void addTrait(Trait *next_t) {
      traits.push_back(next_t);
    }

    bool getCanAbility() const {
      return canUseAbility;
    }
    void setCanUseAbility(bool nState) {
      canUseAbility = nState;
    }

    UC getUC() {
      return occupation;
    }
    void setUC(UC nOcc) {
      occupation = nOcc;
    }
    
    const std::vector<Item*>& getInventory() const {
      return inventory;
    }
    bool hasItem() const {
      return (inventory.size() > 0);
    }
    bool addItem(Item *ele) {
      if (inventory.size() < MAX_ITEM_LIMIT) {
        inventory.push_back(ele);
        return true;
      } else {
        return false;
      }
    }

    void deleteItem(size_t index) {
      inventory.erase(inventory.begin() + index);
    }
    
    int getSettlId() const {
      return settlement;
    }
    void setSettlId(int nextS) {
      settlement = nextS;
    }

    /* Public accessor methods */
    unsigned int getProvId() const {
      return prov_id;
    }
    void setProvId(int prov) {
      prov_id = prov;
    }
    int getConstitution() const {
      return stats[CONS];
    }
    int getTeam() const {
      return team;
    }
    int getHealth() const {
      return stats[HP];
    }
    void checkUp() {
      if (stats[HP] <= 0) {
        up = false;
      }
    }
    void addHealth (int delta) {
      if (stats[HP]+delta < stats[MAX_HP]) {
        stats[HP] += delta;
      } else {
        stats[HP] = stats[MAX_HP];
      }
    }
    int getMaxHealth() const {
      return stats[MAX_HP];
    }
    void addMaxHealth(int delta) {
      stats[MAX_HP] += delta;
    }
    bool isUp() const {
      return up;
    }
    void die() {
      up = false;
    }
    bool isChosen() const {
      return chosen;
    }
    void flipChosen() {
      chosen = !chosen;
    }
    int getInit() const {
      return stats[INIT];
    }
    int getSpeed() const {
      return stats[SPEED];
    }
    void addSpeed(int change) {
      stats[SPEED] += change;
    }
    int getArmour() const {
      return stats[ARMOUR];
    }
    void addArmour(int change) {
      stats[ARMOUR] += change;
    }
    int getDodge() const {
      return stats[DODGE];
    }
    int getAgility() const {
      return stats[DODGE];
    }
    void addAgility(int change) {
      stats[DODGE] += change;
    }
    int getMagicalStr() const {
      return stats[MAGIC_STR];
    }
    void addMagicalStrength(int change) {
      stats[MAGIC_STR] += change;
    }
    int getMagicalRes() const {
      return stats[MAGIC_RES];
    }
    void addMagicalResistance(int change) {
      stats[MAGIC_RES] += change;
    }
    int getAccuracy() const {
      return stats[ACC];
    }
    int getLuck() const {
      return stats[LUCK];
    }
    int getAge() const {
      return age;
    }
    void setAge(int givenAge) {
      age = givenAge;
    }
    int getLevel() const {
      return level;
    }
    const std::string &getLoc() const {
      return current_loc;
    }
    int getStrength() const {
      return stats[STR];
    }
    int getSkill() const {
      return stats[SKILL];
    }
    Unit *getHeld() const {
      return held;
    }
    void setHeld(Unit *otherUnit) {
      held = otherUnit;
    }
    Unit *getPartner() const {
      return partner;
    }
    void setPartner(Unit *p) {
      partner = p;
    }
    const std::array<Weapon*, WEAPON_CAPACITY>& getWeapons() const {
      return weapons;
    }
    int getDenomExp() const {
      return next_lv_milestone;
    }
/*    bool is_boss() {
      return boss;
    }*/

    virtual std::string toString() const {
/*      std::stringstream nameSetup;
      nameSetup << "Unit(Team " << team << "):HP " << stats[HP] << ";SPD " << stats[SPEED] << ";ACC " << accuracy << "@" << getX() << "," << getY();
      nameSetup.flush();
      return nameSetup.str();*/
      return name;
    }

    const std::string& getName() const { // is this ever used...? -V
      return name;
    }

    /* public modifier methods */
    void setLoc(const std::string &prov) {
/*      if (current_loc != nullptr) {
        delete current_loc;
      }*/
      current_loc = /*new std::string(*/prov/*)*/;
/*      current_loc = prov;*/
    }
    
    // stat changes:
    // all of these values should be inputed as either a plus of minus to change it up or down
    void addAccuracy(int change) {
      stats[ACC] += change;
    }
    void addAge(int change) {
      age += change;
    }
    void addConstitution(int change) {
      stats[CONS] += change;
    }
    void addDodge(int change) {
      stats[DODGE] += change;
    }

    int getExp() const {
      return exp;
    }
    void addExp(int change) {
      exp += change;
    }
    void addHitSpeed(int change) {
      stats[HIT_SPEED] += change;
    }
    void addInitiative(int change) {
      stats[INIT] += change;    
    }
    void addLuck(int change) {
      stats[LUCK] += change;
    }
    void addSkill(int change) {
      stats[SKILL] += change;
    }
    void addStrength(int change) {
      stats[STR] += change;
    }
    /*public boolean hasItem() {
      return (inventory.size() > 0);
    }
    public void addItem(Item ele) {
      inventory.addLast(ele);
    }*/
    int alter(Unit *target, StatChange *change) {
      std::array<int, NUM_STATS> wrappedChange;
      if (target == nullptr) {
        std::string change_str = change->toString();
        std::cout << "Stat change being applied is: " << change_str << std::endl;
        wrappedChange = change->arrRep();
        addHealth(wrappedChange[HP]);
        addStrength(wrappedChange[STR]);
        addSpeed(wrappedChange[SPEED]);
        addAgility(wrappedChange[DODGE]);
        addArmour(wrappedChange[ARMOUR]);
        addAccuracy(wrappedChange[ACC]);
        addInitiative(wrappedChange[INIT]);
/*        team = wrappedChange[];*/
        addLuck(wrappedChange[LUCK]);
        addConstitution(wrappedChange[CONS]);
        addMagicalStrength(wrappedChange[MAGIC_STR]);
        addHitSpeed(wrappedChange[HIT_SPEED]);
        addMagicalResistance(wrappedChange[MAGIC_RES]);
      } else {

      }
      return 0;
    }

    int levelUp() {
      int gained = 0;
      unsigned int divisor = 4;
      while (exp >= next_lv_milestone) {
        exp -= next_lv_milestone;
        next_lv_milestone = EXP_NEEDED[level];
        level ++;
        gained ++;
        divisor *= 7;
        divisor /= 5;
        
        /* Process this next level-up via stat changes */
        if (generator() % 2 == 0) {
          int a = (generator() % divisor);
          /*                    std::cout << "\tHealth altered by +" << (1+a) << std::endl;*/
          addMaxHealth(1 + a);
        }
        if (generator() % 2 == 0) {
          int s = (generator() % divisor);
          /*                    std::cout << "\tStrength altered by +" << (1+s) << std::endl;*/
          addStrength(1 + s);
        }
        if (generator() % 2 == 0) {
          int sp = (generator() % divisor);
          /*                    std::cout << "\tSpeed altered by +" << (1+sp) << std::endl;*/
          addSpeed(1 + sp);
        }
        if (generator() % 2 == 0) {
          int ag = (generator() % divisor);
          /*                    std::cout << "\tAgility (dodge?) altered by +" << (1+ag) << std::endl;*/
          addAgility(1 + ag);
        }
        if (generator() % 2 == 0) {
          int ag = (generator() % divisor);
          /*                    std::cout << "\tArmour altered by +" << (1+ag) << std::endl;*/
          addArmour(1 + ag);
        }
        if (generator() % 2 == 0) {
          int ac = (generator() % divisor);
          /*                    std::cout << "\tAccuracy altered by +" << (1+ac) << std::endl;*/
          addAccuracy(1 + ac);
        }
        /*                if (generator() % 6 == 0) {
                          addInitiative();
                          }*/
        if (generator() % 2 == 0) {
          int lu = (generator() % divisor);
          /*                    std::cout << "\tLuck altered by +" << (1+lu) << std::endl;*/
          addLuck(1 + lu);
        }
        if (generator() % 2 == 0) {
          int cc = (generator() % divisor);
          /*                    std::cout << "\tConstitution altered by +" << (1+cc) << std::endl;*/
          addConstitution(1 + cc);
        }
        if (generator() % 2 == 0) {
          int con = (generator() % divisor);
          /*                    std::cout << "\tMagical Strength altered by +" << (1+con) << std::endl;*/
          addMagicalStrength(1 + con/*(generator() % divisor)*/);
        }
        if (generator() % 2 == 0) {
          int hs = (generator() % divisor);
          /*                    std::cout << "\tHitSpeed altered by +" << (1+hs) << std::endl;*/
          addHitSpeed(1 + hs/*(generator() % divisor)*/);
        }
        if (generator() % 2 == 0) {
          int mr = (generator() % divisor);
          /*                    std::cout << "\tMagical Res. altered by +" << (1+mr) << std::endl;*/
          addMagicalResistance(1 + mr/*(generator() % divisor)*/);
        }
      }
      addHealth(stats[MAX_HP]);
      return gained;
    }

    /* Basic Combat Interactions */
    void move(int x,int y) {
      setX(x);
      setY(y);
    }

    void magical_attack(Unit *target, int spellPower, int modifiersOffensive,
                        int modifiersDefensive, int critModifiers) {
      target->dealDamage(spellPower + modifiersOffensive + stats[MAGIC_STR]
          - target->getMagicalRes() - modifiersDefensive,
          this, critModifiers, stats[MAGIC_STR], spellPower);
    }

    void dealDamage(int d, Unit *attacker, int strength, int weaponStrength,
                    int critModifier) {
      int adjustedDamage = (d*2 + ((2*(weaponStrength+strength)))/5);
      if (applyCritical(attacker, critModifier)) {
        stats[HP] -= adjustedDamage;
        std::string attacker_n = attacker->toString();
        std::string defender_n = this->toString();
#ifdef DEBUG_MSG
        std::cout << "[CRITICAL HIT] " << attacker_n <<
          " just hit " << defender_n << " for " <<
          adjustedDamage << " damage" << std::endl;
        /*                std::cout << "[CRITICAL HIT] "+attacker.toString()+" just hit "
                          +this->toString()+" for %d damage\n", adjustedDamage);*/
#endif
//        delete attacker_n;
//        delete defender_n;
      } else {
        stats[HP] -= d;
        std::string attacker_n = attacker->toString();
        std::string defender_n = this->toString();
#ifdef DEBUG_MSG
        std::cout << "[ORDINARY HIT] " << attacker_n <<
          " just hit " << defender_n << " for " << d <<
          " damage" << std::endl;
#endif
        /*                std::cout << "[ORDINARY HIT] "+attacker->toString()+
                          " just hit "+this->toString()+" for %d damage\n", d);*/
      }
      checkUp();
    }

    Unit *getCopy();

    /* Precondition: fedWeapon is a fully constructed non-nullptr Weapon object
     * Postcondition: adds fedWeapon to this character's list of objects in possession
     * If sudo is true and this character already owns a complete set of weapons,
     *  then one weapon will be automatically, siently replaced.
     * TODO: give the client an option of replacing an item of his/her choice
     * (perhaps send another parameter to this function specifying what location
     * should be replaced ?)
     * returns true if the operation was successful, false if the weapon set 
     * is already at max capacity. */
    bool setWeapon(Weapon *fedWeapon, bool sudo) {
      /* Finding the first nullptr slot in the array... hmm */
      auto it = std::find(weapons.begin(), weapons.end(), nullptr);
      if (it != weapons.end()) {
        weapons[it - weapons.begin()] = fedWeapon;
        return true;
      } else if (sudo) {
        weapons[0] = fedWeapon;
        return true;
      } else {
        return false;
      }
    }

    /* Warning: this method may overwrite any existing weapons that the Unit object owns...
     * So use with caution.
     * I have renamed it to 'setINITWeapons' so that hopefully it is only used
     * at the initialization, when a Unit spawns. */
    void setInitWeapons(const std::vector<Weapon *> &weaponFeed) {
      for (size_t i = 0; i < WEAPON_CAPACITY; i++) {
        weapons[i] = weaponFeed[i];
      }
    }

//    void setAnimationFrame(DIRECTION dir, SDL_Texture *sprite) {
//      walkAnims[dir].addFrame(sprite);
//    }

    /* This setter method is probably going to be easier to use,
     * since it wraps all the SDL library calls that reserver memory: */
    /* EDIT: Nope! This approach is ERRONEOUS because it creates 
     *  the SDL_Textures with one Renderer and then later tries to 
     *  RENDER them with a different Renderer :O
     *  Totally illegal. So... replace it. With some other approach.
     * This raises the question: *what* replacement approach?
     * Clearly, unless we pull some crazy stunt (wherein the TMB Renderer
     *  is allocated WELL before it is actually needed - which sounds
     *  like weird sh*t, i.e. a poor design choice, to me),
     * The other option would be to store ONLY THE filename
     *  at the start, when loading map data,
     * and then dynamically allocate Textures properly when launching 
     *  the TMB. */
    void setAnimationFrame(DIRECTION dir, const std::string &sprite/*,
                           SDL_Renderer *dis*/) {
      walkAnims[dir].addFrame(/*dis, */sprite);
    }

    void actualizeAllWalkAnims(SDL_Renderer *renderer) {
      walkAnims[NORTH].actualizeAll(renderer);
      walkAnims[EAST].actualizeAll(renderer);
      walkAnims[SOUTH].actualizeAll(renderer);
      walkAnims[WEST].actualizeAll(renderer);
    }

    void deActualizeWalkAnims() {
      walkAnims[NORTH].deActualizeAll();
      walkAnims[EAST].deActualizeAll();
      walkAnims[SOUTH].deActualizeAll();
      walkAnims[WEST].deActualizeAll();
    }
    
    /* A method call that sets the current direction */
    void setDir(DIRECTION nextDir) {
      currentDir = nextDir;
      cursor = walkAnims[currentDir].getHead();
    }

    SDL_Texture *advanceAnim() {
      cursor = cursor->getNextFrame();
      return cursor->getSprite();
    }

    DIRECTION getDir() {
      return currentDir;
    }

    /* Update: this is now an OVERRIDDEN method.
     * So these checks will take place in subclasses.
     * JULY 2019 REFACTOR: Because UCs can change dynamically
     * at runtime, we make UC a private instance var
     * and therefore eliminate polymorphism here. */
    /* Key for attack() return values:
     *  -1: tried to attack a unit on the same team
     *  -2: Quiver out of arrows
     *  -3: ability not available, already used
     *  -4: DODGED */
//    virtual int attack(Unit *target, Weapon *weapon, int terrainTile) = 0;

//    virtual int attack_must_hit(Unit *target, Weapon *weapon
//       /*, int terrainTile*/) = 0;

    /* Each Unit class should have some kind of cool ability to use
     * during a TMB */
//    virtual int use_ability() = 0;

    //For the statuses it will simply add the status string to a string arrayList and then at lower levels in conjunction with action listener, block actions or add modifiers
    //        void status(Unit *target, std::string status) {
    //            if (!checkExists(statuses, &status)) {
    //                target->statuses.push_back(status);    
    //            }
    //        }
    //        
    //        //I may think about creating a status class and creating a HashMap for this, but that would come later and not be hard to quickly implement
    //        bool checkExists(std::vector<std::string> in, std::string *input) {
    //            unsigned int i;
    //            for(i = 0; i < in.size(); i++) {
    //                if (input->compare(in.at(i)) == 0) {
    //                    return true;
    //                }
    //            }
    //            return false;
    //        }
    //        
    //        std::string removeElem(std::vector<std::string> in, std::string input) {
    //            unsigned int i;
    //            for(i = 0; i < in.size(); i++) {
    //                if (input == (in[i])) {
    ///*                    in.remove(i);*/
    //                    in.erase(in.begin()+i);
    //                    return input;
    //                }
    //            }
    //            return nullptr;
    //        }
    
    /* Precondition: The stats given are valid (i.e., positive integers).
     * Postcondition: will return true iff. a weapon attack actually hits
     * (and will return false otherwise), based on the dodge stats, modifiers, etc.
     * TODO make this static? Just an idea. */
    int accuracyTest(Unit *target, int accuracy, int terrain, int additions) {
      /*            if ((double)actualDistance / ((double)accuracy + additions) <= (generator() % 13)) {
       bool check = ((int)(generator()%11) + 1 >= dodge);
       return check;
       }*/
      int actualDist = Entity::get_distance(getX(), getY(), target->getX(), target->getY());
      int scalar = 1;
      double prob = (scalar*accuracy)+getLuck() + getSkill()*scalar*scalar + getLuck() + additions;
      prob /= actualDist;
      /*            std::cout << " THIS Unit stats:" << std::endl;
       std::cout << "  scalar: " << scalar << std::endl;
       std::cout << "  accuracy: " << accuracy << std::endl;
       std::cout << "  luck: " << getLuck() << std::endl;
       std::cout << "  skill: " << getSkill() << std::endl;*/
      int rando = ((generator()*generator()*generator()) % 100);
      std::cout << " Hit chance computed as " << prob << " (comparing to " << rando << ")" << std::endl;
      if (prob >= rando) {
        int last_p = ((scalar*(target->getDodge())) +
                      EVASION_BONUS[terrain]);
        rando = (generator()*generator()*generator()) % 100;
        /*                std::cout << " TARGET Unit stats:" << std::endl;
         std::cout << "  dodge " << target->getDodge() << std::endl;
         std::cout << "  Dodge chance computed as " << last_p << " (comparing to " << rando << ")" << std::endl;*/
        if (last_p >= rando) {
          return -5;
        } else {
          return 0;
        }
      } else {
        return -6;
      }
    }

    /* This checks if the target can dodge
     * if the target's dodge is over three times the attackers speed this
     * will always miss; the more that the attackers speed is above the target's
     * dodge value the more it will succeed */
    static bool canDodge(Unit *attacker, Unit *target) {
      return double(target->getDodge())/double(attacker->getSpeed()) <= generator() % 3;
    }

    /* TODO obviously make this more robust in a future release...
     * just because two Units have the same first name
     * won't always mean they're actually the same unit. */
    bool operator ==(Unit &other) const {
      return (name == other.getName());
    }
  
  protected:
    Directive order;
    std::string name;
    DIRECTION currentDir;

    CircularList walkAnims[NUM_DIRECTIONS];
    /* A structure to hold all the stats.
     * TODO consider changing to an array;
     * after all, the number of stats a unit has is 
     * a constant known a priori. And Enums 
     * can be casted to integer indices, so... why not an array? */
    int stats[NUM_STATS];
/*    std::unordered_map<STAT, int> stats;*/
    int team;             // 0 for friendly, 1 for enemy. -1 for "no team"?
    UC occupation;
    std::vector<std::string> statuses;
    std::vector<Item *> inventory;
    std::vector<Trait *> traits;
    std::array<Weapon*, WEAPON_CAPACITY> weapons;
  
/*    bool boss;*/
    bool canUseAbility;
    
    /* New attributes, most of which may change at any time.
     * Still, these special attributes are to be displayTMBed in WMSL
     * very quickly / frequently. */
    unsigned int level, exp, next_lv_milestone, age, prov_id;
    std::string *unit_class;
    
    /* TODO find a better way of storing location names,
     * without using up so much heap space...
     * Since there are only finitely many (CONSTANT number of)
     * distinct settlement names, maybe store them in some separate class...? */
    std::string current_loc;
    int settlement;
    
    /* LEFTOVER NOTICE: */
    /* As for these bool vars, 
     * I imagine we should  either make melee/ranged
     * a field or property of the Weapon class.
     * However, if these variables are supposed to 
     * represent whether a single Unit is *capable* of
     * holding a melee weapon, then they can 
     * stay members of this class. */
    bool meleeWeapon, rangedWeapon/* = false*/;
    
  private:
    double k1 = .5; // temporary value - should eventually be optimized
    double k2 = .5; // temporary value
    bool up = true;
    bool chosen = false;
    Unit *held = nullptr;
    Unit *partner = nullptr;
    Frame *cursor;
    bool applyCritical(Unit *attacker, int critModifier) {
      double asPercentage = double((k1*attacker->getLuck()) + (critModifier-k2*this->getLuck()));
      unsigned int luckValue = (unsigned int) (asPercentage / 100.0);
      return luckValue > /*pdf*/(generator());
    }
};

namespace std
{
  template <>
  struct hash<Unit *> {
    size_t operator()(const Unit *u) const {
      /* TODO think of a good way to hash
       * 2d array elements without knowing width
       * a priori.
       * Hmm. This might suck. Maybe come up with a clever
       * XOR scheme that minimizes collisions? */
       return (u->getX() * (2 << 4)) + (u->getY());
    }
  };
};

/* A (temporary) collection of methods that used to be 
 * STATIC methods in JAVA.
 * These should be placed somewhere so that they are accessible
 * GLOBALLY, without need for a class to be instantiated.
 * These have been moved around, e.g. from non-static protected methods in Unit...
 * may want to make them static in the future, we'll see. */
//class TMBManager {
//    public:
//        /* Transferred from Bow class; now a static method.
//         * Precondition: The stats given are valid (i.e. positive integers).
//         * Postcondition: will return true iff. a weapon attack actually hits
//         * (and will return false otherwise), based on the dodge stats, modifiers, etc.
//         * */
//        bool accuracyTest(int range, int dodge, int accuracy, int additions) {
//            if ((double)range / ((double)accuracy + additions) <= pdf(generator)) {
//                bool check = ((int)(pdf(generator)*11) + 1 >= dodge);
//                return check;
//            }
//            return false;
//        }
//};
