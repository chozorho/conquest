#pragma once

/* This is the over arching class that every game object that can be interacted with belongs to.
 * everything has a sprite defined here, also is where adjacency is checked
 * Primarily was used in the displayer class
 * was designed so inanimate objects (rocks, trees) could be used
 */

#ifndef SDL2_image
  #ifdef __linux__
    #include <SDL2/SDL_image.h>
  #else
    #include <SDL_image.h>
  #endif
  #define SDL2_image
#endif

#include <iostream>

class Entity {
  public:
    int MAX_NUM_WEAPONS = 4;

    // This are the primary instances that everything uses
    Entity(/*SDL_Renderer *central_rend, */int r, int j, const char *img) {
      /*            std::cout << img);*/
      x = r;
      y = j;
      if (img != nullptr) {
        /*SDL_Surface **/sprite/*_surf*/ = IMG_Load_RW(SDL_RWFromFile(img, "rb"), 1);
        if (sprite == nullptr) {
          std::cout<<"[ ERR ]    Sprite is still nullptr."<<std::endl;
          std::cout<<"[ ERR ]        Detail: "<<SDL_GetError()<<std::endl;
        }
/*        sprite = SDL_CreateTextureFromSurface(central_rend, sprite_surf);*/
      } else {
        sprite = nullptr;
      }
    }
    Entity(int a, int b) : Entity(a, b, nullptr) {
      
    }
    
    /* Deconstructor */
    virtual ~Entity() {
      /* In some cases, we instantiate an Entity,
       * WITH ITS OWN SPRITE (image structure).
       * If this is the case, then delete it here.
       * Otherwise, there is no sub-component to delete. */
      if (sprite != nullptr) {
/*        SDL_DestroyTexture(sprite);*/
        SDL_FreeSurface(sprite);
      }
    }
    
    void setSprite(std::string img) {
      if (sprite != nullptr) {
/*        SDL_DestroyTexture(sprite);*/
        SDL_FreeSurface(sprite);
      }
      /*SDL_Surface **/sprite/*_surf*/ = IMG_Load_RW(SDL_RWFromFile(img.c_str(), "rb"), 1);
/*      sprite = SDL_CreateTextureFromSurface(central_rend, sprite_surf);*/
    }
    SDL_Surface/*Texture*/ *getSprite() const {
      return sprite;
    }
    
    /*static */bool checkAdjacent(int otherX, int otherY) const {
      return (get_distance(x, y, otherX, otherY) == 1);
    }
    
    int getX() const {
      return x;
    }
    
    int getY() const {
      return y;
    }
    
    SDL_Surface/*Texture*/ *getIcon() const {
      return sprite;
    }
    
    void setX(int posX) {
      x = posX;
    }
    
    void setY(int posY) {
      y = posY;
    }
    
    //  manhattan distance? return abs(x_s - x_c) + abs(y_s - y_c) 
    static inline int get_distance(int x_c, int y_c, int x_s, int y_s) {
      return std::abs(x_c - x_s) + std::abs(y_c - y_s);
//      int x_diff = x_c - x_s;
//      if (x_diff > 0) {
//        int y_diff = y_c - y_s;
//        if (y_diff > 0) {
//          return (x_diff + y_diff);
//        } else {
//          return (x_diff - y_diff);
//        }
//      } else {
//        int y_diff = y_c - y_s;
//        if (y_diff > 0) {
//          return (y_diff - x_diff);
//        } else {
//          return (0 - y_diff - x_diff);
//        }
//      }
    }

/*    bool operator == (const Entity &other)*//* const*//* {
      return (x == other.getX() && y == other.getY());
    }*/
  protected:
    /* Want some Coca Cola?
     * No thanks but I will take a "Sprite."
     * e a s t e r  e g g #5 */
    /* TODO now that SDL is being used...
     * consider a more efficient solution to the sprites.
     * Solving the following two issues:
     * 1: Having to pass a Renderer into Unit as a Constructor parameter.
     *    Basically, if the sprite were a Surface instead of a Texture,
     *    It would make the constructors a lot simpler.
     *    There **may** be a performance tradeoff with this, I'm not sure.
     * 1-update: Because ALL subclasses would have to be modified,
     *    let's replace Texture with Surface.
     *    In the worst-case, the Surface can be converted to a Texture
     *    later at runtime.
     * 2: Having to use multiple (Texture *) objects for the same image...
     *    As currently written, many enemy grunts (and player Units!)
     *    Have the same image sprite associated with them.
     *    Why waste memory with multiple objects for the same image?
     *    Maybe offering a new constructor, where the POINTER to
     *    the Surface/Texture is passed as parameter, would resolve this.
     * */
    SDL_Surface *sprite = nullptr;
    
  private:
    int x, y;
};
