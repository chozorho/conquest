/*
 *    The MultiGen Game -- an elaborate, customizable 2D strategy game.
 *    Copyright (C) 2019 and GNU GPL'd by chozorho & TrebledJ
 *    Thanks to all contributors, including but not limited to:
 *        TrebledJ (C++ design & development)
 *        MrMcApple (Java code design & implemenetation)
 *    A minor thanks to David Holmes for answering C++ questions.
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
/* 
 * Ok, on the miraculous offchance that SOMEONE is reading this:
 * I do sincerely believe that the class design (Object-Oriented Design) used
 * herein is rather clever.

 ========================
 ==  Class structure   ==
 ========================

  +--------------------------------+
  |          Entity                |
  +--------------+-----------------+
                 ^
                 |
          +------+--------------+
          |       Unit          |
          +-----------/---------+
  THE JULY 2019 REFACTOR has killed the Unit subclasses.
       A moment of silence for the lost subclasses.
// /// /// /// //// /// // // /// /// //// //// //// /// //// /
/  // / // /// //  / //  /  / // / /   / /// // ///   /// /////
// /// ///+///-////--//-//-//-///-///-////-////---//-///+//   /
/ //     /|/  ///  /   /  /   /  |//      /// /  /// / ///  //
  +-\-//-/+-\//-+/  //  / +-/\/--+-\\--+ //  /// / +-//-+-///-+
  | / Soldier\//| ///  // | //Archer///|   . . .   |// //// //|
//+--/\--/--//\-+//  / // +--//-\/-/-/\+// // ///  +--//---/-/+
/ //    // / ///  / / // //// ///   / //// // / /// /// /// 
  
 ==========================
 ==Weapon class structure==
 ==========================

                                     +------------------+
                                     |      Weapon      |
                                     +------------------+
                                              ^
                                              |
                          +-------------------------------------------+
                          |                     |                     |
  +--------+        +--------+          +----------+            +----------+
  | Quiver |<-owns<-|  Bow   |          |  Sword   |    . . .   |          |
  +--------+        +--------+          +----------+            +----------+
  
  
 ==================================
 ==All different TYPES o' battles==
 ==================================
  
                         +--------------------+
                         |      Tactical      |
                         |        Map         |
                         |       Battle       |
                         |       (TMB)        |
                         +--------------------+
                                    ^
                                    |
       +--------------------------------------------------------------------------+
       |                 |                 |                  |                   |
  +----------+      +---------+      +-----------+    +--------------+    +----------------+
  |   Rout   |      |   Boss  |      |   Seize   |    |   DefendLoc  |    |    DefendUnit  |
  |   TMB    |      |   TMB   |      |    TMB    |    |     TMB      |    |      TMB       |
  +----------+      +---------+      +-----------+    +--------------+    +----------------+
  
 */
/* TODO Figure out what the C++ "Rule of Three" is and apply it to these classes.
 * (Yep. You read that right. I really am that much of a noob.) */
#pragma once

#include "ai.hpp"
#include "entity.hpp"
#include "global.hpp"
#include "graph.hpp"
#include "item.hpp"
#include "unit.hpp"
#include "weapon.hpp"

#include "SDL_FontCache.hpp"
#ifdef __linux__
  #include <SDL2/SDL_image.h>
  #include <SDL2/SDL_mixer.h>
#else
  #include <SDL_image.h>
  #include <SDL_mixer.h>
#endif

#include <cstdlib>
#include <iostream>
#include <list>
#include <map>
#include <random>
#include <sstream>
#include <string>
#include <unordered_map> /* to be used for pathfinding */
#include <unordered_set> /* Used for the moved-set! :D */
#include <vector>


/* To SEED a random function with the current time,
 * the following may end up being useful:
#include <chrono> */

//  TODO convert macros to constants (const/constexpr)
#define NUM_SIDES 2
#define NUM_STOCK_OPTIONS 9
#define NUM_TERRAIN_TYPES 7
#define MENU_OPTION_HEIGHT 40
#define NUM_TMB_MAP_CHOICES 12
#define VALID_PLAYER_SPAWN_LOC 16
#define VALID_ENEMY_SPAWN_LOC 32

/* Full size * /
#define TMB_SCREEN_HEIGHT 1080
#define TMB_SCREEN_WIDTH 1920 */

/* Half size */
#define TMB_SCREEN_HEIGHT 540
#define TMB_SCREEN_WIDTH 960

/* Quarter size - seems to work better when testing in VMs */
/* #define TMB_SCREEN_HEIGHT 270
#define TMB_SCREEN_WIDTH 480 */

/* Enum for the return type of a TMB launch() */
enum TMB_RESULT {
  PLAYER_VICTORY=0, ENEMY_VICTORY=1, COLD_EXIT=2, ERR=3
};

enum TMB_TYPE {
  ROUT, DEFEND_LOC, DEFEND_UNIT, SEIZE, BOSS
};

enum MUSIC_PHASE { DEFAULT, ONE_ENEMY_LEFT, JUST_SEIZE_IT, ONE_PLAYER_LEFT,
                   DEFEAT, VICTORY };

namespace std
{
  template <>
  struct hash<MUSIC_PHASE> {
    size_t operator()(MUSIC_PHASE muInd) const {
       return (size_t) muInd;
    }
  };
};

class TacticalMapBattle {
  public:
  
    /* TMB Constructor */
    TacticalMapBattle(/*SDL_Renderer *render_displayTMB, */
                      const char *map_file_name, const char *terrain_types,
                      /*ALLEGRO_DISPLAY *render_displayTMB, */std::vector<Unit *> &players,
                      std::vector<Unit *> &enemies/*, int cond*/, int verbose_copy);
    
    virtual ~TacticalMapBattle();
  
    /* Pretty self-explanatory methods for checking conditions
     * (whether the player has won yet or lost yet).
     * Subclasses have different victory conditions etc.*/
    virtual bool check_victory_condition() const = 0;
    virtual bool check_loss_condition() const = 0;

    /* A very simple "post-visit"-esque method, which performs 
     * an action (often nothing!) every time an enemy turn ends. */
    virtual void post_turnphase() = 0;
    int getTurnPhase();
    void setTurnPhase(int newTurn);

    void repaint();

    bool isCheating();

    int getGameState();
    void setGameState(int new_state);

    void clearMovedSet();
    
    /* Postcondition:
     *   will draw the TMB side menu ONTO the menu_img image object,
     *     based on the options listed in the vector `menu` */
    void update_menu_img();

    /* Postcondition: Sets the values in the `menu' vector,
     * which consists of STRINGS to be printed later
     * in the ABOVE member function, update_menu_img(). */
    void make_stop_menu();

    enum GAME_STATE_NAME { IDLE=1, EXPECT_DEST=2, EXPECT_MENU_CHOICE=3,
      EXPECT_ENEMY_CHOICE=4, EXPECT_WEAPON_CHOICE=5, EXPECT_RESCUED_UNIT=6/*11*/,
      EXPECT_DROP_LOC=7/*12*/, EXPECT_PASS=8/*13*/, EXPECT_TRADE_PARTNER=9/*18*/,
      EXPECT_TRADE_ACT=10/*19*/, EXPECT_ITEM_USE=11/*24*/, EXPECT_SPEC_TARGET=12/*28*/,
      EXPECT_SPEC_WEAP=13/*29*/ };

    /* Preconditions:
     *  x_click is a positive value representing the x-coordinate of the click;
     *  y_click is a positive value representing the y-coordinate of the click;
     * Postcondition:
     *  Processes the user's selection and updates the game state. */
    int answer_click(int x_click, int y_click);

    /*
     * ---OUTLINE of setting the contents of the terrain map---
     *  al_create_bitmap of the proper width and height;
     *  al_set_target_bitmap to the newly created map;
     *  go through the same for-loops as below, but use
     *    draw_scaled_bitmap(); FROM the terrain_tiles array TO the target map;
     *  Then reset target_bitmap so that it is the backbuffer of the displayTMB.
     */
    /* Preconditions: Now that this has been re-hauled into an Object,
     *   the preconditions are simply that this object has
     *   been fully constructed. */
    int display_terrain_map(int print_verbose);

    void display_chosen_unit_frame(int t_wid, int t_hei, Unit *sel,
          int urx, int ury, bool showHP);
    void display_chosen_unit(int t_wid, int t_hei, Unit *sel,
          int urx, int ury, bool showHP);
    void display_one_unit(int t_wid, int t_hei, int x, int y, bool showHP);
    int display_units(int tile_width, int tile_height);

    /* Precondition: file_name is non-nullptr valid file name with a proper path included.
     * Postcondition: the variables to which width and height are pointers will be
     *    set correctly;
     *   the game board will be properly read from the appropriate file, and
     *    saved in the 2d array to which `board` points.
     *   ALSO allocates memory for the `unit_board` pointer!  */
    int load_map(const char *file_name);

    int load_units(std::vector<Unit *> &side0_units,
                   std::vector<Unit *> &side1_units);
    
    int load_terrain_tiles(const char *name);
    
    void setSettlementName(const char *settlement);
    
    void setSettlementName(const std::string &settlement);
    
    const std::string &getSettlementName() const;

    bool isProvinceBattle() const;
    void setProvince(bool prov_b);
    
    /* Preconditions: This TMB object has been fully constructed, with
     *  a valid terrain map and unit board, 
     *  non-nullptr objects to contain sets of units,
     *  and a valid non-nullptr displayTMB. */
    virtual TMB_RESULT/*int*/ launch();

    /* Public accessor methods for after the battle ends */
    const std::vector<Unit *> &getDebilPlayers() const;
    const std::vector<Unit *> &getLivingPlayers() const;
    const std::vector<Unit *> &getDebilEnemies() const;

    virtual TMB_TYPE getType() = 0;
    virtual bool checkForMusicTransition();
    
  protected:
    /*** Image data & dimensions ***/
    SDL_Renderer *displayTMB = nullptr;
    SDL_Window *tmb_wind = nullptr;
    SDL_Texture *terrain_base;
    SDL_Texture *layer2;
    SDL_Texture *menu_img;
    SDL_Texture *terrain_tiles[NUM_TERRAIN_TYPES];
    bool redraw;
    int terrain_map_width;
    int terrain_map_height;
    int tiles_x, tiles_y;
    //     Note:  for now we assume that TMB graphical tiles will all be squares.
    //     Should this ever change, divide this into width and height variables
    //     and then update display_terrain_map()
    int tile_side_length;
    
    /*** Underlying map grid data ***/
    TERRAIN_TYPE **terrain_board;
    Unit ***unit_board;
    
    /*** Structures used in the shortest-path alg ***/
    /* Most importantly,
     * we have a graph structure for the WHOLE TACTICAL MAP --
     *   in adjacency-list format */
    /*    std::unordered_map<Node, std::list<Edge>> *graph = nullptr;*/
    std::list<Edge> **graph = nullptr;
    int **locale = nullptr;
    std::list<Entity> path;
    std::unordered_map<Node, Entity> previous;
/*    Node *previous[] = nullptr;*/
    
    /*** General state vars ***/
    int game_state;
    int verbosity;
    int condition; // may be renderred obsolete if using subclasses of the TMB class.
    int turn_phase = 0;
    bool cheat[3];
    bool showAnimation;

    /* Music-related variables */
    MUSIC_PHASE musicPhase = DEFAULT;
    bool musicOn;
    std::unordered_map<MUSIC_PHASE, Mix_Music*> themes;
    
    /* Mutexes for all the sh*t */
    SDL_mutex *redraw_mut;
    SDL_mutex *phase_control;
    SDL_mutex *cheat_control;
    SDL_mutex *state_control;
    
    /*** Timers and Timing parameters ***/
    SDL_TimerID tmbTicker = 0;
    SDL_TimerID tmbEnemyAI = 0;
    SDL_TimerID animation_timer = 0;
    double FPS = 29; // later change to 29.999 or 59.999
    double FRAMES_PER_TILE = 9; // later change to 29.999 or 59.999
    Uint32 msPerAICheck = 250;
    
    /* The almighty PRNG.
     * TODO see about seeding this in a less predictable way? */
    /* UHH this should have been moved into global.hpp herp derp */
    /* "YOUUU DIDN'T SEEEE ANYTHING!" */
/*    std::mt19937_64 generator;*/
    
    /*** Unit data structures ***/
    std::unordered_set<Unit *> moved_set;
    std::vector<Unit *> player_units;
    std::vector<Unit *> enemy_units;
    std::vector<Unit *> debilitated_players;
    std::vector<Unit *> living_players;
    std::vector<Unit *> debilitated_enemies;
    Unit *chosen;
    Unit *target;
    std::vector<std::string> menu;
    std::vector<Unit *> in_range;
    std::string loc_name;
    bool is_province;
    
    /* SDL Utils, for colors, drawing, message boxes */
    SDL_Color black_text = {2, 2, 2, 255};
    SDL_Rect *dframe = nullptr;
    SDL_MessageBoxButtonData cancel_buttons[3] =
    { { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1, "YES, skip with VICTORY"},
      { SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 2,
        "YES, quit with DEFEAT"},
      { SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 3,
        "NO, cancel"} };
    
    SDL_MessageBoxButtonData init_tmb_buttons[3] =
    { { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1, "Autoresolve" },
      { SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 2, "Retreat"},
      { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 3, "Fight"} };
    
    SDL_Texture *player_phase_text = nullptr;
    SDL_Texture *enemy_phase_text = nullptr;
    SDL_Texture *victory_text = nullptr;
    SDL_Texture *defeat_text = nullptr;
    
    TTF_Font *font_luxirb_vvsmall = nullptr;
    TTF_Font *font_luxirb_med = nullptr;
    FC_Font *fc_luxirb_player = nullptr;
    FC_Font *fc_luxirb_enemy = nullptr;
    
    /*** FUNCTIONS ***/
    /* First, put the timer functions. */
    static Uint32 process_tick(Uint32 step, void *argv);
    static Uint32 process_ai(Uint32 step, void *args);
    static Uint32 toggle_terrain_animation(Uint32 step, void *args);
    static Uint32 toggle_unit_animation(Uint32 step, void *args);
    
    /* Now -- to include all the helper functions (there are several,
     * may want to move them to a separate structure later).
     * But at least they're all in one place.  */
    /* TODO Figure out a more elaborate spawning scheme for each map */
    int spawn_locate_units();
      
    bool has_moved(Unit *ele) const;
    
    /* Precondition: turn has value 0 or 1.
     * Postcondition: if it is the player turn: make one enemy unit move in a
     *        random, valid direction.
     *   if it is the enemy turn: have the enemy make a judgement about the
     *   number of player units
     *   (encapsulated in the EnemyAI class) and then move accordingly. */
    void process_one_ai(int turn);
    
    void getFriendliesInRange(int xCoord, int yCoord, const std::array<Weapon*, WEAPON_CAPACITY> &weapons);
    
    void getEnemiesInRange(/*int maxR, int minR, */int xCoord, int yCoord,
                           const std::array<Weapon*, WEAPON_CAPACITY> &weapons/*, int xSrc, int ySrc*/);
    
    /***
     * A recursive method to identify all enemy units in range
     * and determine which, if any, weapons can reach the enemy from the player unit's 
     * current position.
     * *//*
          void getEnemiesInRange(int range, int xCoord, int yCoord, Weapon **weapons,
          int xSrc, int ySrc);*/
    
    /* Postcondition: Returns a boolean,
     *  defining whether the GIVEN unit can travel to the 
     *  GIVEN terrain_board address.
     * For now, follow the  K.I.S.S. methodology. */
    bool canMove(Unit *subject, int dx, int dy) const;
    bool canMove(int dx, int dy) const;
    
    /* A method to check whether the selected Player unit 
     * can attack from its current position...
     * Precondition: `o` (obj) represents a non-nullptr player Unit
     * Postcondition: 
     *   Updates the list of Units that are within attack range
     *   and then returns a bool representing whether that
     *   list is non-empty.  */
    bool can_attack(Unit *o);
      
    bool can_rescue(Unit *o) const;
      
    bool can_drop(Unit *o) const;
      
    bool can_trade(Unit *o) const;
      
    bool can_pass(Unit *o) const;
    
    /* TODO -- METHOD UNDER CONSTRUCTION...
     * MANY ERRORS HERE
     * Especially with the calculation of upper_x etc. */
    int show_move_range_flying(int speed, int src_x, int src_y);
    
    /* Note: this current algorithm is perfectly accurate, but 
     * unfortunately inefficient (can take multiple seconds to finish
     * on maps with large tiles).
     *
     * Update: this is only noticeable when the tile size is large
     *  (so, when the map size is large relative to the number of tiles).
     *
     * So developer(s): be sure to look for something better
     *
     * One attempted solution is given above, in show_move_range_flying,
     * but that is still in development and assumes the unit can
     * fly over bodies of water.
     **/
    /* TODO -- UNSTABLE -- IN TESTING -- implementing dynamic programming
     *  approach; this should be a more efficient approach,
     *  in that it studies each map tile AT MOST ONCE;
     *  the primitive recursive method checks the same
     *  tile many times. */
    void show_move_range_dyn(int speed, int xCoord, int yCoord);
    
    /* Decoupled movement range DISPLAY from internal computation;
     * i.e. one could have an internal array of ints or bools,
     * simply representing whether a unit CAN travel there,
     * and LATER write to the layer2 texture based on the finished array values.
     * The reason why this is superior is because it avoids the need for
     * a SINGLE read-write texture.
     * In order to use LockTexture() and GetRGBA(), the texture must
     * be STREAMING. But in order to actually use it as a target for rendering
     * things on top of it, it must be TARGET.
     * It CANNOT be both at the same time. */
    void show_move_range(const int speed, const int xCoord, const int yCoord);
    
    void show_weapon_range_efficient(int upper_range, int lower_range,
                                     int xCoord, int yCoord);
    
    /* Precondition: xCoord and yCoord are valid (nonnegative and less than the
     *  width and height of the TMB map).
     * 
     * Postcondition: Displays maximum weapon range by drawing rectangles
     *  on the TMB screen.
     * 
     * TODO Fix this for min range and max range
     *   probably with a simple combination of nested loops
     *   ...  the recursive approach here does not work */
    void show_weapon_range(int range, int base, int xCoord, int yCoord);
    
    /* Precondition: next_u is a valid, non-null pointer to a player Unit.
     * Postcondition: if next_u has already moved, no-op.
     *  Otherwise, add next_u to the list of moved units.
     *  TODO make this a HashSet to provide O(1) operations,
     *  rather than the O(n) vector implementation provided here. */
    void set_moved(Unit *next_u);
    
    /* Precondition: start_ind is some natural number whose value is less
     *  than the current size of the menu.
     * Postcondition: Add a player unit to the "has-moved-this-turn" structure.
     * Also free memory associated with the menu. */
    void end_unit_turn(int start_ind);
    
  /* void find_enemies(int range, int xCoord, int yCoord); */

    static int attack(Unit *p, Unit *tar, Weapon *used,
            int t, bool mustHit);
    
  private:
    /* 2018-02-14 : We need something to store
     * the MOST COMMON menu options.
     * For the menu options, we should store some stock strings
     * instead of repeatedly allocating memory for the same exact string.
     * In other words, we need some...
     * ...
     * ... (puts on sunglasses)
     * ...
     * "stock options."
     * Haha!!
     * Contact me (chocorho) if you read that.
     * 6190 lines, 371 days
     *  e a s t e r  e g g  # 3. */
    /*    std::vector<std::string *> *cons_menu_options;*/
    std::string cons_menu_options[NUM_STOCK_OPTIONS];

    /* Helper function called when movement animations play */
    void renderNextChosenAnimationFrame(int dCoordX, int dCoordY);
};

/* The most basic implementation of a classic Tactical Map Battle.
 * Ends with player victory when all enemies are debilitated;
 * ends with player defeat when all player units are debilitated. */
class RoutTMB : public TacticalMapBattle {
  public:
    RoutTMB(const char *map_file, const char *terrain_types,
        /*ALLEGRO_DISPLAY *render_displayTMB, */std::vector<Unit *> &players,
            std::vector<Unit *> &enemies, int verbose_copy);
  
  protected:
    void post_turnphase();
    bool check_victory_condition() const;
    bool check_loss_condition() const;
    TMB_TYPE getType();
};

class BossTMB : public TacticalMapBattle {
  public:
    BossTMB(const char *map_file, const char *terrain_types,
      /*  ALLEGRO_DISPLAY *render_displayTMB, */std::vector<Unit *> &players,
        std::vector<Unit *> &enemies, std::vector<Unit*> &given_boss,
            int verbose_copy);
  
  protected:
    void post_turnphase();
    std::vector<Unit *> bosses;
    
    bool check_victory_condition() const;
    bool check_loss_condition() const;
    TMB_TYPE getType();
};

/* Player needs to seize a special point (coordinate) */
class SeizeTMB : public TacticalMapBattle {
  public:
    SeizeTMB(const char *map_file, const char *terrain_types,
      /*  ALLEGRO_DISPLAY *render_displayTMB,*/ std::vector<Unit *> &players,
             std::vector<Unit *> &enemies, int x, int y, int verbose_copy);

  protected:
    int dest_y, dest_x;
    void post_turnphase();
    bool check_victory_condition() const;
    bool check_loss_condition() const;
    bool checkForMusicTransition();
    TMB_TYPE getType();
};

/* Defend for a certain number of turns */
class DefendLocTMB : public TacticalMapBattle {
  public:
    /* PRECONDITION: `turn_count` is positive!!! */
    DefendLocTMB(const char *map_file, const char *terrain_types,
      /*  ALLEGRO_DISPLAY *render_displayTMB,*/ std::vector<Unit *> &players,
                 std::vector<Unit *> &enemies, size_t turn_count, int verbose_copy);
  
  protected:
    size_t turns_left;
    void post_turnphase();
    bool check_victory_condition() const;
    bool check_loss_condition() const;
    TMB_TYPE getType();
};

class DefendUnitTMB : public TacticalMapBattle {
  public:
    DefendUnitTMB(const char *map_file, const char *terrain_types,
      /*  ALLEGRO_DISPLAY *render_displayTMB,*/ std::vector<Unit *> &players,
        std::vector<Unit *> &enemies, std::vector<Unit*> &to_protect,
                  int turn_count, int verbose_copy);
  
  protected:
    int turns_left;
    std::vector<Unit *> protect_set; // TODO "set"? know what I'm thinking.
    void post_turnphase();
    bool check_victory_condition() const;
    bool check_loss_condition() const;
    TMB_TYPE getType();
};
