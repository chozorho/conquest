/*
 *    The MultiGen Game -- an elaborate, customizable 2D strategy game.
 *    Copyright (C) 2019 and GNU GPL'd by Chocorho & TrebledJ
 *    Thanks to all contributors, including but not limited to:
 *      MrMcApple (Java code design & implementation)
 *      TrebledJ (C++ design & development)
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
*/
#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS
#endif

#include "Game.hpp"

#ifdef __linux__
  #include <SDL2/SDL_mixer.h>
#else
  #include <SDL_mixer.h>
#endif

#include <fstream>
#include <iostream>

#ifdef __APPLE__
  #include <errno.h>
  #include <stdio.h>
  #include <unistd.h>
#endif

namespace global {
  // for displaying the world map and responding properly to clicks
  int x_adjust = 0;
  int y_adjust = 0;
};

WMSL::WMSL(SDL_Renderer *dis, SDL_Window *wind, std::string *name) 
  : WMSL(dis, wind, name->c_str())
{
}

WMSL::WMSL(SDL_Renderer *dis, SDL_Window *wind, const char *name)
  : display(dis), window(wind) {
  dframe = new SDL_Rect();
  /*      child_windows = new std::vector<SDL_Window *>();*/
  game_seconds_per_tick = 1600;
  up_img_fill = IMG_LoadTexture(display, "./res/up_arrow_filled.png");
  if (up_img_fill == nullptr) {
    std::cout << "Up arrow (filled) file not found!" << std::endl;
  }
  up_img_mt = IMG_LoadTexture(display, "./res/up_arrow_mt.png");
  if (up_img_mt == nullptr) {
    std::cout << "Up arrow (gray) file not found!" << std::endl;
  }
  down_img_fill = IMG_LoadTexture(display, "./res/down_arrow_filled.png");
  if (down_img_fill == nullptr) {
    std::cout << "Down arrow (filled) file not found!" << std::endl;
  }
  down_img_mt = IMG_LoadTexture(display, "./res/down_arrow_mt.png");
  if (down_img_mt == nullptr) {
    std::cout << "Down arrow (gray) file not found!" << std::endl;
  }
  arrow_imgs[0] = up_img_fill;
  arrow_imgs[1] = up_img_mt;
  arrow_imgs[2] = down_img_fill;
  arrow_imgs[3] = down_img_mt;
  
  make_team_img  = IMG_LoadTexture(display, "./res/make_team.png");
  if (make_team_img == nullptr) {
    std::cout << "\"Make team\" file not found!" << std::endl;
  }
  invade_province_img = IMG_LoadTexture(display, "./res/invade_province.png");
  if (invade_province_img == nullptr) {
    std::cout << "\"Make team\" file not found!" << std::endl;
  }
  invade_settlement_img = IMG_LoadTexture(display, "./res/invade_settlement_3.png");
  if (invade_settlement_img == nullptr) {
    std::cout << "\"Make team\" file not found!" << std::endl;
  }
  scroll_b = IMG_LoadTexture(display, "./res/scroll_icon_small.png");
  if (scroll_b == nullptr) {
    std::cout << "\"Scroll button\" file not found!" << std::endl;
  }
  
  unit_disp_ind = 0;
  map_img = nullptr;
  lower_right_canvas = nullptr;
  lower_left_canvas = nullptr;
#ifdef NEW_UI
  right_panel_canvas = nullptr;
#endif
  visual_type = MAP;
  /*      time_speed_msg = (char *)calloc(1, 32);*/
  date_msg = "yyy: mm.dd";
  time_speed_msg = "SPEED x0.000";
  paused = true;
  date[0] = date[1] = date[2] = 1; // clever, eh? ;)
  scale_factor = 1.0f;
//  player_units = new std::vector<Unit *>();
//  retired/*_units*/ = new std::vector<Unit *>();
//  opp_units = new std::vector<Unit *>();
//  dead_pool = new std::vector<Unit *>();
  pending_battles = new std::vector<TacticalMapBattle *>();
/*  chosen_ones = new std::vector<Unit *>();*/
/*  chosen_indices = new std::vector<unsigned int>();*/
  prov_ids = new std::vector<int>();
  enemy_provinces = 0;
  names = new std::vector<std::string>();
  load_names();
  game_state = 11;
  thread_state = 11;
  calendar = new (std::vector<TacticalMapBattle *> *[DAYS_PER_MONTH*MONTHS_PER_YEAR]);
  next_year = new (std::vector<TacticalMapBattle *> *[DAYS_PER_MONTH*MONTHS_PER_YEAR]);
  last_prov = 0;
  
  /* Initialize text textures (no that's not a typo) */
  /* TODO consider replacing these with image icons? Makes more space */
  /* FONT initialization */
  font_luxirb_med = TTF_OpenFont("./res/fonts/luxirb.ttf", 28);
  fc_luxirb_med = FC_CreateFont();
  fc_luxirb_small = FC_CreateFont(); // used for Unit data (in displayUnitInfo())
  fc_luxirb_sm = FC_CreateFont(); // used for PROVINCE data (in, uhh, launch(). lol)
  white_luxirb_med = FC_CreateFont();
  white_luxirb_sm = FC_CreateFont();
  load_map(name);
  /* TODO put all the similar fonts in the same array? */
  FC_LoadFont(fc_luxirb_med, display, "./res/fonts/luxirb.ttf", 28,
              FC_MakeColor(2,2,2,255), TTF_STYLE_NORMAL);
  FC_LoadFont(fc_luxirb_sm, display, "./res/fonts/luxirb.ttf", 16,
              FC_MakeColor(2,2,2,255), TTF_STYLE_NORMAL);
  FC_LoadFont(white_luxirb_med, display, "./res/fonts/luxirb.ttf", 28,
              FC_MakeColor(250,250,250,255), TTF_STYLE_NORMAL);
  FC_LoadFont(white_luxirb_sm, display, "./res/fonts/luxirb.ttf", 16,
              FC_MakeColor(250,250,250,255), TTF_STYLE_NORMAL);
  if (fc_luxirb_med == nullptr) {
    std::cerr << "Failed to load the font luxirb.ttf in medium size" << std::endl;
    return;
  }
  if (fc_luxirb_sm == nullptr) {
    std::cerr << "Failed to load the font luxirb.ttf in small size" << std::endl;
    return;
  }
  if (white_luxirb_med == nullptr) {
    std::cerr << "Failed to load the font luxirb.ttf in medium size, white color" << std::endl;
    return;
  }
  if (white_luxirb_sm == nullptr) {
    std::cerr << "Failed to load the font luxirb.ttf in small size, white color" << std::endl;
    return;
  }
  
  /* MUTEX initialization */
  battle_control = SDL_CreateMutex();
  date_control = SDL_CreateMutex();
  prov_control = SDL_CreateMutex();
  state_control = SDL_CreateMutex();
  thread_control = SDL_CreateMutex();
  redraw_control = SDL_CreateMutex();
  pause_control = SDL_CreateMutex();
  
  /* SURFACE init */
#ifdef NEW_UI
  mapTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "MAP", Util::white_text);
  armyTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "ARMY", Util::white_text);
  researchTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "RESEARCH", Util::white_text);
  homefrontTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "HOMEFRONT", Util::white_text);
#else
  mapTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "MAP", black_text);
  armyTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "ARMY", black_text);
  researchTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "RESEARCH", black_text);
  homefrontTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "HOMEFRONT", black_text);
#endif
  mapText = SDL_CreateTextureFromSurface(display, mapTextSurf);
  armyText = SDL_CreateTextureFromSurface(display, armyTextSurf);
  researchText = SDL_CreateTextureFromSurface(display, researchTextSurf);
  homefrontText = SDL_CreateTextureFromSurface(display, homefrontTextSurf);
  
  /* Pause Menu Options */
  SDL_Surface *resumeTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "Resume (`r`)", black_text);
  resumeText = SDL_CreateTextureFromSurface(display, resumeTextSurf);
  SDL_Surface *saveExitTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "Save & Exit (`x`)", black_text);
  saveExitText = SDL_CreateTextureFromSurface(display, saveExitTextSurf);
  SDL_Surface *exitTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "Exit (`q`)", black_text);
  exitText = SDL_CreateTextureFromSurface(display, exitTextSurf);
}

WMSL::~WMSL() {
  /*      free(province_map);*/
/*  unsigned int upper_bound;*/
  /*      free(time_speed_msg);*/
//  upper_bound = provinces[index]->getSettlements()->size();  //   ??? unused ?   -T 
  for (size_t index=0; index < num_provinces; index++) {
    delete provinces[index];
  }
  free(provinces);
  SDL_RemoveTimer(ticker_timer);
  /*      if (game_state == -4) {*/
  for (size_t index = 0; index < player_units.size(); index ++) {
    /*          delete player_units.at(index);*/
    /* already deleted by the province -> settlement -> army deletion chain! */
  }
  /*      }*/
  for (size_t index = 0; index < opp_units.size(); index ++) {
    delete opp_units.at(index);
    /* already deleted by a TMB closure! TODO double-check this */
  }
//  delete player_units;
//  delete retired/*_units*/;
//  delete opp_units;
  
  /*for (size_t index = 0; index < names->size(); index ++) {
    delete (*names)[index];
  }*/ // No longer necessary when moving from string PTRs to strings.
  delete names;
//  delete chosen_ones;
//  delete chosen_indices;
  delete pending_battles;
  delete prov_ids;
  
  SDL_DestroyMutex(battle_control);
  SDL_DestroyMutex(date_control);
  SDL_DestroyMutex(prov_control);
  SDL_DestroyMutex(state_control);
  SDL_DestroyMutex(thread_control);
  SDL_DestroyMutex(redraw_control);
  SDL_DestroyMutex(pause_control);
  
  for (size_t index = 0; index < dead_pool.size(); index ++) {
    delete dead_pool.at(index);
  }
/*  delete dead_pool;*/

  /* Textures */
  SDL_DestroyTexture(up_img_fill);
  SDL_DestroyTexture(up_img_mt);
  SDL_DestroyTexture(down_img_fill);
  SDL_DestroyTexture(down_img_mt);
  SDL_DestroyTexture(make_team_img);
  SDL_DestroyTexture(invade_province_img);
  SDL_DestroyTexture(invade_settlement_img);
  SDL_DestroyTexture(title_screen_text);
  
  SDL_DestroyTexture(mapText);
  SDL_DestroyTexture(armyText);
  SDL_DestroyTexture(researchText);
  SDL_DestroyTexture(homefrontText);
  SDL_DestroyTexture(resumeText);
  SDL_DestroyTexture(saveExitText);
  
  /* Surfaces */
  /* TODO see about freeing these surfaces earlier?
   * 혹시Can we free them right after their corresponding
   * textures are created? */
  SDL_FreeSurface(map_img_surf);
  SDL_FreeSurface(internal_map_surf);
  SDL_FreeSurface(mapTextSurf);
  SDL_FreeSurface(armyTextSurf);
  SDL_FreeSurface(researchTextSurf);
  SDL_FreeSurface(homefrontTextSurf);
  SDL_FreeSurface(title_screen_surf);

  /* TODO free the "Save/Exit" and "Resume Game" Surfaces? */

  /* Fonts */
  FC_FreeFont(fc_luxirb_med);
  FC_FreeFont(fc_luxirb_sm);
  FC_FreeFont(white_luxirb_med);
  
  delete[] calendar;
  delete[] next_year;
  
  delete dframe;
}

bool WMSL::getRedraw() {
    SDL_LockMutex(redraw_control);
    bool red_copy = redraw;
    SDL_UnlockMutex(redraw_control);
    return red_copy;
}
int WMSL::getGameState() {
    SDL_LockMutex(state_control);
    int g_state_copy = game_state;
    SDL_UnlockMutex(state_control);
    return g_state_copy;
}
int WMSL::getThreadState() {
    SDL_LockMutex(thread_control);
    int t_state_copy = thread_state;
    SDL_UnlockMutex(thread_control);
    return t_state_copy;
}
void WMSL::setThreadState(int n_state) {
    SDL_LockMutex(thread_control);
    thread_state = n_state;
    SDL_UnlockMutex(thread_control);
}
void WMSL::setGameState(int n_state) {
    SDL_LockMutex(state_control);
    game_state = n_state;
    SDL_UnlockMutex(state_control);
}
void WMSL::repaint() {
    SDL_LockMutex(redraw_control);
    redraw = true;
    SDL_UnlockMutex(redraw_control);
}
void WMSL::deepRepaint() {
    SDL_LockMutex(redraw_control);
    redraw = true;
    deep_redraw = true;
    SDL_UnlockMutex(redraw_control);
}
bool WMSL::isPaused() {
    SDL_LockMutex(pause_control);
    bool pause_cpy = paused;
    SDL_UnlockMutex(pause_control);
    return pause_cpy;
}
void WMSL::setPaused(bool ex_paused) {
    SDL_LockMutex(pause_control);
    paused = ex_paused;
    SDL_UnlockMutex(pause_control);
}


int WMSL::launch() {
  SDL_TimerID enemyAI_timer;
  SDL_TimerID framerate_ti;
  game_state = 11;
  /*      unsigned int *time = (unsigned int *) malloc(sizeof(unsigned int));*/
  double FPS = 30.0;
  bool l_shift_on=false, r_shift_on=false;
  
  /*** INIT TIMERS ***/
  /* Maybe later create a timer to ensure minimum framerate?
   * This used ot be done in ticker_timer, until the developers realized
   * how much of a misnomer that would be. :P */
  /* TICKER TIMER initialization  */
  /* Only do this after the whole object has fully constructed! */
  /*      TickerParams *param_package = new TickerParams(pending_battles,
   battle_control, date_control);*/
  
  /*      framerate_ti = SDL_AddTimer(1.0 / FPS, , );
   if (framerate_ti == 0) {
   std::cout << "ERR: failed to load FPS timer :-(" << std::endl;
   SDL_DestroyRenderer(display);
   SDL_DestroyWindow(window);
   return -1;
   }*/
  
  /*      Uint32 tick_event_id = SDL_RegisterEvents(1);*/
  ticker_timer = SDL_AddTimer(game_seconds_per_tick, processBattles,
                              ((void *) this)); 
  if (ticker_timer == 0) {
    std::cout << "FATAL ERR: failed to load FPS timer :-(" << std::endl;
  }
  
  enemyAI_timer = SDL_AddTimer(game_seconds_per_tick, processWmslAI, this);
  if (enemyAI_timer == 0) {
    std::cout << "ERR: failed to load the enemy AI timer :-(" << std::endl;
    SDL_DestroyRenderer(display);
    SDL_DestroyWindow(window);
    SDL_RemoveTimer(ticker_timer);
    return (-1);
  }
  
  /*** INIT FONTS ***/
  /* Note to developers:
   * original font files were in /usr/share/fonts/TTF/
   * in case you need to know that drectory.  */
  /*      FC_Font *font_amok = TTF_OpenFont("./res/fonts/ComputerAmok.ttf", 54);*/
  FC_Font *font_amok = FC_CreateFont();
  FC_LoadFont(font_amok, display, "./res/fonts/ComputerAmok.ttf", 54, FC_MakeColor(2,2,2,255), TTF_STYLE_NORMAL);
  if (font_amok == nullptr) {
    std::cout << "Failed to load the font ComputerAmok.ttf" << std::endl;
    SDL_RemoveTimer(enemyAI_timer);
    SDL_RemoveTimer(ticker_timer);
    SDL_DestroyRenderer(display);
    SDL_DestroyWindow(window);
    return (-1);
  }
  /*      FC_Font *font_luxirb = TTF_OpenFont("./res/fonts/luxirb.ttf", 46);*/
  FC_Font *font_luxirb = FC_CreateFont();
  FC_LoadFont(font_luxirb, display, "./res/fonts/luxirb.ttf", 46, FC_MakeColor(2,2,2,255), TTF_STYLE_NORMAL);
  if (font_luxirb == nullptr) {
    std::cout << "Failed to load the font luxirb.ttf" << std::endl;
    SDL_RemoveTimer(enemyAI_timer);
    SDL_RemoveTimer(ticker_timer);
    SDL_DestroyRenderer(display);
    SDL_DestroyWindow(window);
    return (-1);
  }
/*  fc_luxirb_med = TTF_OpenFont("./res/fonts/luxirb.ttf", 28);*/
/*  FC_Font *subfont_luxirb_small = TTF_OpenFont("./res/fonts/luxirb.ttf", 16);*/
  FC_Font *subfont_luxirb_small = FC_CreateFont();
#ifdef HALLOWEEN
  FC_LoadFont(subfont_luxirb_small, display, "res/fonts/luxirb.ttf", 16,
              FC_MakeColor(HL_OJ_R, HL_OJ_G, HL_OJ_B, 255), TTF_STYLE_NORMAL);
#else
  FC_LoadFont(subfont_luxirb_small, display, "res/fonts/luxirb.ttf", 16,
              FC_MakeColor(2, 2, 2, 255), TTF_STYLE_NORMAL);
#endif
  if (subfont_luxirb_small == nullptr) {
    std::cout << "Failed to load the font luxirb.ttf in small (16) size" << std::endl;
    SDL_RemoveTimer(enemyAI_timer);
    SDL_RemoveTimer(ticker_timer);
    SDL_DestroyRenderer(display);
    SDL_DestroyWindow(window);
    return (-1);
  }
  /*TTF_FONT *font_luxirr = TTF_OpenFont("res/fonts/luxirr.ttf", 54, , );*/
  FC_Font *font_luxirr = FC_CreateFont();
  FC_LoadFont(font_luxirr, display, "res/fonts/luxirr.ttf", 54, FC_MakeColor(2,2,2,255), TTF_STYLE_NORMAL);
  if (font_luxirr == nullptr) {
    std::cout << "Failed to load the font luxirr.ttf" << std::endl;
    SDL_RemoveTimer(enemyAI_timer);
    SDL_RemoveTimer(ticker_timer);
    SDL_DestroyRenderer(display);
    SDL_DestroyWindow(window);
    return (-1);
  }
  
  highlighted_province last_selected = {-1, -1};
  /*                     ("res/world_map.png");*/
  /*  lower_right_canvas = IMG_LoadTexture("res/world-map-1-provinces.png");*/
  /*      SDL_SetRenderTarget(display, lower_right_canvas)*/
  /*      map_img = IMG_LoadTexture("res/world-map-2.png");*/
  
  /*      SDL_Texture *terrain_base = SDL_CreateTexture(display, SDL_PIXELFORMATRGBA8888, SDL_TEXTUREACCESS_TARGET, global::screen_width - IMG_WIDTH, IMG_HEIGHT);*/
  /* A very primitive LOADING SCREEN for ridiculous, bombastic effect. */
  /* e a s t e r  e g g # 4 */
#ifdef __APPLE__
  /* On Mac machines, testers have reported a high risk of running into the
   * EMFILE error by now. So on a Mac machine, I print this here.
   */
  char newDirCheck[1025] = {'\0'};
  if (getcwd(newDirCheck, 1024) == nullptr) {
    std::cerr<<"ERRNO for getting the CWD is: "<<errno<<std::endl;
    perror("which corresponds to: ");
  }
  std::cout<<"[SANITY CHECK #1] current directory is "<<newDirCheck<<std::endl;
#endif
/*  SDL_Texture *logo = IMG_LoadTexture(display, "res/ale_logo.png");*/
  SDL_Texture *logo = IMG_LoadTexture(display, "res/sgdt_logo1.png");
  if (logo == nullptr) {
    std::cerr << "ERR -- logo file not found." << std::endl;
    return (-1);
  }
  SDL_SetRenderDrawColor(display, 2, 2, 2, 230);
  SDL_RenderClear(display);
  SDL_RenderPresent(display);
  
  /* TODO Consider making these variables
   * member PROTECTED elements of the class...
   * This will especially help if the Game class will
   * be overriden by subclasses. (But will it? We'll see) */
  /*      char date_msg[16];*/
  /*      result = sprintf(time_speed_msg, "SPEED x%.3f", (10000.0f / game_seconds_per_tick));*/
  std::stringstream tmp_str;
  tmp_str << "SPEED x" << (10000.0 / game_seconds_per_tick);
  time_speed_msg = tmp_str.str();
  tmp_str.str("");
  tmp_str << "1: 1.1";
  /*      tmp_str >> *date_msg;*/
  date_msg = tmp_str.str();
  tmp_str.str("");
  /*      if (result < 0) {
   std::cout << "ERR IN sprintf() call!   :-(" << std::endl;
   al_destroy_display(display);
   SDL_RemoveTimer(ticker_timer);
   SDL_RemoveTimer(enemyAI_timer);
   return -1;
   }*/
  
  int x_settings[NUM_OPTIONS];
  std::string setting_names[NUM_OPTIONS];
  size_t ind;
  for (ind=0; ind < NUM_OPTIONS; ind ++) {
    x_settings[ind] = (21*global::screen_width) / 40;
  }
  setting_names[0] = "FoV:";
  setting_names[1] = "# of Units:";
  setting_names[2] = "Sound FX Vol:";
  setting_names[3] = "Music Vol:";
  setting_names[4] = "World Map:";
/*  bool global_redraw = false;*/
  bool esc_menu = false;
  
  int rtn_result = (-1);
  /* Now initiate the OUTER TMB state machine */
  while (game_state > 0) {
    if (game_state == 11) { // brief logo screen
      int logo_wid=0, logo_hgt=0;
      SDL_QueryTexture(logo, nullptr, nullptr, &logo_wid, &logo_hgt);
      SDL_Texture *loading = SDL_CreateTexture(display,
                                               SDL_PIXELFORMAT_RGBA8888,
                                               SDL_TEXTUREACCESS_TARGET,
                                               global::screen_width,
                                               global::screen_height);
      
      SDL_SetRenderTarget(display, loading);
      SDL_SetRenderDrawColor(display, 2, 2, 2, ind/16);
      SDL_RenderClear(display);
      SDL_SetRenderDrawColor(display, 250, 250, 250, 255/*ind/16*/);
      
      SDL_SetRenderTarget(display, nullptr);
      SDL_QueryTexture(logo, nullptr, nullptr, &(dframe->w), &(dframe->h));
      dframe->x = (global::screen_width - logo_wid)/2;
      dframe->y = (global::screen_height - logo_hgt)/3;
      SDL_RenderCopy(display, logo, nullptr, dframe);
      FC_Draw(white_luxirb_med, display, 3*global::screen_width/10,
              global::screen_height/4, "Strange Games Development Team");
      
      /*          dframe->x = (global::screen_width - logo_wid)/4;
       dframe->y = 3*(global::screen_height - logo_hgt)/4;
       SDL_QueryTexture(loading, nullptr, nullptr, &(dframe->w), &(dframe->h));
       SDL_RenderCopy(display, loading, nullptr, dframe);*/
      SDL_RenderPresent(display);
      SDL_Delay(1000);
      SDL_Delay(wait_time);
      
      SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
      SDL_RenderClear(display);
      SDL_SetRenderTarget(display, loading);
      SDL_SetRenderDrawColor(display, 2, 2, 2, ind/16);
      SDL_RenderClear(display);
      //          SDL_SetRenderDrawColor(display, 250, 250, 250, 255/*ind/16*/);
      
      SDL_SetRenderTarget(display, nullptr);
      SDL_Color white_text = {250, 250, 250, 255};
      FC_Draw(white_luxirb_med, display, 3.4*global::screen_width/8,
              2.0f*global::screen_height/8, "Music:");
      FC_Draw(white_luxirb_med, display, 3.2*global::screen_width/8,
              3.0f*global::screen_height/8, "Lobo Loco ");
      FC_Draw(white_luxirb_med, display, 2.5*global::screen_width/8,
              4.0f*global::screen_height/8, "Snowmelt Yukkon River");
      FC_Draw(white_luxirb_med, display, 2.0*global::screen_width/8,
              5.0f*global::screen_height/8, "Creative Commons by Attribution");
      FC_Draw(white_luxirb_med, display, 3.0*global::screen_width/8,
              6.0f*global::screen_height/8, "Noncommercial");
      /*          al_draw_text(fc_luxirb_med, al_map_rgb(250, 250, 250),
       global::screen_width/2, 3.0f*global::screen_height/8, ALLEGRO_ALIGN_CENTER, "Pedro Santiago");
       FC_Draw(fc_luxirb_med, display, 1, 1, "Valle Grande -- \"Cosmic\"");
       FC_Draw(fc_luxirb_med, display, 1, 1, "Creative Commons by Attribution 4.0");*/
/*    SDL_QueryTexture(loading, nullptr, nullptr, &(dframe->w), &(dframe->h));
      dframe->x = (global::screen_width - logo_wid)/4;
      dframe->y = 3*(global::screen_height - logo_hgt)/4;
      SDL_RenderCopy(display, loading, nullptr, dframe);*/
      SDL_RenderPresent(display);
      SDL_Delay(1000);
      SDL_Delay(wait_time);
      SDL_SetRenderDrawColor(display, 2, 2, 2, 250);
      SDL_RenderClear(display);
      SDL_RenderPresent(display);
      game_state = 1;
    } else if (game_state == 1) {
      SDL_SetRenderDrawColor(display, 5, 126, 200, 255);
      SDL_RenderClear(display);
      SDL_RWops *title_screen_ops = SDL_RWFromFile
          ("res/title_screen_0-0-3-1_base.png", "rb");
      SDL_Rect entirety = {0, 0, static_cast<int>(global::screen_width), static_cast<int>(global::screen_height)};
      if (title_screen_ops == nullptr) {
        std::cout << "[FATAL]    RWFromFile (title screen!) returned a nullptr." <<
        " See the error: " << SDL_GetError() << std::endl;
        FC_Draw(font_luxirb, display, 2.5*global::screen_width/8,
                global::screen_height/4, "MultiGen Game");
        FC_Draw(font_luxirb, display, 2*global::screen_width/8,
                17.0f*global::screen_height/32, "Press 1 - Start new game");
        FC_Draw(font_luxirb, display, 1.6*global::screen_width/8,
                20.0f*global::screen_height/32, "Press 2 - Resume saved game");
        FC_Draw(font_luxirb, display, 2.4*global::screen_width/8,
                23.0f*global::screen_height/32, "Press 3 - Options");
        FC_Draw(font_luxirb, display, 1.5*global::screen_width/8,
                27.0f*global::screen_height/32, "Press ESC/Q - Quit to desktop");
        SDL_RenderPresent(display);
      } else {
        title_screen_surf = IMG_LoadPNG_RW(title_screen_ops);
        title_screen_text = SDL_CreateTextureFromSurface(display, title_screen_surf);
        SDL_RenderCopy(display, title_screen_text, nullptr, &entirety);
        SDL_RenderPresent(display);
      }
      while (game_state == 1/*global_redraw == false*/) {
        /* ^ global_redraw? What was I thinking? */
        SDL_Event ev;
        while (SDL_PollEvent(&ev)) {
          if (ev.type == SDL_KEYUP) {
            /* NOTE the +1 shift between the number enterred and the 
             * actual game_state set.  */
            if (ev.key.keysym.sym == SDLK_1) {
              game_state = 2;
/*              global_redraw = true;*/
            } else if (ev.key.keysym.sym == SDLK_2) {
              game_state = 4;
/*              global_redraw = true;*/
            } else if (ev.key.keysym.sym == SDLK_3) {
              game_state = 3;
/*              global_redraw = true;*/
            } else if ((ev.key.keysym.sym == SDLK_q) ||
                       (ev.key.keysym.sym == SDLK_ESCAPE)) {
              game_state = -4;
/*              global_redraw = true;*/
            }
          } else if ((ev.type == SDL_WINDOWEVENT) &&
                     (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
            if (game_state == 1) {
              /* no-op for now */
              SDL_SetRenderDrawColor(display, 5, 126, 200, 250);
              SDL_RenderClear(display);
//              FC_Draw(font_luxirb, display, 2.5*global::screen_width/8,
//                      global::screen_height/4, "MultiGen Game");
//              FC_Draw(font_luxirb, display, 2*global::screen_width/8,
//                      17.0f*global::screen_height/32, "Press 1 - Start new game");
//              FC_Draw(font_luxirb, display, 1.6*global::screen_width/8,
//                      20.0f*global::screen_height/32, "Press 2 - Resume saved game");
//              FC_Draw(font_luxirb, display, 2.4*global::screen_width/8,
//                      23.0f*global::screen_height/32, "Press 3 - Options");
//              FC_Draw(font_luxirb, display, 1.5*global::screen_width/8,
//                      27.0f*global::screen_height/32, "Press ESC/Q - Quit to desktop");
//              SDL_RenderPresent(display);
              SDL_RenderCopy(display, title_screen_text, nullptr, &entirety);
              SDL_RenderPresent(display);
            }
          } else if ((ev.type == SDL_WINDOWEVENT) &&
                     (ev.window.event == SDL_WINDOWEVENT_RESIZED)) {
            SDL_SetRenderDrawColor(display, 5, 126, 200, 250);
            SDL_RenderClear(display);
            SDL_RenderCopy(display, title_screen_text, nullptr, &entirety);
            SDL_RenderPresent(display);
          } else if (ev.type == SDL_WINDOWEVENT &&
                     ev.window.event == SDL_WINDOWEVENT_CLOSE) {
            game_state = -4;
          } else if (ev.type == SDL_MOUSEBUTTONUP) {
            int xClick = ev.button.x;
            bool show_creds = false;
/*            int yClick = ev.button.y;*/
            if (ev.button.y+100 > global::screen_height) {
              if (xClick < (global::screen_width / 6)) {
                /* selected "NEW" */
                game_state = 2;
/*                global_redraw = true;*/
              } else if (xClick < (2*global::screen_width / 6)) {
                /* selected "LOAD" */
                game_state = 4;
/*                global_redraw = true;*/
              } else if (xClick < (global::screen_width/2)) {
                /* selected "OPTIONS" */
                game_state = 3;
/*                global_redraw = true;*/
              } else if (xClick > ((2*global::screen_width) / 3)) {
                if (xClick > ((5*global::screen_width) / 6)) {
                  /* "EXIT" was selected */
                  game_state = -4;
/*                  global_redraw = true;*/
                } else {
                  /* TODO Display the License and Credits! */
/*                  game_state;*/
                  show_creds = !show_creds;
//                  if (show_creds) {
//                    al_draw_filled_rectangle(20, 20, global::screen_width-20,
//                      global::screen_height-120, al_map_rgba(233,233,233,112));
//                    al_draw_text(font_luxirb, al_map_rgb(2, 2, 2),
//                      global::screen_width/2, 7.0f*global::screen_height/32,
//                      ALLEGRO_ALIGN_CENTER, "GNU GPL'd by chocorho");
//                    al_draw_text(font_luxirb, al_map_rgb(2, 2, 2),
//                      global::screen_width/2, 10.0f*global::screen_height/32,
//                      ALLEGRO_ALIGN_CENTER, "Special thanks to contributors");
//                    al_flip_display();
//                  }
                }
              }
            }
          }
        }
      }
    } else if (game_state == 2) {
      /* set up initial values here? Or perhaps somewhere else to reduce delay... */
      bool music = true;
      tmp_str.str("");
      /*          tmp_str << "SPEED x" << (10000.0f / game_seconds_per_tick);*/
      tmp_str << date[0] << ": " << date[1] << "." << date[2];
      /*          tmp_str >> *date_msg;*/
      date_msg = tmp_str.str();
      visual_type = MAP;
      draw_lr_quad(this/*display, lower_right_canvas, map_img, *player_units,
                        *chosen_ones, *chosen_indices, unit_disp_ind, 0, subfont_luxirb_small*/);
/*      draw_ll_quad(display, lower_left_canvas, map_img, 0, subfont_luxirb_small);*/
#ifdef NEW_UI
      SDL_SetRenderTarget(display, right_panel_canvas);
#else
      SDL_SetRenderTarget(display, lower_left_canvas);
#endif
#ifdef HALLOWEEN
      SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
#else
      SDL_SetRenderDrawColor(display, 2, 100, 2, 100);
#endif
      SDL_RenderClear(display);
      SDL_SetRenderTarget(display, nullptr);
      
      /* Initial game state: a temporary screen */
      SDL_SetRenderDrawColor(display, 5, 126, 200, 255);
      SDL_RenderClear(display);
      SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
      FC_Draw(font_luxirb, display, global::screen_width/3,
              2*global::screen_height/5, "WORLD STRATEGY LAYER");
      FC_Draw(fc_luxirb_med, display, 2*global::screen_width/3,
              4*global::screen_height/5, "[now loading...]");
      SDL_RenderPresent(display);
      
      Mix_Music *worldMapTheme = Mix_LoadMUS("res/music/Lobo_Loco_-_11_-_Snowmelt_Yukkon_River.ogg");
      if (worldMapTheme != nullptr) {
        int stat = Mix_PlayMusic(worldMapTheme, 1);
        if (stat != 0) {
          std::cerr << "[ ERR ]    Music would not be played." << std::endl;
          std::cerr << SDL_GetError() << std::endl;
        }
      } else {
        std::cerr<<"[ ERR ]    Music file not found!"<<std::endl;
      }
      
      deep_redraw = false;
      /* major game loop (MGL) (mgl) */
      while (game_state >= 0) {
        if (paused || esc_menu) {
          /* Note that if the game is paused, the opponent AI
           * (even though it might queue up some decisions in a future version)
           * does not take any actions because the game has not ticked forward
           * yet. TODO only allow the user to pause for some finite amount of time,
           * with a cooldown. */
          while ((game_state >= 0) && (paused || esc_menu)) {
            /* Get next event and respond to it */
            SDL_Event ev;
            while (SDL_PollEvent(&ev)) {
              if (ev.type == SDL_KEYDOWN) {
                if ((ev.key.keysym.sym == SDLK_SPACE) /* ||
                    (ev.key.keysym.sym == SDLK_r)*/) {
                  /* Unpause the WMSL */
                  setPaused(false);
                  if (game_state == 1) {
                    game_state = 2;
                  }
                } else if (ev.key.keysym.sym == SDLK_ESCAPE ||
                           (ev.key.keysym.sym == SDLK_r && esc_menu)) {
                  /* Toggle escape menu */
                  if (game_state == 1) {
                    game_state = 2;
                  } else if (game_state == 2) {
                    game_state = 1;
                  }
                  esc_menu = !(esc_menu);
                  deepRepaint();
                } else if (ev.key.keysym.sym == SDLK_q) {
                  /* Quit the WMSL */
                  game_state = -3;
                  std::cout << "\"Q\" Key has been pressed (exiting)" << std::endl;
                } else if ((ev.key.keysym.sym == SDLK_x)
                           && (esc_menu)) {
                  FILE *stored_data = fopen("res/savedgamedata/log1.txt", "w");
                  fprintf(stored_data, "Date=%s\n", date_msg.c_str());
                  /*                      fprintf(stored_data, "" << std::endl;*/
                  fclose(stored_data);
                  setPaused(false);
                  game_state = -3;
                } else if (ev.key.keysym.sym == SDLK_m) {
                  if (music) {
                    music = false;
                    Mix_FadeOutMusic(500);
                  } else {
                    music = true;
                    Mix_FadeInMusic(worldMapTheme, 1, 500);
                  }
                }
              } else if ((ev.type == SDL_WINDOWEVENT) &&
                         (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
                if (game_state == 1) {
                  /* no-op for now */
                  /*                    } else if (visual_type == ARMY) {
                   deepRepaint();*/
                } else {
                  repaint();
                }
              } else if (ev.type == SDL_WINDOWEVENT &&
                         ev.window.event == SDL_WINDOWEVENT_CLOSE) {
                game_state = -3;
              } else if ((ev.type == SDL_WINDOWEVENT) &&
                         (ev.window.event == SDL_WINDOWEVENT_RESIZED)) {
                repaint();
              } else if (ev.type == SDL_MOUSEBUTTONDOWN) {
                int xClick = ev.button.x;
                int yClick = ev.button.y;
                if (ev.button.button == SDL_BUTTON_LEFT) {
#ifdef NEW_UI
       /*} else */if (yClick > dy && xClick < leftPanelBound) {
                    if ((yClick-dy) < 0.07*(global::screen_height - dy)) {
                      visual_type = ARMY;
#else
/*                if ((xClick < leftPanelBound) && (yClick > dy)) {
                    // no-op for now... the UP-click will deal with this
         } else*/ if (yClick < 40) {
                    if ((xClick < 0.159*global::screen_width) && (xClick > 0.06*global::screen_width)) {
                      visual_type = MAP;
#endif
                      draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                        *player_units, *chosen_ones, *chosen_indices, unit_disp_ind, 0, subfont_luxirb_small*/);
#ifdef NEW_UI
                    } else if ((yClick-dy) < 0.13*(global::screen_height - dy)) {
                      visual_type = MAP;
#else
/*                  } else if (xClick < (6.8f*global::screen_width/16)) {*/
                    } else if ((xClick < 0.28*global::screen_width) && (xClick > 0.185*global::screen_width)) {
                      visual_type = ARMY;
#endif
/*                      SDL_SetRenderTarget(display, lower_right_canvas);*/
/*                      SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);*/
/*                      SDL_RenderClear(display);*/
                      draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                        *player_units, *chosen_ones, *chosen_indices,
                                        unit_disp_ind, 1, subfont_luxirb_small*/);
#ifdef NEW_UI
                    } else if ((yClick-dy) < 0.22*(global::screen_height - dy)) {
#else
                    } else if ((xClick < 0.48*global::screen_width) && (xClick > 0.310*global::screen_width)) {
#endif
/*                    } else if (xClick < (10.8f*global::screen_width/16)) {*/
                      visual_type = RESEARCH;
                      draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                        *player_units, *chosen_ones, *chosen_indices,
                                        unit_disp_ind, 2, subfont_luxirb_small*/);
                      /*                        std::cout << "Just set \'visual_type\' to 2!" << std::endl;*/
#ifdef NEW_UI
                    } else if ((yClick-dy) < 0.30*(global::screen_height - dy)) {
#else
                    } else if ((xClick < 0.75*global::screen_width) && (xClick > 0.611*global::screen_width)) {
#endif
                      /*                      } else if (xClick < (10.8f*global::screen_width/16)) {*/
                      visual_type = HOMEFRONT;
                      draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                        *player_units, *chosen_ones, *chosen_indices,
                                        unit_disp_ind, 3, subfont_luxirb_small*/);
                      /*                        std::cout << "Just set \'visual_type\' to 3!" << std::endl;*/
                    }
                  }
                  repaint();
/*                  deepRepaint();*/
#ifdef NEW_UI
                } else if (visual_type == ARMY && ev.button.button == SDL_BUTTON_RIGHT) {
#else
                } else if (visual_type == ARMY && ev.button.button == SDL_BUTTON_LEFT) {
#endif
                  /* insert selected unit into chosen units */
                  /*                      int x_coord = xClick-leftPanelBound;*/
                  int y_coord = yClick-dy;
                  unsigned int choice = y_coord / TABLE_ROW_HEIGHT;
                  if ((y_coord > 0) && (choice > 1) && (choice < 2+NUM_UNITS_PER_PAGE)
                      && ((unit_disp_ind+choice-2) < player_units.size())) {
                    SDL_SetRenderTarget(display, lower_right_canvas);
#ifdef HALLOWEEN
                    SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
                    SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);
#endif
                    SDL_RenderClear(display);
                    const SDL_Rect rect_locate = {
                        1, static_cast<int>(choice*TABLE_ROW_HEIGHT+1),
                        static_cast<int>(global::screen_width-leftPanelBound-1),
                        TABLE_ROW_HEIGHT
                    };
                    draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                      *player_units, *chosen_ones, *chosen_indices, unit_disp_ind,
                                      1, subfont_luxirb_small*/);
//                  SDL_SetRenderDrawColor(display, 255, 140, 1, 90);
//                  SDL_RenderFillRect(display, &rect_locate);
                    
                    Unit *chosen = player_units.at(unit_disp_ind+choice-2);
                    if (chosen->isChosen()) {
                      chosen_ones.erase(std::remove/*_if*/(chosen_ones.begin(),
                                 chosen_ones.end(), chosen),
                                 chosen_ones.end());
                    } else {
                      chosen_ones.push_back(chosen);
                    }
                    chosen->flipChosen();
                    SDL_SetRenderTarget(display, nullptr);
                  }
                  deepRepaint();
                } /*else {
                   std::cout << "Mouse button #" << ev.mouse.button << std::endl;
                   }*/
                /*                std::cout << "Just clicked DOWN at " << ((float)xClick / global::screen_width)) << " percent of the global::screen_width" << std::endl;*/
              } else if (ev.type == SDL_MOUSEBUTTONUP) {
                int xClick = ev.button.x;
                int yClick = ev.button.y;
                if (ev.button.button == SDL_BUTTON_LEFT) {
/*                  std::cout << "Identified a mouse unclick at (" << xClick << ", " << yClick << ")" << std::endl;*/
#ifdef NEW_UI
                  if ((xClick > leftPanelBound) &&
                      (xClick < rightPanelBound) && (yClick > dy)) {
#else
                  if ((xClick > leftPanelBound) && (yClick > dy)) {
#endif
                    /*bool was_red = false;*/
                    /*  TODO work on this algorithm...
                     *  there are more times when we may want to color a
                     *  province white (NOT ONLY when the same PIXEL POINT is
                     *   clicked twice in a row,
                     *  BUT ALSO more generally when the same PROVINCE is
                     *  selected.. right?) */
                    if (visual_type == MAP) {
                      int x_coord = xClick-leftPanelBound-global::x_adjust;
                      int y_coord = yClick-dy-global::y_adjust;
                      /*                          Uint8 red_ext, green_ext, blue_ext;*/
                      Uint8 red_in, green_in, blue_in;
                      
                      dframe->x = (int)(x_coord/scale_factor);
                      dframe->y = (int)(y_coord/scale_factor);
                      dframe->w = 1;
                      dframe->h = 1;
                      int aggregateColVal = Util::getPixel(internal_map_surf, dframe->x, dframe->y);
/*                    std::cout << SDL_GetPixelFormatName(internal_map_surf->format->format);*/
/*                    std::cout << std::endl;*/
                      SDL_GetRGB(aggregateColVal, rgb_format24,
                                 &red_in, &green_in, &blue_in);
                      /*                          SDL_UnlockTexture(internal_map_img);*/
                      /*                          std::cout << "[DEBUG]    " << (int)(red_in) << ", " <<
                       (int)(green_in) << ", " << (int)(blue_in) << std::endl;*/
                      /*                          SDL_SetRenderTarget(display, lower_right_canvas);*/
                      //                          if ((red_ext == 255) && (green_ext == 2) && (blue_ext == 6)) {
                      //                            if (last_selected.x != (-1)) {
                      //                              SDL_SetRenderDrawColor(display, 255, 255, 255, 255);
                      ////                              fillEdge(last_selected.x-leftPanelBound, last_selected.y-dy, -1, lower_right_canvas, ;
                      //                            }
                      //  /*                          was_red = true;*/
                      //                          } else if ((blue_ext != 232) || (green_ext != 162) || (red_ext >=10)) {
                      SDL_SetRenderDrawColor(display, 255, 2, 6, 255);
                      //                            fillEdge(x_coord, y_coord, -1, lower_right_canvas, ;
                      /*                        std::cout << "\t\tcomparing " << last_selected.x << " to " << xClick << std::endl;*/
                      if ((last_selected.x != xClick) && (last_selected.x >= 0)) {
                        SDL_SetRenderDrawColor(display, 255, 255, 255, 255);
                        //                              fillEdge(last_selected.x-leftPanelBound, last_selected.y-dy, -1, lower_right_canvas, ;
                      }
                      unsigned int prov_id;
                      prov_id = Util::hash(red_in, green_in, blue_in, 0);
/*                      std::cout << "[DEBUG]    Comparing " << prov_id;
                      std::cout << " to " << num_provinces << " (province count)" << std::endl;*/
/*                      if (prov_id >= num_provinces) {
                        prov_id = Util::hash(red_in, green_in, blue_in, 0);
                        if (prov_id >= num_provinces) {
                          prov_id = 0;
                        }
                      }*/
                      if (prov_id < num_provinces) {
#ifdef DEBUG_MSG
                        std::cout << "[DEBUG] According to the hash, the province ID should be " << prov_id << std::endl;
#endif
                        Province *selected = provinces[prov_id];
                        const std::string &name = selected->getName();
/*                        std::cout << "[DEBUG]    Province name is " << name << std::endl;*/
/*                        Util::get_status_text(stat_arr, selected->getStatus());*/
/*                        std::string *stat = Util::get_status_str(selected->getStatus());*/
#ifdef NEW_UI
                        SDL_SetRenderTarget(display, right_panel_canvas);
#else
                        SDL_SetRenderTarget(display, lower_left_canvas);
#endif
#ifdef HALLOWEEN
                        SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
#else
                        SDL_SetRenderDrawColor(display, 2, 100, 2, 100);
#endif
                        SDL_RenderClear(display);
                        FC_Draw(subfont_luxirb_small, display, 3, 3,
                                "PROVINCE DATA");
                        FC_Draw(subfont_luxirb_small, display, 3,
                                (0.10)*(global::screen_height - dy), name.c_str());
                        FC_Draw(subfont_luxirb_small, display, 3,
                                (0.15)*(global::screen_height - dy), Util::stat_msgs[selected->getStatus()]);
                        unsigned int n = selected->getSettlements()->size();
                        
                        /* TODO change to a for-each loop
                         * like all the smart C++ people would do. */
                        for (ind=0; ind < n; ind ++) {
                          FC_Draw(subfont_luxirb_small, display, 3,
                                  (0.20+(0.05*(ind)))*(global::screen_height-dy),
                                  selected->getSettlements()->at(ind)->getName().c_str());
                        }
                        int line_y = (7.0*global::screen_height) / 8.0;
                        SDL_SetRenderDrawColor(display, 250, 250, 250, 255);
                        SDL_RenderDrawLine(display, 2, line_y-dy, leftPanelBound-2, line_y-dy);
                        
                        int invade_settlement_w = 0, invade_settlement_h = 0;
                        SDL_QueryTexture(invade_settlement_img, nullptr,
                                         nullptr, &invade_settlement_w,
                                         &invade_settlement_h);
                        int buf_y = (global::screen_height - dy - line_y - invade_settlement_h) / 2;
                        if (buf_y < 0) {
                          buf_y = 0;
                        }
                        /*std::cout << "" << std::endl;
                        std::cout << "global::screen_height : " << global::screen_height << std::endl;
                        std::cout << "dy : " << dy << std::endl;
                        std::cout << "line_y : " << line_y << std::endl;
                        std::cout << "height : " << al_get_bitmap_height(invade_settlement_img) << std::endl;
                        std::cout << "Buffer in the y dir is " << buf_y << std::endl;*/
                        int buf_x = (leftPanelBound - invade_settlement_w) / 2;
                        dframe->w = invade_settlement_w;
                        dframe->h = invade_settlement_h;
                        dframe->x = buf_x;
                        dframe->y = line_y - dy + buf_y;
                        SDL_RenderCopy(display, invade_settlement_img, nullptr, dframe);
                        last_selected.x = xClick;
                        last_selected.y = yClick;
                        last_prov = prov_id;
                      }
//                          }
                      /*if ((last_selected.x != xClick) && (last_selected.x != (-1))) {
                       SDL_SetRenderDrawColor(display, 255, 255, 255), 255);
                       fillEdge(last_selected.x-leftPanelBound, last_selected.y-dy, -1, lower_right_canvas, ;
                       }*/
/*                      free(pixel);*/
/*                      free(int_pixel);*/
                      SDL_SetRenderTarget(display, nullptr);
                      SDL_QueryTexture(lower_left_canvas, nullptr, nullptr,
                                       &(dframe->w), &(dframe->h));
                      dframe->x = 0;
                      dframe->y = dy;
                      SDL_RenderCopy(display, lower_left_canvas, nullptr, dframe);
                      
                      SDL_QueryTexture(lower_right_canvas, nullptr, nullptr,
                                       &(dframe->w), &(dframe->h));
                      dframe->x = leftPanelBound;
                      dframe->y = dy;
                      SDL_RenderCopy(display, lower_right_canvas, nullptr, dframe);
#ifdef NEW_UI
                      SDL_QueryTexture(right_panel_canvas, nullptr, nullptr,
                                       &(dframe->w), &(dframe->h));
                      dframe->x = rightPanelBound;
                      SDL_RenderCopy(display, right_panel_canvas, nullptr, dframe);
#endif
/*                      std::cout << "Should have just colored the selected province." << std::endl;
                       std::cout << "\tagain, mouse unclick occurred at ("
                       << xClick << ", " << yClick << ")" << std::endl;*/
                    } else if (visual_type == ARMY/*(visual_type % 2) == 1*/) {
                      int x_coord = xClick - leftPanelBound;
                      int y_coord = yClick - dy;
                      size_t choice = unit_disp_ind
                          + (y_coord / TABLE_ROW_HEIGHT);
                      if (choice > 0) {
                        if (displayUnitInfo(x_coord, y_coord, choice)) {
                          repaint();
/*                          deepRepaint();*/
                        }
//                        SDL_SetRenderTarget(display, nullptr); // should be redundant but idk wtf is happening
//                        SDL_QueryTexture(lower_left_canvas, nullptr, nullptr, &(dframe->w), &(dframe->h));
//                        dframe->x = 1;
//                        dframe->y = dy;
//                        SDL_RenderCopy(display, lower_left_canvas, nullptr, dframe);
//                        SDL_RenderPresent(display);
                      } else {
                        /*** Sort! ***/
                        /* make a lambda for each search criteria;
                         * decide which lambda to use... */
                        auto by_name = [] (Unit *a, Unit *b) -> bool { return (a->getName() < b->getName()); };
                        auto by_age = [] (Unit *a, Unit *b) -> bool { return (a->getAge() > b->getAge()); };
                        auto by_level = [] (Unit *a, Unit *b) -> bool {
                          //  this can be rewritten succinctly using std::tuple
                          if (a->getLevel() > b->getLevel()) {
                            return true;
                          } else if (a->getLevel() < b->getLevel()) {
                            return false;
                          } else {
                            return a->getExp() > b->getExp();
                          }
                        };
                        auto by_loc = [] (Unit *a, Unit *b) -> bool {
                          int aProv = a->getProvId();
                          int bProv = b->getProvId();
                          if (aProv < bProv) {
                            return true;
                          } else if (aProv > bProv) {
                            return false;
                          } else {
                            return (a->getSettlId() < b->getSettlId());
                          }
                        };
                        auto by_chosen = [&by_level, &by_loc] (Unit *a, Unit *b) -> bool {
/*                          return (a->isChosen() && !(b->isChosen()));*/
                          if (a->isChosen()) {
                            if (b->isChosen()) {
                              return by_level(a, b);
                            } else {
                              return true;
                            }
                          } else {
                            if (b->isChosen()) {
                              return false;
                            } else {
/*                              return by_level(a, b);*/
                              return by_loc(a, b);
                            }
                          }
                        };
                        if (x_coord < LR_QUAD_AGE_HDR_X) {
                          std::sort(player_units.begin(), player_units.end(), by_name);
                        } else if (x_coord < LR_QUAD_LEVEL_HDR_X) {
                          std::sort(player_units.begin(), player_units.end(), by_age);
                        } else if (x_coord < LR_QUAD_LOCATION_HDR_X) {
                          std::sort(player_units.begin(), player_units.end(), by_level);
                        } else if (x_coord < LR_QUAD_CHOSEN_HDR_X) {
                          std::sort(player_units.begin(), player_units.end(), by_loc);
                        } else /*if (x_pixel < )*/ {
                          std::sort(player_units.begin(), player_units.end(), by_chosen);
                        }
                        deepRepaint();
                      }
                    }
#ifdef NEW_UI
                  } else if ((xClick > rightPanelBound) && (yClick > ((7*global::screen_height) / 8))) {
#else
                  } else if ((xClick < leftPanelBound) && (yClick > ((7*global::screen_height) / 8))) {
#endif
                    /* Invade a Settlement */
                    if (visual_type == MAP) {
                      int pause_result = 0;
                      /* Get the player's selection. * 
                       * Speaking of which: */
                      /* "Now, there's no secret to toasting perfection;
                       *  there's a dial on the side and you make your selection." */
                      /*                          Ghost_WMSL *bg_data = new Ghost_WMSL(display,
                       lower_left_canvas, lower_right_canvas,
                       fc_luxirb_med, date_msg, true);*/
                      SDL_Thread *bg_t = SDL_CreateThread(minimal_pause,
                                                          "[Paused] World Map Strategy Layer", this/*bg_data*/);
                      
/*                    SDL_Window *secondary_wind = SDL_CreateWindow("Settlement Invasion Selection", global::screen_width, 0, global::screen_width, global::screen_height, SDL_WINDOW_OPENGL|SDL_WINDOW_INPUT_GRABBED);*/
/*                    child_windows->push_back(secondary_wind);*/
                      InvadeSettleDisplay *settlement_choice = new
                      InvadeSettleDisplay(window, (3*leftPanelBound/2), provinces[last_prov],
                                          map_img_surf, internal_map_surf);
                      /*      InvadeSettleDisplay(ALLEGRO_DISPLAY *disp_p, int del, Province *prov,
                       SDL_Texture *world_img, SDL_Texture *internal) : */
                      SDL_SetWindowTitle(window, "MultiGen Game [PAUSED]");
                      int settl_choice = settlement_choice->launch();
                      SDL_SetWindowTitle(window, "MultiGen Game");
                      delete settlement_choice;
                      
                      SDL_LockMutex(redraw_control);
                      SDL_LockMutex(thread_control);
                      redraw = true;
                      thread_state = 0;
                      SDL_UnlockMutex(thread_control);
                      SDL_UnlockMutex(redraw_control);
                      SDL_WaitThread(bg_t, &pause_result);
/*                    delete bg_data;*/
                      
                      /* IF a settlement was in fact chosen... */
                      if (settl_choice >= 0) {
                        Field *field = provinces[last_prov]->getField();
                        Settlement *seige = provinces[last_prov]->getSettlements()->at(settl_choice);
                        
                        /* ...and if the settlement is enemy-owned... */
                        if (seige->getOccupierEnum() == ENEMY_OWNED) {
                          /* ...then actually initiate the battle. */
                          
                          /* Calculate relative enemyAI strength, to get number of enemy units */
                          unsigned int num_e_units = (enemy_provinces * SCALE) / (num_provinces);
                          std::vector<Unit *> &en_units_needed = seige->units();
                          /*                              std::cout << "[DEBUG]    enemy_provinces == "
                           << enemy_provinces << std::endl;
                           std::cout << "[DEBUG]    SCALE == "
                           << SCALE << std::endl;
                           std::cout << "[DEBUG]    num_provinces == "
                           << num_provinces << std::endl;
                           std::cout << "[DEBUG]    Just calculated "
                           <<"en str as " << num_e_units << std::endl;*/
                          
                          /* Now determine enemy UCs and then spawn them for real... */
                          std::stringstream name_setup;
                          for (ind = en_units_needed.size(); ind < num_e_units; ind ++) {
                            int exit_code;
                            name_setup.str("");
                            name_setup << "Test Baddy " << ind;
                            /*Soldier*/Unit *next_e = new Unit(SOLDIER, 3, 4, 1, name_setup.str().c_str());
                            Sword *swordInst = new Sword(3, 4, 8);
                            std::stringstream pic_name3;
                            pic_name3 << "res/nic-cage-" << (generator() % 5) << ".png";//".jpg";
                            next_e->setSprite(pic_name3.str());
                            /*  testSoldier0->setSprite("res/ghost-0.png");*/
                            exit_code = next_e->setWeapon(swordInst, false);
                            if (!exit_code) {
                              std::cout << "I'm afraid I've got some bad news boys.\n\tCouldn't give enemy soldier a Sword." << std::endl;
                            }
                            en_units_needed.push_back(next_e);
                          }
                          
                          /* Start a (Rout?) TMB at this location */
                          /* TODO call intelligent map decision */
                          std::stringstream map_name;
                          int map_num = generator() % NUM_TMB_MAP_CHOICES;
                          map_name << "res/maps/unsorted/map" << map_num << ".map";
                          map_name.flush();
                          RoutTMB *scheduled = new RoutTMB
                          (map_name.str().c_str(), "res/terrains.conf",
                                field->player_units(), en_units_needed, 0);
                          
                          std::cout << "Field units identified:" << field->player_units().size() << std::endl;
                          for (unsigned int it = 0; it < field->player_units().size(); it ++) {
                            std::cout << "    " << field->player_units().at(it)->getName();
                          }
                          std::cout << std::endl;
                          
                          std::cout << "Enemy units identified:" << en_units_needed.size() << std::endl;
                          for (unsigned int it = 0; it < en_units_needed.size(); it ++) {
                            std::cout << "    " << en_units_needed.at(it)->getName();
                          }
                          std::cout << std::endl;
                          /* TODO delete the en_units_needs vector at the proper time? */
                          /* OR else...
                           make enums for TMB type, to be able to know that
                           you need to delete debilEnemyUnits later on?
                           What about Special "Commander"-type Enemy units?
                           Put them in a separate vector?
                           Suffice it to say, a lot needs to be worked out here. */
                          scheduled->setSettlementName(seige->getName());
                          pending_battles->push_back(scheduled);
                          prov_ids->push_back(last_prov);
                        } else {
                          /* if the chosen Settlement IS player-owned...
                           * TODO Display a message here (maybe even a Victory
                           * screen), and then move units into the settlement */
                          std::vector<Unit *> &en_units_needed = seige->units();
                          /* Start a (Rout?) TMB at this location */
                          std::stringstream map_name;
                          /* TODO call intelligent map decision */
                          int map_num = generator() % NUM_TMB_MAP_CHOICES;
                          map_name << "res/maps/unsorted/map" << map_num << ".map";
                          RoutTMB *scheduled = new RoutTMB
                          (map_name.str().c_str(), "res/terrains.conf",
                             field->player_units(), en_units_needed, 0);
                          scheduled->setSettlementName(seige->getName());
                          pending_battles->push_back(scheduled);
                          prov_ids->push_back(last_prov);
                        }
                      }
                    }
                  }
/*                  deepRepaint();*/
                  repaint();
                }
              } else if (ev.type == SDL_MOUSEWHEEL) {
                /* Note: I actually check the visual_type
                 * INSIDE the scroll_*() methods.
                 * If this is a bad design choice, then
                 * uncomment the following: */
/*                if (visual_type == ARMY) {*/
                  if (ev.wheel.y > 0) {
                    scroll_up();
                    deepRepaint();
                  } else if (ev.wheel.y < 0) {
                    scroll_down();
                    deepRepaint();
                  }
/*                } else if (visual_type == HOMEFRONT) {
                  if (ev.wheel.y > 0) {
                    scroll_up();
                    deepRepaint();
                  } else if (ev.wheel.y < 0) {
                    scroll_down();
                    deepRepaint();
                  }
                }*/
              } else if (ev.type == SDL_USEREVENT) {
                void (*periodicFunc) (void *) = (void (*)(void *)) ev.user.data1;
                //                    if (SDL_RemoveTimer(ticker_timer) == SDL_TRUE) {
                //                      // OK
                //                    } else {
                //                      std::cerr << "[ ERR ]    Could not remove ticker timer to replace it!" << std::endl;
                //                    }
                //                    if (SDL_RemoveTimer(enemyAI_timer) == SDL_TRUE) {
                //                      // OK
                //                    } else {
                //                      std::cerr << "[ ERR ]    Could not remove ticker timer to replace it!" << std::endl;
                //                    }
                periodicFunc(ev.user.data2);
                //                    ticker_timer = SDL_AddTimer(game_seconds_per_tick,
                //                      processBattles, ((void *) this));
                //                    if (ticker_timer == 0) {
                //                      std::cerr<<"[DEBUG]    Holy sh*t. Ticker timer could not be re-animated."<<std::endl;
                //                    }
                //                    enemyAI_timer = SDL_AddTimer(game_seconds_per_tick,
                //                      processWmslAI, this);
                //                    if (enemyAI_timer == 0) {
                //                      std::cerr<<"[DEBUG]    Holy sh*t. EnemyAI timer could not be re-animated."<<std::endl;
                //                    }
              }
            }
            SDL_LockMutex(redraw_control);
            if (/*global_*/redraw) {
              /*global_*/redraw = false;
/*            SDL_QueryTexture(lower_right_canvas, nullptr, nullptr, &(dframe->w), &(dframe->h));
              dframe->x = 0;
              dframe->y = 0;
              SDL_RenderCopy(display, lower_right_canvas, nullptr, dframe);*/
              if (display == nullptr) {
                std::cout << "Looks like I found the problem. `display` equals nullptr!" << std::endl;
              }
              SDL_SetRenderTarget(display, nullptr);
#ifdef HALLOWEEN
              SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
              SDL_SetRenderDrawColor(display, 255, 255, 255, 255);
#endif
              SDL_RenderClear(display);
              
              /* First: drawing the headers */
              /* TODO replace with image icons? Makes more space */
#ifdef NEW_UI
              SDL_SetRenderTarget(display, lower_left_canvas);
              FC_Draw(white_luxirb_sm, display,
                      40, 0.02*global::screen_height, "ARMY");
              
              FC_Draw(white_luxirb_sm, display,
                      40, 0.08*global::screen_height, "MAP  ");
              
              FC_Draw(white_luxirb_sm, display,
                      40, 0.14*global::screen_height, "RESEARCH");
              
              FC_Draw(white_luxirb_sm, display,
                      40, 0.20*global::screen_height, "HOMEFRONT");
              
              SDL_SetRenderTarget(display, nullptr);
#else
/*              dframe->y = 1;
              dframe->h = 41;
              dframe->x = 0.08*global::screen_width;
              dframe->w = 0.08*global::screen_width;
              SDL_RenderCopy(display, mapText, nullptr, dframe);*/
              FC_Draw(fc_luxirb_med, display, 0.08*global::screen_width, 1, "MAP");
              
/*              dframe->x = 0.18*global::screen_width;
              dframe->w = 0.11*global::screen_width;
              SDL_RenderCopy(display, armyText, nullptr, dframe);*/
              FC_Draw(fc_luxirb_med, display, 0.18*global::screen_width, 1, "ARMY");
              
/*              dframe->x = 0.33*global::screen_width;
              dframe->w = 0.23*global::screen_width;
              SDL_RenderCopy(display, researchText, nullptr, dframe);*/
              FC_Draw(fc_luxirb_med, display, 0.33*global::screen_width, 1, "RESEARCH");
              
/*              dframe->x = 0.61*global::screen_width;
              dframe->w = 0.18*global::screen_width;
              SDL_RenderCopy(display, homefrontText, nullptr, dframe);*/
              FC_Draw(fc_luxirb_med, display, 0.61*global::screen_width, 1, "HOMEFRONT");
#endif
              
              /* If this is a deep redraw, then update the bitmaps themselves */
              if (deep_redraw) {
/*                draw_ll_quad();*/
                SDL_SetRenderTarget(display, lower_right_canvas);
//#ifdef HALLOWEEN
//                SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
//#else
//                SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);
//#endif
//                SDL_RenderClear(display);
                draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                  *player_units, *chosen_ones, *chosen_indices,
                                  unit_disp_ind, 1, subfont_luxirb_small*/);
#ifdef NEW_UI
                SDL_SetRenderTarget(display, right_panel_canvas);
#else
                SDL_SetRenderTarget(display, lower_left_canvas);
#endif
#ifdef HALLOWEEN
                SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
#else
                SDL_SetRenderDrawColor(display, 2, 100, 2, 100);
#endif
                SDL_RenderClear(display);
                SDL_SetRenderTarget(display, nullptr);
                /*                    std::cout << "Just did a deep redraw of the LR matrix" << std::endl;*/
                deep_redraw = false;
              }
              
              /* Then draw the lower quadrants / canvas images */
              SDL_QueryTexture(lower_right_canvas, nullptr, nullptr,
                               &(dframe->w), &(dframe->h));
              dframe->x = leftPanelBound;
              dframe->y = dy;
              SDL_RenderCopy(display, lower_right_canvas, nullptr, dframe);
              SDL_QueryTexture(lower_left_canvas, nullptr, nullptr, &(dframe->w), &(dframe->h));
              dframe->x = 1;
              dframe->y = dy;
              SDL_RenderCopy(display, lower_left_canvas, nullptr, dframe);

#ifdef NEW_UI
              dframe->x = rightPanelBound;
              dframe->y = dy;
              SDL_RenderCopy(display, right_panel_canvas, nullptr, dframe);
#endif
              
              /* Then draw the horizontal and vertical borders for
               * each quadrants */
              SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
              SDL_RenderDrawLine(display, 1, dy, global::screen_width, dy);
              SDL_RenderDrawLine(display, 1, 40, global::screen_width, 40);
              SDL_RenderDrawLine(display, leftPanelBound, 1, leftPanelBound, global::screen_height);
              FC_Draw(fc_luxirb_med, display, 13.8f*global::screen_width/16, 35, date_msg.c_str());
              /*                  if ((!visual_type) && last_selected.x != (-1)) {
               SDL_SetRenderDrawColor(display, 255, 255, 255), 255);
               fillEdge(last_selected.x-leftPanelBound, last_selected.y-dy, -1, lower_right_canvas, ;
               last_selected.x = (-1);
               }*/
              /* Draw the border rectangle */
#ifndef NEW_UI
              if (visual_type == MAP) {
                dframe->x = 70;
                dframe->y = 2;
                dframe->w = 80;
                dframe->h = 36;
                SDL_RenderDrawRect(display, dframe);
              } else if (visual_type == ARMY) {
                dframe->x = 172;
                dframe->y = 2;
                dframe->w = 103;
                dframe->h = 36;
                SDL_RenderDrawRect(display, dframe);
              } else if (visual_type == RESEARCH) {
                dframe->x = 305;
                dframe->y = 2;
                dframe->w = 250;
                dframe->h = 36;
                SDL_RenderDrawRect(display, dframe);
              } else if (visual_type == HOMEFRONT) {
                dframe->x = 580;
                dframe->y = 2;
                dframe->w = 200;
                dframe->h = 36;
                SDL_RenderDrawRect(display, dframe);
              }
#endif
              FC_Draw(fc_luxirb_med, display, 13.2f*global::screen_width/16, 1,
                      "[PAUSED]");
              if (esc_menu) {
                show_esc_menu(/*subfont_luxirb_small*/);
              }
              SDL_RenderPresent(display);
            }
            SDL_UnlockMutex(redraw_control);
            SDL_Delay(1000 / FPS);
          }
        } else { /* The "Big Else" - separates 'paused' from 'not paused'*/
          repaint();
          while ((game_state >= 0) && (! paused)) {
            /*                std::cout << "\tTOP OF INNER UNPAUSED LOOP, paused == %d\n", paused);*/
            /* This means the game is NOT actually paused,
             * so it keeps on a-goin'...  */
            SDL_Event ev;
            while (SDL_PollEvent(&ev)) {
              if (ev.type == SDL_KEYDOWN) {
                if (ev.key.keysym.sym == SDLK_SPACE) {
                  /* Go to "paused" and update the screen to show it. */
                  setPaused(true);
                  repaint();
                  /*                    global_redraw = true;*/
                  /*                    std::cout << "\tEVENT PROCESSED, paused == %d\n", paused);*/
                } else if (ev.key.keysym.sym == SDLK_ESCAPE) {
                  /* */
                  std::cout << "\"ESC\" Key has been pressed" << std::endl;
                  setPaused(true);
                  esc_menu = !esc_menu;
                  repaint();
                } else if (ev.key.keysym.sym == SDLK_q) {
                  /* */
                  game_state = -3;
                  std::cout << "\"Q\" Key has been pressed (exiting)" << std::endl;
                } else if (ev.key.keysym.sym == SDLK_m) {
                  if (music) {
                    music = false;
                    Mix_FadeOutMusic(500);
                  } else {
                    music = true;
                    Mix_FadeInMusic(worldMapTheme, 1, 500);
                  }
                } else if ((ev.key.keysym.sym == SDLK_KP_MINUS) ||
                           (ev.key.keysym.sym == SDLK_MINUS)) {
                  /* Make the gameplay slower */
                  /*                    std::cout << "old value %u => ", game_seconds_per_tick);*/
                  /*if (game_seconds_per_tick < 515) {
                   game_seconds_per_tick -= 100;
                   } else {*/
                  game_seconds_per_tick += 200;
                  /*}*/
                  /*                    al_set_timer_speed(ticker_timer, game_seconds_per_tick);*/
                  if (SDL_RemoveTimer(ticker_timer) == SDL_TRUE) {
                    ticker_timer = SDL_AddTimer(game_seconds_per_tick, processBattles,
                                                ((void *) this));
                  } else {
                    std::cerr << "[ ERR ]    Could not remove ticker timer to replace it!" << std::endl;
                  }
                  /*                    result = sprintf(time_speed_msg, "SPEED x%.3f", (10000.0f / game_seconds_per_tick));*/
                  tmp_str.str("");
                  tmp_str << "SPEED x" << (10000.0 / game_seconds_per_tick);
                  time_speed_msg = tmp_str.str();
                  tmp_str.str("");
                  repaint();
                } else if (ev.key.keysym.sym == SDLK_KP_PLUS) {
                  speed_up();
                  repaint();
                } else if (ev.key.keysym.sym == SDLK_LSHIFT) {
                  l_shift_on = true;
                } else if (ev.key.keysym.sym == SDLK_RSHIFT) {
                  r_shift_on = true;
                } else if ((ev.key.keysym.sym == SDLK_EQUALS)
                           && /*(SDL_GetModState(ev.key.keysym.mod) &
                               (KMOD_LSHIFT|KMOD_RSHIFT))*/
                               (l_shift_on || r_shift_on)) {
                  speed_up();
                  repaint();
                }
              } else if (ev.type == SDL_KEYUP) {
                if (ev.key.keysym.sym == SDLK_LSHIFT) {
                  l_shift_on = false;
                } else if (ev.key.keysym.sym == SDLK_RSHIFT) {
                  r_shift_on = false;
                }
              } else if (ev.type == SDL_WINDOWEVENT &&
                         ev.window.event == SDL_WINDOWEVENT_CLOSE) {
                game_state = -3;
              } else if ((ev.type == SDL_WINDOWEVENT) &&
                         (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
                repaint();
              } else if ((ev.type == SDL_WINDOWEVENT) &&
                         (ev.window.event == SDL_WINDOWEVENT_RESIZED)) {
                repaint();
              } else if (ev.type == SDL_MOUSEWHEEL) {
                if (ev.wheel.y != 0) {
                  /* process a MOUSE SCROLL up or down!! */
                }
              } else if (ev.type == SDL_MOUSEBUTTONDOWN) {
                int xClick = ev.button.x;
                int yClick = ev.button.y;
/*              std::cout << "Mouse has been clicked DOWN at (%d, %d)\n", xClick, yClick);*/
                if ((xClick < leftPanelBound) && (yClick > dy)) {
                  
#ifdef NEW_UI
                } else if (yClick > dy && xClick < leftPanelBound) {
                  if ((yClick-dy) < 0.07*(global::screen_height - dy)) {
#else
                } else if (yClick < 40) {
                  if ((xClick < 0.159*global::screen_width) && (xClick > 0.06*global::screen_width)) {
#endif
                    visual_type = MAP;
                    draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                      *player_units, *chosen_ones, *chosen_indices,
                                      unit_disp_ind, 0, subfont_luxirb_small*/);
                    /*                        } else if (xClick < (6.8f*global::screen_width/16)) {*/
#ifdef NEW_UI
                  } else if ((yClick-dy) < 0.13*(global::screen_height - dy)) {
#else
                  } else if ((xClick < 0.28*global::screen_width) && (xClick > 0.185*global::screen_width)) {
#endif
                    visual_type = ARMY;
/*                    SDL_SetRenderTarget(display, lower_right_canvas);*/
/*                    SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);*/
/*                    SDL_RenderClear(display);*/
                    draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                      *player_units, *chosen_ones, *chosen_indices,
                                      unit_disp_ind, 1, subfont_luxirb_small*/);
#ifdef NEW_UI
                  } else if ((yClick-dy) < 0.22*(global::screen_height - dy)) {
#else
                  } else if ((xClick < 0.48*global::screen_width) && (xClick > 0.310*global::screen_width)) {
#endif
                    /*                      } else if (xClick < (10.8f*global::screen_width/16)) {*/
                    visual_type = RESEARCH;
                    draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                      *player_units, *chosen_ones, *chosen_indices,
                                      unit_disp_ind, 2, subfont_luxirb_small*/);
#ifdef NEW_UI
                  } else if ((yClick-dy) < 0.30*(global::screen_height - dy)) {
#else
                  } else if ((xClick < 0.75*global::screen_width) && (xClick > 0.611*global::screen_width)) {
#endif
                    /*                      } else if (xClick < (10.8f*global::screen_width/16)) {*/
                    visual_type = HOMEFRONT;
                    draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                      *player_units, *chosen_ones, *chosen_indices,
                                      unit_disp_ind, 3, subfont_luxirb_small*/);
                    /*                        std::cout << "Just set \'visual_type\' to 3!" << std::endl;*/
                  }
                  repaint();
                }
              } else if (ev.type == SDL_MOUSEBUTTONUP) {
                int xClick = ev.button.x;
                int yClick = ev.button.y;
/*              std::cout << "Identified a mouse unclick at (%d, %d)\n", xClick, yClick);*/
#ifdef NEW_UI
                if ((xClick > leftPanelBound) && (xClick < rightPanelBound) && (yClick > dy)) {
#else
                if ((xClick > leftPanelBound) && (yClick > dy)) {
#endif
                  if (visual_type == MAP) {
                    int x_coord = xClick-leftPanelBound-global::x_adjust;
                    int y_coord = yClick-dy-global::y_adjust;
/*                    void *pixel = calloc(1, sizeof(Uint32));
                    void *int_pixel = calloc(1, sizeof(Uint32));*/
                    dframe->x = (int)(x_coord/scale_factor);
                    dframe->y = (int)(y_coord/scale_factor);
                    dframe->w = 1;
                    dframe->h = 1;
                    /*                        Uint8 red_ext, green_ext, blue_ext;*/
                    Uint8 red_in, green_in, blue_in;
                    /*                        SDL_LockTexture(lower_right_canvas, dframe, &pixel, &pitch_verify);
                     std::cout << "[DEBUG]    pitch value is " << pitch_verify << std::endl;
                     SDL_GetRGB(((Uint32 *)pixel)[0], rgb_format, &red_ext, &green_ext, &blue_ext);
                     SDL_UnlockTexture(lower_right_canvas);*/
                    int aggregateColVal = Util::getPixel(internal_map_surf, dframe->x, dframe->y);
                    /*                        SDL_LockTexture(internal_map_img, dframe, &int_pixel, &pitch_verify);*/
                    //                        int_pixel = (Uint32 *)(internal_map_surf->pixels)
                    //                            [(dframe->y*internal_map_surf->w) + dframe->x];
                    SDL_GetRGB(aggregateColVal, rgb_format,
                               &red_in, &green_in, &blue_in);
                    //                        if ((red_ext == 255) && (green_ext == 2) && (blue_ext == 6)) {
                    //                          if  (last_selected.x != (-1)) {
                    //                            SDL_SetRenderTarget(display, lower_right_canvas);
                    //                            SDL_SetRenderDrawColor(display, 255, 255, 255, 255);
                    ////                            fillEdge(last_selected.x-leftPanelBound, last_selected.y-dy, -1, lower_right_canvas, ;
                    //                            free(pixel);
                    //                            SDL_SetRenderTarget(display, nullptr);
                    //                          }
                    //                        } else if ((blue_ext != 232) || (green_ext != 162) || (red_ext >=10)) {
                    SDL_SetRenderTarget(display, lower_right_canvas);
                    SDL_SetRenderDrawColor(display, 255, 2, 6, 255);
                    //                          fillEdge(x_coord, y_coord, -1, lower_right_canvas, ;
                    /*                        std::cout << "\t\tcomparing %d to %d\n", last_selected.x, xClick);*/
                    if ((last_selected.x != xClick) && (last_selected.x >= 0)) {
                      SDL_SetRenderDrawColor(display, 255, 255, 255, 255);
                      //                          fillEdge(last_selected.x-leftPanelBound, last_selected.y-dy, -1, lower_right_canvas, ;
                    }
                    SDL_SetRenderTarget(display, nullptr);
                    /*                          int prov_id = province_map[yClick / scaleF][xClick / scaleF];*/
                    /*                          std::cout << "[DEBUG] red blue green = {%d, %d, %d}\n", red, green, blue);*/
                    unsigned int prov_id;
                    prov_id = Util::hash(red_in, green_in, blue_in, 0);
/*                    if (prov_id >= num_provinces) {
                      prov_id = Util::hash(red_in, green_in, blue_in, 0);
                      if (prov_id >= num_provinces) {
                        prov_id = 0;
                      }
                    }*/
                    if (prov_id < num_provinces) {
#ifdef DEBUG_MSG
                      std::cout << "[DEBUG] According to the hash, the province ID should be " << prov_id << std::endl;
#endif
                      Province *selected = provinces[prov_id];
                      const std::string &name = selected->getName();
                      /*                        Util::get_status_text(stat_arr, selected->getStatus());*/
                      /*                        std::string *stat = Util::get_status_str(selected->getStatus());*/
#ifdef NEW_UI
                      SDL_SetRenderTarget(display, right_panel_canvas);
#else
                      SDL_SetRenderTarget(display, lower_left_canvas);
#endif
#ifdef HALLOWEEN
                      SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
#else
                      SDL_SetRenderDrawColor(display, 2, 100, 2, 100);
#endif
                      SDL_RenderClear(display);
                      FC_Draw(subfont_luxirb_small, display, 3, 3,
                              "PROVINCE DATA");
                      FC_Draw(subfont_luxirb_small, display, 3,
                              (0.10)*(global::screen_height - dy), name.c_str());
                      FC_Draw(subfont_luxirb_small, display, 3,
                              (0.15)*(global::screen_height - dy), Util::stat_msgs[selected->getStatus()]);
                      unsigned int n = selected->getSettlements()->size();
                      char settlements[NUM_SETTLEMENTS_PER_PROVINCE][64] = {'\0'};
                      for (ind=0; ind < n; ind ++) {
                        sprintf(settlements[ind], "-%s", selected->
                          getSettlements()->at(ind)->getName().c_str());
                        FC_Draw(subfont_luxirb_small, display, 3,
                                (0.20+(0.05*(ind)))*(global::screen_height-dy), settlements[ind]);
                      }
                      SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
                      /*                          SDL_RenderDrawLine(display, 2, 7.0*(global::screen_height - dy) / 8.0, leftPanelBound-2,
                       7.0*(global::screen_height - dy) / 8.0);*/
                      SDL_RenderDrawLine(display, 2, ((7.0*global::screen_height) / 8.0) - dy, leftPanelBound-2,
                                         ((7.0*global::screen_height) / 8.0) - dy);
                      last_selected.x = xClick;
                      last_selected.y = yClick;
                      last_prov = prov_id;
                    }
                    //                        }
                    /* if ((last_selected.x != xClick) && (last_selected.x != (-1))) {
                     SDL_SetRenderDrawColor(display, 255, 255, 255), 255);
                     fillEdge(last_selected.x-leftPanelBound, last_selected.y-dy, -1, lower_right_canvas, ;
                     } */
                    SDL_SetRenderTarget(display, nullptr);
                    /*                      SDL_QueryTexture(lower_right_canvas, nullptr, nullptr, &(dframe->w), &(dframe->h));
                     dframe->x = leftPanelBound;
                     dframe->y = dy;
                     SDL_RenderCopy(display, lower_right_canvas, nullptr, dframe);
                     SDL_QueryTexture(lower_left_canvas, nullptr, nullptr, &(dframe->w), &(dframe->h));
                     dframe->x = 1;
                     dframe->y = dy;
                     SDL_RenderCopy(display, lower_left_canvas, nullptr, dframe);*/
                    /*                      std::cout << "Should have just colored the selected province." << std::endl;
                     std::cout << "\tagain, mouse unclick occurred at (%d, %d)\n", xClick, yClick);*/
                    repaint();
                  } else if (visual_type == ARMY) {
                    int y_coord = yClick - dy;
                    size_t choice = unit_disp_ind
                      + (y_coord / TABLE_ROW_HEIGHT);
                    if ((y_coord > 0) && (choice < (player_units.size()+2))) {
                      SDL_SetRenderTarget(display, lower_right_canvas);
#ifdef HALLOWEEN
                      SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
                      SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);
#endif
                      SDL_RenderClear(display);
                      SDL_SetRenderDrawColor(display, 255, 140, 1, 255);
                      const SDL_Rect rect_locate = {
                          1, static_cast<int>(choice*TABLE_ROW_HEIGHT+1),
                          static_cast<int>(global::screen_width-leftPanelBound)-1,
                          static_cast<int>(((choice+1)*TABLE_ROW_HEIGHT)
                                    -(choice*TABLE_ROW_HEIGHT+1))
                      };
                      SDL_RenderFillRect(display, &rect_locate);
                      visual_type = ARMY;
                      draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                        *player_units, *chosen_ones, *chosen_indices,
                                        unit_disp_ind, 1, subfont_luxirb_small*/);
                      /* Displays Unit information on the left panel,
                       * for the proper Unit!
                       * If there are many units in the list, be aware of the
                       * position in the list (especially difficult if there is a 
                       * scroll bar!) */
                      Unit *chosen = player_units.at(choice-2);
                      int width = chosen->getSprite()->w;
                      int height = chosen->getSprite()->h;
                      float ratio = width / ((float) height);
                      float std_ratio = (float)(leftPanelBound) / ((float) (dy-40));
                      if (std_ratio > ratio) {
                        /* Scale the sprite bitmap based on WIDTH */
                        float sprite_factor = ((float)leftPanelBound)/width;
                        int new_wid = (sprite_factor*width);
                        int new_height = (sprite_factor*height);
                        /*                          std::cout << "ORIGINAL width %d, height %d\n", wid, hgt);
                         std::cout << "[DEBUG] [if] BY WIDTH Scale factor %.6f\n\tnew width %d\tnew height %d\n", scale_factor, new_wid, new_height);*/
                        const SDL_Rect dt = {(leftPanelBound-new_wid)/2, 40, new_wid, new_height};
                        SDL_Texture *sprite_text = SDL_CreateTextureFromSurface(display, chosen->getSprite());
                        SDL_RenderCopy(display, sprite_text, nullptr, &dt);
                      } else {
                        /* Scale the sprite bitmap based on HEIGHT  */
                        float sprite_factor = ((float)dy-40)/height;
                        int new_wid = (sprite_factor*width);
                        int new_height = (sprite_factor*height);
                        /*                          std::cout << "[DEBUG] [else] BY HEIGHT Scale factor %.6f\n\tnew width %d\tnew height %d\n", scale_factor, new_wid, new_height);*/
                        const SDL_Rect dt = {(leftPanelBound-new_wid)/2, 40, new_wid, new_height};
                        SDL_Texture *sprite_text = SDL_CreateTextureFromSurface(display, chosen->getSprite());
                        SDL_RenderCopy(display, sprite_text, nullptr, &dt);
                      }
                      std::stringstream unit_data;
                      unit_data << chosen->getName();
                      /*                        dframe->w = chosen->getSprite()->w;
                      dframe->h = chosen->getSprite()->h;
                      dframe->x = leftPanelBound-width;
                      dframe->y = dy;
                      SDL_RenderCopy(display, chosen->getSprite(), nullptr, dframe);*/
#ifdef NEW_UI
                      SDL_SetRenderTarget(display, right_panel_canvas);
#else
                      SDL_SetRenderTarget(display, lower_left_canvas);
#endif
#ifdef HALLOWEEN
                      SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
#else
                      SDL_SetRenderDrawColor(display, 2, 100, 2, 100);
#endif
                      SDL_RenderClear(display);
                      FC_Draw(subfont_luxirb_small, display, 3,
                              (0.10)*(global::screen_height - dy), "UNIT DATA");
                      FC_Draw(subfont_luxirb_small, display, 3,
                              (0.20)*(global::screen_height - dy), "%s", chosen->getName().c_str());
                      FC_Draw(subfont_luxirb_small, display, 3,
                              (0.30)*(global::screen_height - dy), "Level: %d", chosen->getLevel());
                      FC_Draw(subfont_luxirb_small, display, 3,
                              (0.35)*(global::screen_height - dy), "EXP: %d / %d",
                              chosen->getExp(), chosen->getDenomExp());
                      dframe->w = chosen->getSprite()->w;
                      dframe->h = chosen->getSprite()->h;
                      dframe->x = (leftPanelBound-width)/2;
                      dframe->y = 2*(global::screen_height-dy)/3;
                      SDL_Texture *sprite_text = SDL_CreateTextureFromSurface(display, chosen->getSprite());
                      SDL_RenderCopy(display, sprite_text, nullptr, dframe);
                      SDL_SetRenderTarget(display, nullptr);
                    }
                  }
                  repaint();
                }
              } else if (ev.type == SDL_USEREVENT) {
                setPaused(true);
                void (*periodicFunc) (void *) = (void (*)(void *)) ev.user.data1;
                periodicFunc(ev.user.data2);
                setPaused(false);
              } /*else if (ev.type == ) {*/
            }
            /* else if () {
              
            }*/
            
/*          long afterTime = clock_gettime(-1, after);
            long elapsed = afterTIme - beforeTime;
            
            if (elapsed < game_seconds_per_tick) {
              int remainder = (((game_seconds_per_tick)- elapsed) / 1000);
              al_rest(remainder);
            }*/
            SDL_LockMutex(redraw_control);
            if (redraw) {
              redraw = false;
              SDL_SetRenderTarget(display, nullptr);
#ifdef HALLOWEEN
              SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
              SDL_SetRenderDrawColor(display, 255, 255, 255, 255);
#endif
              SDL_RenderClear(display);
              
              /* First: drawing the headers */
              /* TODO add or replace with image icons? Makes more space */
/*              SDL_RenderCopy(display, mapText, nullptr, dframe);*/
#ifdef NEW_UI
              SDL_SetRenderTarget(display, lower_left_canvas);
              FC_Draw(white_luxirb_sm, display,
                      40, 0.02*global::screen_height, "ARMY");
              
              FC_Draw(white_luxirb_sm, display,
                      40, 0.08*global::screen_height, "MAP  ");
              
              FC_Draw(white_luxirb_sm, display,
                      40, 0.14*global::screen_height, "RESEARCH");
              
              FC_Draw(white_luxirb_sm, display,
                      40, 0.20*global::screen_height, "HOMEFRONT");
              
              SDL_SetRenderTarget(display, nullptr);
#else
              dframe->y = 1;
              dframe->h = 41;
              dframe->x = 0.08*global::screen_width;
              dframe->w = 0.08*global::screen_width;
              FC_Draw(fc_luxirb_med, display, 0.08*global::screen_width, 1, "MAP");
              
/*              dframe->x = 0.18*global::screen_width;*/
/*              dframe->w = 0.11*global::screen_width;*/
/*              SDL_RenderCopy(display, armyText, nullptr, dframe);*/
              FC_Draw(fc_luxirb_med, display, 0.18*global::screen_width, 1, "ARMY");
              
/*              dframe->x = 0.33*global::screen_width;*/
/*              dframe->w = 0.23*global::screen_width;*/
/*              SDL_RenderCopy(display, researchText, nullptr, dframe);*/
              FC_Draw(fc_luxirb_med, display, 0.33*global::screen_width, 1, "RESEARCH");
              
/*              dframe->x = 0.61*global::screen_width;*/
/*              dframe->w = 0.18*global::screen_width;*/
/*              SDL_RenderCopy(display, homefrontText, nullptr, dframe);*/
              FC_Draw(fc_luxirb_med, display, 0.60*global::screen_width, 1, "HOMEFRONT");
#endif
              
              /* If this is a deep redraw, then update the bitmaps themselves */
              if (deep_redraw) {
                SDL_SetRenderTarget(display, lower_right_canvas);
#ifdef HALLOWEEN
                SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
                SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);
#endif
                SDL_RenderClear(display);
                draw_lr_quad(this/*display, lower_right_canvas, map_img,
                                  *player_units, *chosen_ones, *chosen_indices,
                                  unit_disp_ind, visual_type, subfont_luxirb_small*/);
#ifdef NEW_UI
                SDL_SetRenderTarget(display, right_panel_canvas);
#else
                SDL_SetRenderTarget(display, lower_left_canvas);
#endif
#ifdef HALLOWEEN
                SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
#else
                SDL_SetRenderDrawColor(display, 2, 100, 2, 100);
#endif
                SDL_RenderClear(display);
                SDL_SetRenderTarget(display, nullptr);
                /*                    std::cout << "Just did a deep redraw of the LR matrix" << std::endl;*/
                deep_redraw = false;
              }
              
              /* Then draw the lower_right canvas image */
              SDL_QueryTexture(lower_left_canvas, nullptr, nullptr,
                               &(dframe->w), &(dframe->h));
              dframe->x = 0;
              dframe->y = dy;
              SDL_RenderCopy(display, lower_left_canvas, nullptr, dframe);
              
              SDL_QueryTexture(lower_right_canvas, nullptr, nullptr,
                               &(dframe->w), &(dframe->h));
              dframe->x = leftPanelBound;
              dframe->y = dy;
              SDL_RenderCopy(display, lower_right_canvas, nullptr, dframe);

#ifdef NEW_UI
              dframe->x = rightPanelBound;
              dframe->w = global::screen_width - rightPanelBound;
              dframe->y = dy;
              SDL_RenderCopy(display, right_panel_canvas, nullptr, dframe);
#endif
              
              /* Then draw the horizontal and vertical borders for
               * each quadrants */
              SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
              SDL_RenderDrawLine(display, 1, dy, global::screen_width, dy);
              SDL_RenderDrawLine(display, 1, 40, global::screen_width, 40);
              SDL_RenderDrawLine(display, leftPanelBound, 1,
                                 leftPanelBound, global::screen_height);
              FC_Draw(fc_luxirb_med, display, 13.0f*global::screen_width/16, 1, time_speed_msg.c_str());
              FC_Draw(fc_luxirb_med, display, 13.8f*global::screen_width/16, 35, date_msg.c_str());
              if (esc_menu) {
                show_esc_menu(/*subfont_luxirb_small*/);
              }
              SDL_RenderPresent(display);
              /*SDL_QueryTexture(terrain_base, nullptr, nullptr, &(dframe->w), &(dframe->h));
              dframe->x = 1;
              dframe->y = dy;
              SDL_RenderCopy(display, terrain_base, nullptr, dframe);*/
              /*std::cout << "Just FINISHED processing the redraw. Going to look for another event...?" << std::endl;*/
            }
            SDL_UnlockMutex(redraw_control);
            SDL_Delay(1000 / FPS);
          }
        }
      }
      Mix_FreeMusic(worldMapTheme);
    } else if (game_state == 3) { /* OPTIONS MENU */
      SDL_Texture *opt_button_img[NUM_OPTIONS];
      SDL_Texture *opt_img = SDL_CreateTexture(display, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, global::screen_width, global::screen_height);
      bool mouse_down = false;
      int dial_index = 0;
      int rightBound = 30 + ((8*global::screen_width)/10);
      SDL_SetRenderTarget(display, opt_img);
      SDL_SetRenderDrawColor(display, 5, 126, 200, 255);
      SDL_RenderClear(display);
      /*          SDL_Surface *settingsTitleSurf = TTF_RenderUTF8_Solid(font_luxirb, "Game Settings", black_text);
       SDL_Texture *settingsTitle = SDL_CreateTextureFromSurface(display, settingsTitleSurf);
       SDL_RenderCopy(display, settingsTitle, nullptr, );
       SDL_Surface *settingsSubtitleSurf = TTF_RenderUTF8_Solid(fc_luxirb_med, "[press SPACE to go back]", black_text);
       SDL_Texture *settingsSubtitle = SDL_CreateTextureFromSurface(display, settingsSubtitleSurf);
       SDL_RenderCopy(display, settingsSubtitle, nullptr, );*/
      FC_Draw(font_luxirb, display, 5*global::screen_width/16, global::screen_height/10, "Game Settings");
      FC_Draw(fc_luxirb_med, display, 3*global::screen_width/8, 6*global::screen_height/7, "[press SPACE to go back]");
      for (ind=0; ind < NUM_OPTIONS; ind ++) {
        SDL_Texture* next_block = SDL_CreateTexture(display,
                                                    SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 30,
                                                    OPTION_MENU_DIAL_HEIGHT);
        SDL_SetRenderTarget(display, next_block);
        SDL_SetRenderDrawColor(display, 10, 10, 10, 255);
        SDL_RenderClear(display);
        opt_button_img[ind] = next_block;
      }
      SDL_SetRenderTarget(display, opt_img);
      for (ind=0; ind < NUM_OPTIONS; ind ++) {
        /*std::cout << "[DEBUG] [option button %d] init x and y (%d, %d)\n", ind, x_settings[ind], BASE+((4+OPTION_MENU_DIAL_HEIGHT)*ind));*/
        int y_pos = BASE+((4 + OPTION_MENU_DIAL_HEIGHT)*ind);
        const SDL_Rect button_loc = {x_settings[ind], y_pos, 30, OPTION_MENU_DIAL_HEIGHT};
        SDL_RenderCopy(display, opt_button_img[ind], nullptr, &button_loc);
        FC_Draw(fc_luxirb_med, display, 1, y_pos-10, setting_names[ind].c_str());
      }
      SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
      SDL_RenderDrawLine(display, (5*global::screen_width)/20, global::screen_height/4, (5*global::screen_width)/20, (3*global::screen_height)/4);
      SDL_RenderDrawLine(display, rightBound, global::screen_height/4, rightBound, (3*global::screen_height)/4);
      
      SDL_SetRenderTarget(display, nullptr);
      SDL_QueryTexture(opt_img, nullptr, nullptr, &(dframe->w), &(dframe->h));
      dframe->x = 0;
      dframe->y = 0;
      SDL_RenderCopy(display, opt_img, nullptr, dframe);
      SDL_RenderPresent(display);
      while (game_state == 3) {
        SDL_Event ev;
        SDL_PollEvent(&ev);
        
        if (ev.type == SDL_KEYDOWN) {
          if (ev.key.keysym.sym == SDLK_SPACE) {
            game_state = 1;
            /*global_redraw = false;*/
          }
        } else if (ev.type == SDL_MOUSEBUTTONDOWN) {
          Uint8 red, green, blue, alpha;
          int x_coord = ev.button.x-leftPanelBound;
          int y_coord = ev.button.y-dy;
          int pitch_verify = 0;
          void *pixel = calloc(1, sizeof(Uint32));
          dframe->x = (int)(x_coord/scale_factor);
          dframe->y = (int)(y_coord/scale_factor);
          dframe->w = 1;
          dframe->h = 1;
          SDL_LockTexture(lower_right_canvas, dframe, &pixel, &pitch_verify);
          SDL_GetRGBA(((Uint32 *) pixel)[0], rgba_format, &red, &green, &blue, &alpha);
          SDL_UnlockTexture(lower_right_canvas);
          if ((red == 10)/* && (alpha == 128)*/) {
            /*                std::cout << "[DEBUG] `mouse_down` IS NOW SET TO TRUE!" << std::endl;*/
            mouse_down = true;
            dial_index = ((ev.button.y - BASE) / OPTION_MENU_DIAL_HEIGHT);
            if (dial_index >= NUM_OPTIONS) {
              dial_index = NUM_OPTIONS-1;
            }
          }
        } else if (ev.type == SDL_MOUSEBUTTONUP) {
          mouse_down = false;
        } else if ((ev.type == SDL_WINDOWEVENT) &&
                   (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
          SDL_SetRenderTarget(display, opt_img);
          SDL_SetRenderDrawColor(display, 5, 126, 200, 255);
          SDL_RenderClear(display);
          FC_Draw(font_luxirb, display, 5*global::screen_width/16, global::screen_height/10, "Game Settings");
          FC_Draw(fc_luxirb_med, display, 3*global::screen_width/8, 6*global::screen_height/7, "[press SPACE to go back]");
          /* Separated into two for loops,
           * in an effort to avoid rapidly-switching target-bitmaps... */
          SDL_SetRenderTarget(display, opt_img);
          for (ind=0; ind < NUM_OPTIONS; ind ++) {
            int y_pos = BASE+((4+OPTION_MENU_DIAL_HEIGHT)*ind);
            SDL_QueryTexture(opt_button_img[ind], nullptr, nullptr, &(dframe->w), &(dframe->h));
            dframe->x = x_settings[ind];
            dframe->y = y_pos;
            SDL_RenderCopy(display, opt_button_img[ind], nullptr, dframe);
            FC_Draw(fc_luxirb_med, display, 1, y_pos-10, setting_names[ind].c_str());
          }
          SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
          SDL_RenderDrawLine(display, (5*global::screen_width)/20, global::screen_height/4, (5*global::screen_width)/20, (3*global::screen_height)/4);
          SDL_RenderDrawLine(display, rightBound, global::screen_height/4, rightBound, (3*global::screen_height)/4);
          SDL_SetRenderTarget(display, nullptr);
          SDL_QueryTexture(opt_img, nullptr, nullptr, &(dframe->w), &(dframe->h));
          dframe->x = 0;
          dframe->y = 0;
          SDL_RenderCopy(display, opt_img, nullptr, dframe);
          SDL_RenderPresent(display);
        } else if (ev.type == SDL_MOUSEMOTION) {
          if (mouse_down && dial_index >= 0) {
            int x_ch = ev.motion.xrel;
            if ((x_settings[dial_index]+x_ch > 5*global::screen_width/20) &&
                (x_settings[dial_index]+x_ch < 8*global::screen_width/10)) {
              x_settings[dial_index] += x_ch;
              SDL_SetRenderTarget(display, opt_img);
              SDL_SetRenderDrawColor(display, 5, 126, 200, 255);
              SDL_RenderClear(display);
              FC_Draw(font_luxirb, display, global::screen_width/4, global::screen_height/6, "Game Settings");
              FC_Draw(fc_luxirb_med, display, 1, 1, "[press SPACE to go back]");
              /* Separated into two for loops,
               * in an effort to avoid rapidly-switching target-bitmaps...  */
              SDL_SetRenderTarget(display, opt_img);
              for (ind=0; ind < NUM_OPTIONS; ind ++) {
                int y_pos = BASE+((4+OPTION_MENU_DIAL_HEIGHT)*ind);
                SDL_QueryTexture(opt_button_img[ind], nullptr, nullptr, &(dframe->w), &(dframe->h));
                dframe->x = x_settings[ind];
                dframe->y = y_pos;
                SDL_RenderCopy(display, opt_button_img[ind], nullptr, dframe);
                FC_Draw(fc_luxirb_med, display, 1, y_pos-10, setting_names[ind].c_str());
              }
              SDL_SetRenderDrawColor(display, 2, 2, 2, 255);
              SDL_RenderDrawLine(display, (5*global::screen_width)/20, global::screen_height/4, (5*global::screen_width)/20, (3*global::screen_height)/4);
              SDL_RenderDrawLine(display, rightBound, global::screen_height/4, rightBound, (3*global::screen_height)/4);
              SDL_SetRenderTarget(display, nullptr);
              SDL_QueryTexture(opt_img, nullptr, nullptr, &(dframe->w), &(dframe->h));
              dframe->x = 0;
              dframe->y = 0;
              SDL_RenderCopy(display, opt_img, nullptr, dframe);
              SDL_RenderPresent(display);
            }
          }
        } else if (ev.type == SDL_WINDOWEVENT &&
                   ev.window.event == SDL_WINDOWEVENT_CLOSE) {
          game_state = (-3);
        }
        /*if (global_redraw) {
          
        }*/
      }
      for (ind=0; ind < NUM_OPTIONS; ind ++) {
        SDL_DestroyTexture(opt_button_img[ind]);
      }
    } else if (game_state == 4) {
      /* LOAD a saved game... */
      SDL_SetRenderDrawColor(display, 5, 126, 200, 255);
      SDL_RenderClear(display);
      FC_Draw(font_luxirb, display, global::screen_width/6, global::screen_height/16, "~MAY BE UNSTABLE~");
      FC_Draw(font_luxirb, display, global::screen_width/8, 10*global::screen_height/16, "Press 1 to load the (only) saved game");
      FC_Draw(fc_luxirb_med, display, global::screen_width/8, 13*global::screen_height/16, "[or press SPACE to go back]");
      SDL_RenderPresent(display);
      
      while (game_state == 4) {
        SDL_Event ev;
        SDL_PollEvent(&ev);
        
        if (ev.type == SDL_KEYDOWN) {
          if (ev.key.keysym.sym == SDLK_SPACE) {
            game_state = 1;
            /*global_redraw = false;*/
          } else if (ev.key.keysym.sym == SDLK_1) {
            FILE *save_file_1 = fopen("res/savedgamedata/log1.txt", "r");
            if (save_file_1 == nullptr) {
              std::cout << "ERR -- cannot read from file. It is empty / non-existent!" << std::endl;
            } else {
              result = fscanf(save_file_1, "Date=%d:%d.%d\n", &date[0], &date[1], &date[2]);
              if (result != 3) {
                std::cout << "Problem detected... read " << result << " values, should be 3" << std::endl;
              }
              tmp_str.str("");
              tmp_str << date[0] << ": " << date[1] << "." << date[2];
              date_msg = tmp_str.str();
              tmp_str.str("");
              /*                  result = sprintf(date_msg, "%d: %d.%d", date[0], date[1], date[2]);*/
              std::cout << "Forged the date message as the string: " << date_msg << std::endl;
              fclose(save_file_1);
            }
          } else if (ev.type == SDLK_q) {
            game_state = -3;
          }
        } else if (ev.type == SDL_WINDOWEVENT &&
                   ev.window.event == SDL_WINDOWEVENT_CLOSE) {
          game_state = -3;
        } else if ((ev.type == SDL_WINDOWEVENT) &&
                   (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
          SDL_SetRenderDrawColor(display, 5, 126, 200, 255);
          SDL_RenderClear(display);
          /*              FC_Draw(font_luxirb, display, 1, 1, "MAY BE UNSTABLE~");*/
          FC_Draw(font_luxirb, display, global::screen_width/6, global::screen_height/16, "~MAY BE UNSTABLE~");
          FC_Draw(font_luxirb, display, global::screen_width/8, 10*global::screen_height/16, "Press 1 to load the (only) saved game");
          FC_Draw(fc_luxirb_med, display, global::screen_width/8, 13*global::screen_height/16, "[or press SPACE to go back]");
          SDL_RenderPresent(display);
        }
      }
    }
  }
  if (game_state == -1) {
    rtn_result = -1;
    unsigned int iter;
    bool new_top = true;
    long score = 0;
    for (iter=0; iter < player_units.size(); iter ++) {
      Unit *n_pu = player_units.at(iter);
      if (n_pu->isUp()) {
        score += 100*n_pu->getLevel();
      } else {
        /* optionally deduct some points from the score */
      }
    }
    FILE *t_sc_f = fopen("res/scores.conf", "r");     //  TODO replace with std::ifstream, check file structure of .conf to optimise  -T
    while (new_top && !feof(t_sc_f)) {
      std::cout << "In the while loop, iterating..." << std::endl;
      long next_s = 0;
      int read = fscanf(t_sc_f, "%ld\n", &next_s);
      if (next_s > score) {
        new_top = false;
      }
      if (read != 1) {
        std::cerr << "[ ERR ] Failed to read the \"top scores\" file..." << std::endl;
      }
    }
    std::cout << "About to close and reopen the file" << std::endl;
    if (t_sc_f == nullptr) {
      std::cerr << "[ ERR ] The SCORES file ptr is in fact nullptr!" << std::endl;
    }
    fclose(t_sc_f);
    t_sc_f = fopen("res/scores.conf", "a");
    fprintf(t_sc_f, "%ld\n", score);
    
    std::stringstream score_str;
    score_str << "Score: " << score;
    if (new_top) {
      score_str << "* (new high score!)";
    }
    
    fclose(t_sc_f);
    SDL_SetRenderDrawColor(display, 233, 233, 233, 112);
    dframe->x = 20;
    dframe->y = 20;
    dframe->w = (global::screen_width-40);
    dframe->h = (global::screen_height-60);
    SDL_RenderFillRect(display, dframe);
    SDL_SetRenderDrawColor(display, 20, 250, 20, 255);
    dframe->x = 3.8f*global::screen_width/16;
    dframe->y = 1.5f*global::screen_height/8;
    dframe->w = 8.4f*global::screen_width/16;
    dframe->h = 5.0f*global::screen_height/8;
    SDL_RenderDrawRect(display, dframe);
    FC_Draw(fc_luxirb_med, display, global::screen_width/4, 20, "*** GRAND STRAT VICTORY ***");
    FC_Draw(fc_luxirb_med, display, global::screen_width/4, 60, score_str.str().c_str());
    SDL_RenderPresent(display);
    /*        unsigned int unit_ind = 0;
     unsigned int padding = player_units.size();
     while (unit_ind < padding) {
     Unit *next_pl = player_units.at(unit_ind);
     FC_Draw(subfont_luxirb_small, display, 1, 1, next_pl->toString().c_str());
     unit_ind ++;
     }
     unit_ind = 0;*/
    /*        while (unit_ind < debilitated_players->size()) {
     Unit *next_pl = debilitated_players->at(unit_ind);
     FC_Draw(subfont_luxirb_small, display, 1, 1, next_pl->toString().c_str());
     unit_ind ++;
     }*/
    while (game_state == -1) {
      /* Get next event and respond to it */
      SDL_Event ev;
      while (SDL_PollEvent(&ev)) {
        /*            if (ev.type == SDL_KEYDOWN) {
         game_state = 0;
         } else*/ if (ev.type == SDL_WINDOWEVENT &&
                      ev.window.event == SDL_WINDOWEVENT_CLOSE) {
           game_state = 0;
         } else if ((ev.type == SDL_MOUSEBUTTONDOWN) &&
                    (ev.window.windowID == SDL_GetWindowID(window))) {
           game_state = 0;
         } else if ((ev.type == SDL_WINDOWEVENT) &&
                    (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
   //              SDL_SetRenderDrawColor(display, 233, 233, 233, 112);
   //              dframe->x = 20;
   //              dframe->y = 20;
   //              dframe->w = global::screen_width-40;
   //              dframe->h = global::screen_height-60;
   //              SDL_RenderFillRect(display, dframe);
   //              SDL_SetRenderDrawColor(display, 20, 250, 20, 255);
   //              dframe->x = 3.8f*global::screen_width/16;
   //              dframe->y = 1.5f*global::screen_height/8;
   //              dframe->w = 8.4f*global::screen_width/16;
   //              dframe->h = 5.0f*global::screen_height/8;
   //              SDL_RenderDrawRect(display, dframe);
   //              FC_Draw(fc_luxirb_med, display, global::screen_width/4, 20, "*** GRAND STRAT VICTORY ***");
   //              FC_Draw(fc_luxirb_med, display, global::screen_width/2, global::screen_height/2, score_str.str().c_str());
           SDL_RenderPresent(display);
         }
      }
    }
  }
  if (game_state == -2) {
    rtn_result = -2;
    SDL_SetRenderDrawColor(display, 233, 2, 2, 112);
    dframe->x = 20;
    dframe->y = 20;
    dframe->w = global::screen_width-40;
    dframe->h = global::screen_height-60;
    /*        SDL_RenderFillRect(display, dframe);*/
    SDL_RenderDrawRect(display, dframe);
    SDL_SetRenderDrawColor(display, 250, 20, 20, 255);
    dframe->x = 3.8f*global::screen_width/16;
    dframe->y = 1.5f*global::screen_height/8;
    dframe->w = (12.2f*global::screen_width/16)-(3.8f*global::screen_width/16);
    dframe->h = (6.5f*global::screen_height/8)-(1.5f*global::screen_height/8);
    SDL_RenderDrawRect(display, dframe);
    
    FC_Draw(fc_luxirb_med, display, global::screen_height/4, 20, "*** GRAND STRAT DEFEAT ***");
    SDL_RenderPresent(display);
    
    /*        while (unit_ind < debilitated_players->size()) {
     Unit *next_pl = debilitated_players->at(unit_ind);
     FC_Draw(subfont_luxirb_small, display, 1, 1, next_pl->toString().c_str());
     unit_ind ++;
     }*/
    while (game_state == -2) {
      /* Get next event and respond to it */
      SDL_Event ev;
      SDL_PollEvent(&ev);
      if (ev.type == SDL_KEYDOWN) {
        game_state = 0;
      } else if (ev.type == SDL_WINDOWEVENT &&
                 ev.window.event == SDL_WINDOWEVENT_CLOSE) {
        game_state = 0;
      } else if ((ev.type == SDL_MOUSEBUTTONDOWN) &&
                 (ev.window.windowID == SDL_GetWindowID(window))) {
        game_state = 0;
      } else if (ev.type == SDL_WINDOWEVENT &&
                 ev.window.event == SDL_WINDOWEVENT_ENTER) {
        SDL_RenderPresent(display);
      }
    }
  }
  if (game_state == -3) {
    rtn_result = -3;
    /*        SDL_Rect *exit_screen = new SDL_Rect();*/
    SDL_SetRenderDrawColor(display, 255, 255, 255, 200);
    dframe->x = 20;
    dframe->y = 20;
    dframe->w = global::screen_width-40;
    dframe->h = global::screen_height-60;
    /*        SDL_RenderFillRect(display, dframe);*/
    SDL_RenderFillRect(display, dframe);
    SDL_SetRenderDrawColor(display, 20, 20, 250, 255);
    dframe->x = 3.8f*global::screen_width/16;
    dframe->y = 1.5f*global::screen_height/8;
    dframe->w = (8.4f*global::screen_width/16);
    dframe->h = (5.0f*global::screen_height/8);
    SDL_RenderDrawRect(display, dframe);
    FC_Draw(fc_luxirb_med, display, global::screen_width/4, 20, "*** GRAND STRAT COLD EXIT ***");
    SDL_RenderPresent(display);
    while (game_state == -3) {
      /* Get next event and respond to it */
      SDL_Event ev;
      while (SDL_PollEvent(&ev)) {
        if (ev.type == SDL_KEYDOWN) {
          game_state = 0;
        } else if (ev.type == SDL_WINDOWEVENT &&
                   ev.window.event == SDL_WINDOWEVENT_CLOSE) {
          game_state = 0;
        } else if ((ev.type == SDL_MOUSEBUTTONDOWN) &&
                   (ev.window.windowID == SDL_GetWindowID(window))) {
          game_state = 0;
        } else if (ev.type == SDL_WINDOWEVENT &&
                   ev.window.event == SDL_WINDOWEVENT_ENTER) {
          /*            SDL_SetRenderDrawColor(display, 20, 20, 250, 255);*/
          /*            SDL_RenderClear(display);*/
          SDL_SetRenderDrawColor(display, 255, 255, 255, 200);
          dframe->x = 20;
          dframe->y = 20;
          dframe->w = global::screen_width-40;
          dframe->h = global::screen_height-60;
          /*            SDL_RenderFillRect(display, dframe);*/
          SDL_RenderFillRect(display, dframe);
          SDL_SetRenderDrawColor(display, 20, 20, 250, 255);
          dframe->x = 3.8f*global::screen_width/16;
          dframe->y = 1.5f*global::screen_height/8;
          dframe->w = (8.4f*global::screen_width/16);
          dframe->h = (5.0f*global::screen_height/8);
          SDL_RenderDrawRect(display, dframe);
          FC_Draw(fc_luxirb_med, display, global::screen_width/4, 20, "*** GRAND STRAT COLD EXIT ***");
          SDL_RenderPresent(display);
        }
      }
    }
    /*        delete exit_screen;*/
  }
  
  /* Destroy textures */
  SDL_DestroyTexture(map_img);
  SDL_DestroyTexture(logo);
  SDL_DestroyTexture(lower_right_canvas);
  SDL_DestroyTexture(lower_left_canvas);
#ifdef NEW_UI
  SDL_DestroyTexture(right_panel_canvas);
#endif
  SDL_DestroyTexture(internal_map_canvas);
  
  /* Destroy fonts */
  /*      TTF_CloseFont(font_amok);*/
  FC_FreeFont(font_amok);
  /*      TTF_CloseFont(font_luxirb);*/
  FC_FreeFont(font_luxirb);
  TTF_CloseFont(font_luxirb_med);
  /*      FC_FreeFont(fc_luxirb_med);*/
  /*      TTF_CloseFont(subfont_luxirb_small);*/
  FC_FreeFont(subfont_luxirb_small);
  /*      TTF_CloseFont(font_luxirr);*/
  FC_FreeFont(font_luxirr);
  
  SDL_DestroyRenderer(display);
  return rtn_result;
}

void WMSL::speed_up() {
  /* Make the gameplay faster */
  /*      std::cout << "old value " << game_seconds_per_tick << " => " << ")";*/
  if (game_seconds_per_tick < 153) {
    /* no-op, it's already too fast! */
  } else if (game_seconds_per_tick < 1051) {
    game_seconds_per_tick -= 100;
  } else {
    game_seconds_per_tick -= 200;
  }
  if (SDL_RemoveTimer(ticker_timer) == SDL_TRUE) {
    ticker_timer = SDL_AddTimer(game_seconds_per_tick, processBattles,
                                ((void *) this));
  } else {
    std::cerr << "[ ERR ]    Could not remove ticker timer to replace it!" << std::endl;
  }
  /*      result = sprintf(time_speed_msg, "SPEED x%.3f", (10.0f / game_seconds_per_tick));*/
  std::stringstream speed_setup;
  speed_setup << "SPEED x" << (10000.0 / game_seconds_per_tick);
  /*      speed_setup >> *time_speed_msg;*/
  time_speed_msg = speed_setup.str();
  /*      std::cout << "New game_seconds_per_tick=%f, new speed=%f\n", game_seconds_per_tick, (10000.0f / game_seconds_per_tick));*/
}

void WMSL::scroll_up() {
  if (visual_type == ARMY) {
    if (unit_disp_ind > NUM_UNITS_PER_PAGE / 2) {
      unit_disp_ind -= (NUM_UNITS_PER_PAGE / 2);
    } else {
      unit_disp_ind = 0;
    }
  } else if (visual_type == HOMEFRONT) {
    if (retired_disp_ind > NUM_UNITS_PER_PAGE / 2) {
      retired_disp_ind -= (NUM_UNITS_PER_PAGE / 2);
    } else {
      retired_disp_ind = 0;
    }
  }
}

void WMSL::scroll_down() {
  if (visual_type == ARMY) {
#ifdef DEBUG_MSG
/*    std::cout << "[DEBUG]    unit_disp_ind is " << unit_disp_ind << std::endl;*/
#endif
    if (player_units.size() < NUM_UNITS_PER_PAGE) {
      /* no-op because the player units vector
       * is too small to support a down-scroll. */
      return;
    } else if (unit_disp_ind+(NUM_UNITS_PER_PAGE/2) > player_units.size()-NUM_UNITS_PER_PAGE) {
      /* if the NEW unit display index is NOT sufficiently small
       * (i.e., small enough to allow NUM_UNITS_PER_PAGE
       *  to be displayed at once), then make it the maximum.
       * */
      unit_disp_ind = player_units.size()-NUM_UNITS_PER_PAGE;
    } else {
      /* Otherwise, it IS sufficiently small. So 
       * simply add half the buffer amount and be done. */
      unit_disp_ind += NUM_UNITS_PER_PAGE/2;
    }
#ifdef DEBUG_MSG
    std::cout << "[DEBUG]    unit_disp_ind being set to " << unit_disp_ind << std::endl;
#endif
  } else if (visual_type == HOMEFRONT) {
    if (retired.size() < NUM_UNITS_PER_PAGE) {
      /* no-op because the player units vector
       * is too small to support a down-scroll. */
      return;
    } else if (retired_disp_ind+(NUM_UNITS_PER_PAGE/2) >
        retired.size()-NUM_UNITS_PER_PAGE) {
      /* if the NEW unit display index is NOT sufficiently small
       * (i.e., small enough to allow NUM_UNITS_PER_PAGE
       *  to be displayed at once), then make it the maximum.
       * */
      retired_disp_ind = retired.size()-NUM_UNITS_PER_PAGE;
    } else {
      /* Otherwise, it IS sufficiently small. So 
       * simply add half the buffer amount and be done. */
      retired_disp_ind += NUM_UNITS_PER_PAGE/2;
    }
  }
}

/* Postcondition: displays a very simple ESC menu as an overlay */
int WMSL::show_esc_menu(/*SDL_Texture *map, int code, FC_Font *menu_font*/) {
  SDL_SetRenderDrawColor(display, 233, 233, 233, 112);
  dframe->x = 20;
  dframe->y = 20;
  dframe->w = (global::screen_width-40);
  dframe->h = (global::screen_height-60);
  SDL_RenderFillRect(display, dframe);
  SDL_SetRenderDrawColor(display, 250, 20, 20, 255);
  dframe->x = 6.8f*global::screen_width/16;
  dframe->y = 3.0f*global::screen_height/8;
  dframe->w = (9.2f*global::screen_width/16)-(6.8f*global::screen_width/16);
  dframe->h = (5.0f*global::screen_height/8)-(3.0f*global::screen_height/8);
  SDL_RenderDrawRect(display, dframe);
  dframe->y += 20;
  SDL_RenderCopy(display, resumeText, nullptr, dframe);
  dframe->y += 20;
  SDL_RenderCopy(display, saveExitText, nullptr, dframe);
  dframe->y += 20;
  SDL_RenderCopy(display, exitText, nullptr, dframe);
  return 0;
}

// THE ONLY place where IMG_WIDTH is invoked!!!
//int Game::fillEdge(int x_val, int y_val, int img_w, int img_h, DIRECTION last_direction, SDL_Texture *bmp,
//             /* SDL_Texture *dest,*/ SDL_Color *color) {
//  if ((x_val > 0) && (x_val < img_w+leftPanelBound) &&
//      (y_val > 0) && (y_val < img_h+dy)) {
//    Uint8 red, green, blue;
//    Uint8 new_red, new_green, new_blue;
//    /* TODO get this working again LOL */
//    /*        SDL_LockTexture(bmp, nullptr, , );*/
//    /* For reference, the other LockTexture call: */
//    /*                      SDL_LockTexture(lower_right_canvas, dframe, &pixel, &pitch_verify);*/
//    /*        SDL_GetRGB(thisPxlColor, rgb_format, &red, &green, &blue);*/
//    new_red = color->r;
//    new_green = color->g;
//    new_blue = color->b;
//    /*        std::cout << "READ pixel at (%d, %d) - has current color (%d, %d, %d).\n",
//     x_val, y_val, red, green, blue);*/
//    if ((red < 11) && (green < 11) && (blue < 11)) {
//      /* This is in fact a province boundary (black pixel) */
//      /*          std::cout << "\tEXITING BECAUSE WE REACHED A BLACK SQUARE" << std::endl;*/
//      return 1;
//    } else if ((red < 10) && (green == 162) && (blue == 232)) {
//      return 1;
//    } else if ((red != new_red) || (green != new_green) || (blue != new_blue)) {
//      /*          std::cout << "\tSetting pixel at (%d, %d) to the chosen fill color (%d, %d, %d).\n",
//       x_val, y_val, new_red, new_green, new_blue);*/
//      /* *** TODO REPLACE with whatever API changes pixels in SDL!! ***
//       * al_put_pixel(x_val, y_val, color); */
//      int innerRes;
//      if (last_direction != 2) {
//        innerRes = fillEdge(x_val, y_val-1, 0, bmp,/* dest,*/ color);
//        if (innerRes) {
//          
//        }
//      }
//      if (last_direction != 3) {
//        innerRes = fillEdge(x_val+1, y_val, 1, bmp,/* dest,*/ color);
//      }
//      if (last_direction != 0) {
//        innerRes = fillEdge(x_val, y_val+1, 2, bmp,/* dest,*/ color);
//      }
//      if (last_direction != 1) {
//        innerRes = fillEdge(x_val-1, y_val, 3, bmp,/* dest,*/ color);
//      }
//      return 0;
//    } else {
//      /*          std::cout << "\tEXITING BECAUSE WE REACHED A RED SQUARE" << std::endl;*/
//      return -1;
//    }
//    /* More efficient, non-recursive approach */
//    /*        int wid = al_get_bitmap_width(map_img)*scale_factor;
//     int hgt = al_get_bitmap_height(map_img)*scale_factor;
//     SDL_Color this_pxl = al_get_pixel(bmp, x_val, y_val);
//     Uint8 red, green, blue;*/
//    /*        Uint8 new_red, new_green, new_blue;
//     SDL_GetRGB(color, &new_red, &new_green, &new_blue);*/
//    /*        int x=x_val+1, y;
//     while (x < wid) {
//     y = y_val+1;
//     while (y < hgt) {
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     y = hgt;
//     } else {
//     al_put_pixel(x, y, color);
//     }
//     y ++;
//     }
//     x ++;
//     this_pxl = al_get_pixel(bmp, x, y_val+1);
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     x = wid;
//     } else {
//     }
//     }
//     x = x_val+1;
//     while (x < wid) {
//     y = y_val-1;
//     while (y > 0) {
//     this_pxl = al_get_pixel(bmp, x, y);
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     y = -1;
//     } else {
//     al_put_pixel(x, y, color);
//     }
//     y --;
//     }
//     x ++;
//     this_pxl = al_get_pixel(bmp, x, y_val-1);
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     x = wid;
//     } else {
//     }
//     }
//     x = x_val-1;
//     while (x > 0) {
//     y = y_val-1;
//     while (y > 0) {
//     this_pxl = al_get_pixel(bmp, x, y);
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     y = -1;
//     } else {
//     al_put_pixel(x, y, color);
//     }
//     y --;
//     }
//     x --;
//     this_pxl = al_get_pixel(bmp, x, y_val-1);
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     x = -1;
//     } else {
//     }
//     }
//     x = x_val-1;
//     while (x > 0) {
//     y = y_val+1;
//     while (y < hgt) {
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     y = hgt;
//     } else {
//     al_put_pixel(x, y, color);
//     }
//     y ++;
//     }
//     x --;
//     this_pxl = al_get_pixel(bmp, x, y_val+1);
//     SDL_GetRGB(this_pxl, &red, &green, &blue);
//     if ((red < 11) && (green < 11) && (blue < 11)) {
//     x = -1;
//     } else {
//     }
//     }*/
//    return 0;
//  } else {
//    /*        std::cout << "\tEXITING BECAUSE THE INDICES ARE OUT OF RANGE" << std::endl;*/
//    return -1;
//  }
//}

/* TODO convert from C to C++ */  //  done? -T
void WMSL::load_names() {
  
  //  TODO test this   -T 
  std::ifstream unit_name_f("res/unit-names.conf");
  
  std::string lineBuffer, crBuffer;
  while (std::getline(unit_name_f, lineBuffer)) {
    std::stringstream ss(lineBuffer);
    while (std::getline(ss, crBuffer, '\r')) {
      names->push_back(std::string(crBuffer));
    }
  }
  unit_name_f.close();
  
  //    FILE *unit_name_f = fopen("res/unit-names.conf", "r");
  //    
  //    char next_name[256];
  //    while (fgets(next_name, 256, unit_name_f)) {
  //      /*TODO make this more cross paltform!!!!
  //       * In this primitive code, I am simply replacing a '\n' char
  //       * with a '\0' !!!
  //       * Really ugly stuff!
  //       * So TODO. Find a way to do this elegantly and platform-independently.
  //       * */
  //      size_t length = strlen(next_name);
  //      next_name[length-1]='\0';
  //      if (next_name[length-2] == '\n' || next_name[length-2] == '\r') {
  //        next_name[length-2] = '\0';
  //      }
  //      names->push_back(new std::string(next_name));
  //    }
  //    /*      fscanf(unit_name_f, "", );*/
  //    fclose(unit_name_f);
}

void WMSL::load_map(const char *map_name) {
  /* STEP 1...
   * TODO sort this out.
   * a better approach may be to actually KEEP
   * multiple copies of the world map PNG stored in res/
   * (call the new copy MAP_PRIME),
   * in order to assign EACH DISTINCT COLOR a new province ID.
   * Challenge:
   *  -how will we assign this province ID? What function should we use?
   * Advantage:
   *  -would allow a more efficient, automatic generation of
   *  (this part of) the world map conf file. (A single intelligent
   *  program could simply scan through the MAP PRIME image and then
   *  perform a constant-time operation on each pixel, and then
   *  accumulate the results in a text file.)
   *  The more I think about it, this seems like the best approach available.
   *  
   * Options: 
   *  1. Have a REALLY GOOD HASHING FUNCTION (one that elaborately
   *  acts on the RGB values),
   *  in which case you don't even need to store this data in the
   *  world map conf file (because, at run-time, the actual Game
   *  instance could simply get the pixel and run the HASH to get
   *  a province ID),
   *  (would need to have another, secondary hash function, to get
   *  to the actual province data stored in a array; or else
   *  store it in a linked list instead, but that has O(n) lookup
   *  time)...
   *  OR:
   *  2. Have a really BAD hashing function and instead store these
   *  numbers in the world map conf file.
   *  (For all we care the hashing function could be as simple as
   *  give each province a unique integer numbering from one onward.)
   *  3. Store in memory a SMALLER matrix, based on the smaller
   *  (unscaled?) image and then use RATIOS of coorodinates
   *  relative to the size (width and height) of the image...
   */
  bool exit_code;
  std::ifstream wmData(map_name);
  
  /* Read the actual image and create bitmaps / canvases */
  /*      lower_right_canvas_loc = new SDL_Rect();*/
  dframe->x = leftPanelBound;
  dframe->y = dy;
  dframe->w = global::screen_width-leftPanelBound;
  dframe->h = global::screen_height-dy;
  /*SDL_Surface *ll_canvas_surf = SDL_CreateSurface(0, leftPanelBound, global::screen_height-dy, );*/
  lower_left_canvas = /*SDL_CreateTextureFromSurface();*/
  SDL_CreateTexture(display, SDL_PIXELFORMAT_RGBA8888,
                    SDL_TEXTUREACCESS_TARGET, leftPanelBound, global::screen_height-dy); // change to STREAMING if necessary!
  lower_right_canvas = /*SDL_CreateTextureFromSurface();*/
  SDL_CreateTexture(display, SDL_PIXELFORMAT_RGBA8888,
                    SDL_TEXTUREACCESS_TARGET/*STREAMING*/,
#ifdef NEW_UI
                    rightPanelBound-leftPanelBound, global::screen_height-dy);
  right_panel_canvas = /*SDL_CreateTextureFromSurface();*/
  SDL_CreateTexture(display, SDL_PIXELFORMAT_RGBA8888,
                    SDL_TEXTUREACCESS_TARGET/*STREAMING*/,
                    global::screen_width-rightPanelBound, global::screen_height-dy);
#else
                    global::screen_width-leftPanelBound, global::screen_height-dy);
#endif
  
  /* copied from <draw_lr_quad> ... */
/*  SDL_RWops *world_map_ext_ops = SDL_RWFromFile("res/world-map-2.png", "rb");*/
  SDL_RWops *world_map_ext_ops = SDL_RWFromFile("res/world-map-3.png", "rb");
  if (world_map_ext_ops == nullptr) {
    std::cout << "[FATAL]    RWFromFile returned a nullptr." <<
    " See the error: " << SDL_GetError() << std::endl;
    return;
  }
  map_img_surf = IMG_LoadPNG_RW(world_map_ext_ops);
  if (map_img_surf == nullptr) {
    std::cout << "[FATAL]    SURF IS NULL! Well at least we finally" <<
      " know the error: " << SDL_GetError() << std::endl;
    return;
  }
  int wid = map_img_surf->w, hgt = map_img_surf->h;
  map_img = SDL_CreateTextureFromSurface(display, map_img_surf);
  internal_map_canvas = SDL_CreateTexture(display, SDL_PIXELFORMAT_RGBA8888,
                                          SDL_TEXTUREACCESS_STREAMING,
                                          global::screen_width-leftPanelBound,
                                          global::screen_height-dy);
/*  internal_map_surf = IMG_Load_RW(SDL_RWFromFile("res/world-map-2-internal.png", "rb"), 0);*/
  SDL_RWops *internal_ops = SDL_RWFromFile("res/world-map-3-internal.png", "rb");
  if (internal_ops == nullptr) {
    std::cout << "[FATAL]    RWFromFile returned a nullptr." <<
    " See the error: " << SDL_GetError() << std::endl;
    return;
  }
  internal_map_surf = IMG_Load_RW(internal_ops, 0);
  /*      internal_map_img = IMG_LoadTexture("res/world-map-2-internal.png");*/
  internal_map_img = SDL_CreateTextureFromSurface(display, internal_map_surf);
  /*      font_luxirb_small = TTF_OpenFont("./res/fonts/luxirb.ttf", 24);*/

#ifdef HALLOWEEN
  FC_LoadFont(fc_luxirb_small, display, "./res/fonts/luxirb.ttf", 20,
              FC_MakeColor(HL_OJ_R,HL_OJ_G,HL_OJ_B,255), TTF_STYLE_NORMAL);
#else
  FC_LoadFont(fc_luxirb_small, display, "./res/fonts/luxirb.ttf", 20,
              FC_MakeColor(2,2,2,255), TTF_STYLE_NORMAL);
#endif

  if (fc_luxirb_small == nullptr) {
    std::cerr<<"[ ERR ] Finally found the font error. fc_luxirb_small is nullptr."<<std::endl;
  }
  
  SDL_SetRenderTarget(display, lower_right_canvas);
  SDL_SetRenderDrawColor(display, 250, 250, 250, 255);
  SDL_RenderClear(display);
  float ratio = wid / ((float) hgt);
  float std_ratio = (rightPanelBound - leftPanelBound) / ((float) (global::screen_height - dy));
#ifdef DEBUG_MSG
  std::cout<<"[DEBUG]    Image width is "<<wid<<std::endl;
  std::cout<<"[DEBUG]    Image height is "<<hgt<<std::endl;
#endif
  if (std_ratio < ratio) {
    /* Scale the world map bitmap based on WIDTH   */
#ifdef NEW_UI
    scale_factor = ((float)rightPanelBound-leftPanelBound)/wid;
#else
    scale_factor = ((float)global::screen_width-leftPanelBound)/wid;
#endif
    int new_wid = (scale_factor*wid);
    int new_height = (scale_factor*hgt);
    std::cout << "ORIGINAL width " << wid << ", height " << hgt << std::endl;
    /*        std::cout << "[DEBUG] [if] BY WIDTH Scale factor %.6f\n\tnew width %d\tnew height %d\n", scale_factor, new_wid, new_height);*/
    const SDL_Rect dt = {1, 1, new_wid, new_height};
    SDL_RenderCopy(display, map_img, nullptr, &dt);
  } else {
    /* Scale the world map bitmap based on HEIGHT  */
    scale_factor = ((float)global::screen_height-dy/*-40*/)/hgt;
    int new_wid = (scale_factor*wid);
    int new_height = (scale_factor*hgt);
#ifdef DEBUG_MSG
    std::cout << "[DEBUG] [else] BY HEIGHT scale factor = "
        << scale_factor << std::endl;
#endif
    /*%.6f\n\tnew width %d\tnew height %d\n", scale_factor, new_wid, new_height);*/
    const SDL_Rect dt = {1, 1, new_wid, new_height};
    SDL_RenderCopy(display, map_img, nullptr, &dt);
  }
  /* </draw_lr_quad> */
  SDL_QueryTexture(internal_map_img, nullptr, nullptr, &wid, &hgt);
  /*      SDL_SetRenderTarget(display, lower_right_canvas);*/
#ifdef HALLOWEEN
  SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
  SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);
#endif
  SDL_RenderClear(display);
  /*      SDL_SetRenderTarget(display, lower_right_canvas);*/
  /*      SDL_SetRenderTarget(display, internal_map_canvas);*/
  
  SDL_SetRenderTarget(display, internal_map_canvas);
  SDL_SetRenderDrawColor(display, 250, 250, 250, 255);
  SDL_RenderClear(display);
/*  std::cout << "scale factor is now " << scale_factor << std::endl;*/
  dframe->x = 0;
  dframe->y = 0;
  dframe->w = scale_factor*wid;
  dframe->h = scale_factor*hgt;
  SDL_RenderCopy(display, internal_map_img, nullptr, dframe);
  SDL_SetRenderTarget(display, nullptr);
  
  /* STEP 2!
   * Now, we need to READ province data for ALL provinces on this map!
   * So that for each individual province, we can have its name & settlement data
   * stored in Memory.
   * This is (perhaps) best done using the same exact file. */
  unsigned int id;
  /*      char *s = nullptr;*/
  std::string buffer;
#ifdef JSON
  std::stringstream bStream;
  bStream << wmData.rdbuf();
  lineBuffer=bStream.str();
/*  std::cout<<lineBuffer<<std::endl;*/
  buffer << wmData.rdbuf();
  rapidjson::Document allConfigData;
  if (allConfigData.ParseInsitu((char *)buffer.c_str()).HasParseError()) {
    std::cout<<"[FATAL]    Could not parse the JSON config file!!!"<<std:endl;
    return;
  } else {
    std::cout<<"[  OK  ]   Parsed the JSON config file correctly."<<std::endl;
  }
  // Now the harder part...
  // TODO actually unwind the DOCUMENT structure
  // in order to fill in the Province array.

  Value &prov = allConfigData["graph"];
  if (!prov.IsArray()) {
    std::cout<<"[FATAL]    Could not parse the JSON config file!!!"<<std:endl;
    std::cout<<"[     ]    Detail: province list could not be identified as an Array :("<<std:endl;
    return;
  }
  for (size_t iter=0; iter < prov.Size(); ++iter) {
    
    provinces[id] = next_prov;
  }
#else
  wmData >> buffer;
  std::stringstream(buffer) >> wait_time;
  wmData >> buffer;
  std::stringstream(buffer) >> num_provinces;
  provinces = (Province **)calloc(num_provinces, sizeof(Province));
  /* TODO Consider replacing this with a 
   * Serialization code (to save and load binary Unit object data in the file) 
   * or using RegEx */
  while ((wmData.is_open()) && (!wmData.eof()) && (buffer != "STOP")) {
    std::string name = "";
    std::string sn = "";
    std::string garbageNickname = "";
    int own_stat = -1;
    char ownership;
    int settlement_own = 6789;
    int settl_num = (-1);
    
    /* TODO make/find an Assert() function to make this readable/simple */
    while (buffer != "Province") {
      wmData >> buffer;
  #ifdef CONF_FILE_DEBUG
      std::cout<<"[DEBUG]    Looking for the word \"Province\". Instead found "
        <<buffer<<std::endl;
      SDL_Delay(1000);
  #endif
    }
    wmData >> buffer;
    std::stringstream(buffer) >> id;
    wmData >> garbageNickname;
    do {
      wmData >> buffer;
  #ifdef CONF_FILE_DEBUG
      std::cout<<"[DEBUG]    Looking for the word \"Name:\". Instead found "
        <<buffer<<std::endl;
      SDL_Delay(1000);
  #endif
    } while (buffer != "Name:");
    wmData >> name;
    wmData >> buffer;
    std::stringstream(buffer) >> own_stat;
    wmData >> buffer;
    ownership = buffer.c_str()[0];
    if (ownership == '(') ownership = buffer.c_str()[1];
    Province *next_prov = new Province(name, own_stat, ownership);
    if (own_stat == ENEMY_OWNED) {
      enemy_provinces ++;
    }
    do {
      wmData >> buffer;
/*      std::cout<<"Searching for \"Settlements\", found "<<buffer<<std::endl;*/
    } while (buffer != "Settlements:");
    wmData >> sn;
    /*        std::cout << "FIRST TIME READING settlement - string name read as " << sn << std::endl;*/
    /*        std::cout << "\tsettlement ownership: read " << settlement_own << std::endl;*/
    /*        std::cout << "\t# of variables read = " << result << std::endl;*/
    /* Iterate through each Settlement */
    while (/*(result > 0) && (*/sn != "End"/*strcmp(sn, "End"))*/) {
      int ind;
      wmData >> buffer;
      std::stringstream(buffer) >> settlement_own;
/*      std::cout<<"[DEBUG]    Settlement name: "<<sn<<std::endl;*/
      /*Archer *testArcher1 = new Unit(ARCHER, 5, 7, 0, );
      testArcher1->setSprite("res/Archer.png");
      Potion *testPt1 = new Potion(6);
      testArcher1->addItem(testPt1);
      Archer *testArcher2 = new Unit(ARCHER, 4, 3, 0, );
      testArcher2->setSprite("res/Archer.png");
      Potion *testPt2 = new Potion(8);
      Potion *testPt3 = new Potion(7);
      testArcher2->addItem(testPt2);
      testArcher2->addItem(testPt3);
      Archer *testArcher3 = new Unit(ARCHER, 6, 3, 0, );
      testArcher3->setSprite("res/Archer.png");
      Potion *testPt4 = new Potion(4);
      Potion *testPt5 = new Potion(5);
      Potion *testPt6 = new Potion(1);
      testArcher3->addItem(testPt4);
      testArcher3->addItem(testPt5);
      testArcher3->addItem(testPt6);*/
      /*std::cout << "settlement name is apparently " << sn << std::endl;*/
      /*Settlement *next_sett = new Settlement(sn, settlement_own);*/
      /*next_prov->addSettlement(next_sett);*/
      Area &next_sett = next_prov->addSettlement(sn, settlement_own);
      
      /* Read the INITIAL UNIT DATA from the file!!
       * Also check for any inconsistencies (TODO, maybe).
       * Iterate through each line that describes a particular UNIT
       * i.e., the next loop processes ONE UNIT PER LINE */
      int unit_class;
      int team, x_pos, y_pos;
      std::string un_data = "";
      wmData >> un_data;
  #ifdef CONF_FILE_DEBUG
      std::cout<<"[DEBUG] un_data (next unit name?) read as "
        <<un_data<<std::endl;
      SDL_Delay(1000);
  #endif
      while (/*(result == 5) && (*/un_data != "End_Unit"/*)*/) {
/*        std::cout << "Now reading the file line with UNIT NAME " <<
          un_data << std::endl;*/
        wmData >> buffer;
        std::stringstream(buffer) >> unit_class;
        do {
          wmData >> buffer;
  #ifdef CONF_FILE_DEBUG
          std::cout<<"For some reason expecting a \"Team:\" token. But instead we have "<<buffer<<std::endl;
          SDL_Delay(1000);
  #endif
        } while (buffer != "Team:");
        wmData >> buffer;
        std::stringstream(buffer) >> team;
        
        wmData >> buffer;
        std::stringstream(buffer) >> x_pos;
        
        wmData >> buffer;
        std::stringstream(buffer) >> y_pos;
        Unit *next_u = nullptr;
        if (settlement_own == team) {
          switch (unit_class) {
            case 0:
            {
              next_u = new Unit(ARCHER, x_pos, y_pos, team, un_data.c_str());
              next_u->setSprite("res/Archer-2.png");
              break;
            }
            case 1:
            {
              next_u = new Unit(SOLDIER, x_pos, y_pos, team, un_data.c_str());
              Sword *swordInst8 = new Sword(3, 4, 8);
              std::stringstream pic_name3;
              pic_name3<<"res/nic-cage-"<<(generator() % 5)<<".png";//".jpg";
              next_u->setSprite(pic_name3.str());
              /*  testSoldier0->setSprite("res/ghost-0.png");*/
              exit_code = next_u->setWeapon(swordInst8, false);
              if (!exit_code) {
                std::cout<<"I'm afraid I've got some bad news boys."<<std::endl;
                std::cout<<"\tCouldn't give enemy soldier 0 a Sword."<<std::endl;
              }
              break;
            }
            case 2:
              next_u = new Unit(ARCHER, x_pos, y_pos, team, un_data.c_str());
              next_u->setSprite("res/Archer-2.png");
              break;
            case 3:
            {
              next_u = new Unit(SOLDIER, x_pos, y_pos, team, un_data.c_str());
              Sword *swordInst9 = new Sword(3, 4, 8);
              std::stringstream pic_name2;
              pic_name2<<"res/nic-cage-"<<(generator() % 5)<<".png";//".jpg";
              next_u->setSprite(pic_name2.str());
              /*  testSoldier0->setSprite("res/ghost-0.png");*/
              exit_code = next_u->setWeapon(swordInst9, false);
              if (!exit_code) {
                std::cout<<"I'm afraid I've got some bad news boys."<<std::endl;
                std::cout<<"\tCouldn't give enemy soldier 0 a Sword."<<std::endl;
              }
              break;
            }
            case 4:
              next_u = new Unit(ARCHER, x_pos, y_pos, team, un_data.c_str());
              next_u->setSprite("res/Archer-2.png");
              break;
            default:
              next_u = nullptr;
              break;
          }
        } else {
          std::cerr << "ERR: unit team " << team << " does not match settlement ownership " << settlement_own << std::endl;
        }
        if (next_u != nullptr) {
      /*  std::stringstream loc_src;
          loc_src << sn << ", " << name;
          next_sett->addUnit(next_u);
          next_u->setLoc(new std::string(sn)/ *loc_src.str()* /);*/
          next_u->setLoc(next_sett.getName());
          next_u->setProvId(id);
          next_u->setSettlId(settl_num);
          next_sett.addUnit(next_u);
  #ifdef CONF_FILE_DEBUG
          std::cout << "[DEBUG]    Now adding Unit " << next_u->getName()
            << " of province " << id << " at settlement " << settl_num
            << ", i.e. " << next_sett.getName() << std::endl;
          SDL_Delay(1000);
  #endif
          if (team == 0) {
            player_units.push_back(next_u);
          }
          std::string keyword = "";
          do {
            wmData >> keyword;
          } while (keyword != "EAST:");
          wmData >> keyword;
/*          while (keyword != "EAST:") {
            next_u->setAnimationFrame(NORTH, keyword, display);
            wmData >> keyword;
          }
          wmData >> keyword;*/
          while (keyword != "SOUTH:") {
            next_u->setAnimationFrame(EAST, keyword/*, display*/);
            wmData >> keyword;
          }
          wmData >> keyword;
          while (keyword != "WEST:") {
            next_u->setAnimationFrame(SOUTH, keyword/*, display*/);
            wmData >> keyword;
          }
          wmData >> keyword;
          while (keyword != "NORTH:") {
            next_u->setAnimationFrame(NORTH, keyword/*, display*/);
            wmData >> keyword;
          }
          wmData >> keyword;
          while (keyword != "End_Anim") {
            next_u->setAnimationFrame(WEST, keyword/*, display*/);
            wmData >> keyword;
          }
        }
//        for (ind=0; ind < 32; ind ++) {
//          un_data[ind] = '\0';
//        }
        un_data = "";

        /* Code to replace the whole fscanf line */
        /* Except, now that it has been ported to C++,
         * we read it TOKEN BY TOKEN as opposed to the entire line.
         * So, only read un_data in order to check the loop condition. */
        wmData >> un_data;
//        wmData >> buffer;
//        std::stringstream(buffer) >> unit_class;
//        do {
//          wmData >> buffer;
//        } while (buffer != "Team: ");
//        wmData >> buffer;
//        std::stringstream(buffer) >> team;
//        
//        wmData >> buffer;
//        std::stringstream(buffer) >> x_pos;
//        
//        wmData >> buffer;
//        std::stringstream(buffer) >> y_pos;
/*        std::cout<<"[DEBUG] un_data (next unit name?) read as "
          <<un_data<<std::endl;*/
      }
//      if (result == -1) {
//        std::cout << "ERR: missing End_Unit in conf file." << std::endl;
//      }
      
      sn = "";
      wmData >> sn;
      
  #ifdef CONF_FILE_DEBUG
      std::cout<<"Next line: expected Settlement name, read as "<<sn<<std::endl;
      std::cout << " --- and the number (ownership state) is " << settlement_own;
      std::cout<<std::endl;
      SDL_Delay(1000);
  #endif
      settl_num ++;
    }
    provinces[id] = next_prov;
    wmData >> buffer;
  }
  wmData.close();
#endif
  
  /* Initialize a constant set of baddies - just for now */
  /* TODO make each TMB initiate by choosing Enemy Units FROM settlements
   * in the same province... */
  Sword *opp_sword_0 = new Sword(5, 7, 8);
  Sword *opp_sword_1 = new Sword(3, 4, 8);
  Sword *opp_sword_2 = new Sword(3, 4, 8);
  
  std::stringstream opp_pic_name;
  opp_pic_name<<"res/nic-cage-"<<(generator() % 5)<<".png";//".jpg";
  /*  int pic_choice = generator() % 5;
   sprintf();*/
  /*Soldier*/Unit *opp_soldier_0 = new Unit(SOLDIER, 3, 6, 1, "Test_Baddy_0");
  opp_soldier_0->setSprite(opp_pic_name.str());
  /*  testSoldier0->setSprite("res/ghost-0.png");*/
  exit_code = opp_soldier_0->setWeapon(opp_sword_0, false);
  if (!exit_code) {
    std::cout<<"I'm afraid I've got some bad news boys."<<std::endl;
    std::cout<<"\tCouldn't give enemy soldier 0 a Sword."<<std::endl;
  }
  opp_pic_name.str("");
  opp_pic_name<<"res/nic-cage-"<<(generator() % 5)<<".png";//".jpg";
  /*Soldier*/Unit *opp_soldier_1 = new Unit(SOLDIER, 3, 5, 1, "Test_Baddy_1");
  opp_soldier_1->setSprite(opp_pic_name.str());
  /* For Halloween edition, make the pictures ghosts! */
  /*  testSoldier1->setSprite("res/ghost-0.png");*/
  
  exit_code = opp_soldier_1->setWeapon(opp_sword_1, false);
  if (!exit_code) {
    std::cout << "I'm afraid I've got some bad news boys.\n\tCouldn't give enemy soldier 1 a Sword." << std::endl;
  }
  opp_pic_name.str("");
  opp_pic_name<<"res/nic-cage-"<<(generator() % 5)<<".png";//".jpg";
  /*Soldier*/Unit *opp_soldier_2 = new Unit(SOLDIER, 3, 4, 1, "Test_Baddy_2");
  opp_soldier_2->setSprite(opp_pic_name.str());
  /*  testSoldier2->setSprite("res/ghost-0.png");*/
  exit_code = opp_soldier_2->setWeapon(opp_sword_2, false);
  if (!exit_code) {
    std::cout << "I'm afraid I've got some bad news boys." << std::endl;
    std::cout << "\tCouldn't give enemy soldier 2 a Sword." << std::endl;
  }
  
  opp_units.push_back(opp_soldier_0);
  opp_units.push_back(opp_soldier_1);
  opp_units.push_back(opp_soldier_2);
}

/* Postcondition: properly processes a left-click in the lower-RIGHT
 *    quadrant, assuming the index is greater than zero.
 *  The main functionality of this method is to 
 *  DISPLAY A UNIT'S STATS. But if the click does not refer to a valid
 *  unit in the Army, then other checks are done 
 *  (to process scrolling and Province invasion, and possibly other
 *   buttons in a future release). */
bool WMSL::displayUnitInfo(int x_coord, int y_coord, size_t index) {
  if ((y_coord > 0) && (index > 1)
      &&  (index < (2+NUM_UNITS_PER_PAGE+unit_disp_ind))
      &&  (index < (2+player_units.size()))) {
    SDL_SetRenderTarget(display, lower_right_canvas);
/*    SDL_SetRenderDrawColor(display, 153, 75, 0, 150);*/
#ifdef HALLOWEEN
    SDL_SetRenderDrawColor(display, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
    SDL_SetRenderDrawColor(display, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);
#endif
    SDL_RenderClear(display);
    SDL_SetRenderDrawColor(display, 255, 140, 1, 255);
    const SDL_Rect rect_locate = {1, static_cast<int>(index*TABLE_ROW_HEIGHT+1),
      static_cast<int>(global::screen_width-leftPanelBound-1), static_cast<int>(((index+1)*TABLE_ROW_HEIGHT)-
      (index*TABLE_ROW_HEIGHT+1))};
    SDL_RenderFillRect(display, &rect_locate);
/*      visual_type = ARMY;*/
    /* Manage the lower RIGHT display first: */
    draw_lr_quad(this/*display, lower_right_canvas, map_img,
                      *player_units, *chosen_ones, *chosen_indices,
                      unit_disp_ind, 1, subfont_luxirb_small*/);

    /* Now, manage the lower LEFT sidebar: */
#ifdef NEW_UI
    SDL_SetRenderTarget(display, right_panel_canvas);
#else
    SDL_SetRenderTarget(display, lower_left_canvas);
#endif
    /* Display Unit information on the left panel,
     * for the proper Unit!!!
     * If there are many units in the list, be aware of the
     * position in the list (especially difficult if there is a 
     * scroll bar!) */
    Unit *chosen = player_units.at(index-2);
    int width = chosen->getSprite()->w;
    int height = chosen->getSprite()->h;
    float ratio = width / ((float) height);
    float std_ratio = (float)(leftPanelBound) / (dy-40);
    if (std_ratio < ratio) {
      /* Scale the sprite bitmap based on WIDTH   */
      scale_factor = ((float)leftPanelBound)/width;
      int new_wid = (scale_factor*width);
      int new_height = (scale_factor*height);
/*      std::cout << "ORIGINAL width " << wid << ", << hgt << std::endl;
      std::cout << "[DEBUG] [if] BY WIDTH Scale factor %.6f\n\tnew width %d\tnew height %d\n", scale_factor, new_wid, new_height);*/
      SDL_Texture *nextIcon = SDL_CreateTextureFromSurface(display, chosen->getSprite());
      const SDL_Rect dt = {(leftPanelBound-new_wid)/2, 40, new_wid, new_height};
      SDL_RenderCopy(display, nextIcon, nullptr, &dt);
      
      /* Free the texture data here? Or later? Figure this out TODO */
      SDL_DestroyTexture(nextIcon);
    } else {
      /* Scale the sprite bitmap based on HEIGHT */
      scale_factor = ((float)dy-40)/height;
      int new_wid = scale_factor*width;
      int new_height = scale_factor*height;
      /*          std::cout << "[DEBUG] [else] BY HEIGHT Scale factor %.6f\n\tnew width %d\tnew height %d\n", scale_factor, new_wid, new_height);*/
      const SDL_Rect dt = {(leftPanelBound-new_wid)/2, 40, new_wid, new_height};
      SDL_Texture *nextIcon = SDL_CreateTextureFromSurface(display, chosen->getSprite());
      SDL_RenderCopy(display, nextIcon, nullptr, &dt);
      /* Free the texture data here? Or later? TODO Figure this out */
      SDL_DestroyTexture(nextIcon);
    }
/*    dframe->w = chosen->getSprite()->w;
    dframe->h = chosen->getSprite()->h;
    dframe->x = leftPanelBound-width;
    dframe->y = dy;
    SDL_RenderCopy(display, chosen->getSprite(), nullptr, dframe);*/
#ifdef HALLOWEEN
    SDL_SetRenderDrawColor(display, 2, 2, 2, 250);
#else
    SDL_SetRenderDrawColor(display, 2, 100, 2, 100);
#endif
    SDL_RenderClear(display);
    FC_Draw(fc_luxirb_small, display, 3,
            (0.10)*(global::screen_height - dy), "UNIT DATA");
    FC_Draw(fc_luxirb_small, display, 3,
            (0.20)*(global::screen_height - dy), "%s", chosen->getName().c_str());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.30)*(global::screen_height - dy), "Level: %d", chosen->getLevel());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.35)*(global::screen_height - dy), "EXP: %d / %d",
            chosen->getExp(), chosen->getDenomExp());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.45)*(global::screen_height - dy), "~~~STATS~~~");
    FC_Draw(fc_luxirb_small, display, 3,
            (0.50)*(global::screen_height - dy), "HP: %d", chosen->getHealth());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.55)*(global::screen_height - dy), "STR: %d", chosen->getStrength());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.60)*(global::screen_height - dy), "CONST: %d", chosen->getConstitution());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.65)*(global::screen_height - dy), "SPEED: %d", chosen->getSpeed());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.70)*(global::screen_height - dy), "AGILITY: %d", chosen->getAgility());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.75)*(global::screen_height - dy), "MG STR: %d", chosen->getMagicalStr());
    FC_Draw(fc_luxirb_small, display, 3,
            (0.80)*(global::screen_height - dy), "MG DEF: %d", chosen->getMagicalRes());
/*    dframe->w = chosen->getSprite()->w;
    dframe->h = chosen->getSprite()->h;
    dframe->x = (leftPanelBound-width)/2;
    dframe->y = 2*(global::screen_height-dy)/3;
    SDL_RenderCopy(display, chosen->getSprite(), nullptr, dframe);*/
    SDL_SetRenderTarget(display, nullptr);
    return true;
  } else if ((x_coord > 20) && (x_coord < 50) &&
      (index > 1+NUM_UNITS_PER_PAGE)) {
    scroll_up();
    deepRepaint();
    return true;
  } else if ((x_coord > 70) && (x_coord < 100) &&
      (index > 1+NUM_UNITS_PER_PAGE)) {
    scroll_down();
    deepRepaint();
    return true;
    
    /* TODO make these comparisons more easily scalable,
     * for if the game ever supports a RESIZABLE window. */
#ifdef NEW_UI
  } else if ((x_coord > rightPanelBound-leftPanelBound-70) && (y_coord > global::screen_height-dy-30)) {
#else
  } else if ((x_coord > global::screen_width-leftPanelBound-70) && (y_coord > global::screen_height-dy-30)) {
#endif
    /* "Make Army" was clicked */
    invadeProvince();
    deepRepaint();
  }
  return false;
}

void WMSL::spawnUnits() {
  /*      unsigned int numPlayerProvs = 0;
   unsigned int numContested = 0;*/
  unsigned int index;
  std::vector<int> *indices = new std::vector<int>();
  for (index = 0; index < num_provinces; index ++) {
    if (provinces[index]->getStatus() == PLAYER_OWNED) {
      /*          numPlayerProvs ++;*/
      indices->push_back(index);
    }/* else if (provinces[index]->getStatus() == CONTESTED) {
      numContested ++;
      }*/
  }
  
  /*      std::cout << "[DEBUG]    x_settings for # units is " << x_settings[1]
   << std::endl;*/
  /* TODO Tie it to the game settings, via x_settings... */
  unsigned int unit_limit = 45/*x_settings[1] / 12*/;
  
  unsigned int max_num_new = player_units.size() < unit_limit/*UNIT_LIMIT*/ ?
  (1+indices->size()) : 1;
#ifdef DEBUG_MSG
  std::cout << "[DEBUG]    indices vector has length " << indices->size() << std::endl;
  std::cout << "[DEBUG]    player units: " << player_units.size() << " / " << unit_limit << std::endl;
  std::cout << "[DEBUG]    max num new units is " << max_num_new << std::endl;
#endif
  unsigned int num_new = generator() % max_num_new;
  
  /* In order to make the gameplay realistic and stable,
   * for now I will NOT have units spawn in contested Provinces.
   * So if you're on the losing end, you're SOL. */
  if (indices->size() > 0/*numPlayerProvs > 0*/) {
    for (unsigned int ind=0; ind < num_new; ind ++) {
      /* Then for each Unit, choose a home Province
       * and get Trait data from that Province obj  */
      
      unsigned int hometown_index = generator() % indices->size();
      Province *hometown = provinces[hometown_index];
      Unit *spawn = nullptr;
      
      /* Randomly select a name... */
      std::string given_name = (*names)[generator() % names->size()];
      
      /* Randomly select class... */
      int uc_id = generator();
      std::cout << "[DEBUG]    random integer (which determines UC) is " << uc_id << std::endl;
      switch (uc_id) {
        case 0:
        {
          spawn = new Unit(ARCHER, 1, 2, 0, given_name.c_str());
          spawn->setSprite("res/Archer-halloween-1.png");
          break;
        }
        case 1:
        {
          spawn = new Unit(SOLDIER, 1, 2, 0, given_name.c_str());
          Sword *swordInst = new Sword(3, 4, 8);
          /*                std::stringstream pic_name3;
           pic_name3 << "res/nic-cage-" << (next_u->rand() % 5) << ".jpg";
           next_u->setSprite(pic_name3.str());*/
          spawn->setSprite("res/ghost-0.png");
          spawn->setWeapon(swordInst, false);
          break;
        }
          /*            case 2:
           {
           
           break;
           }
           case 3:
           {
           
           break;
           }*/
        default:
        {
          spawn = new Unit(ARCHER, 1, 1, 0, given_name.c_str());
          spawn->setSprite("res/Archer-halloween-1.png");
          break;
        }
      }
      
      /* Get base Stat and Trait data from the home Province */
      /*          getTraitModifier();*/
      std::cout << "[DEBUG]    The new unit's team is initialized at " <<  spawn->getTeam() << std::endl;
      std::vector<Settlement *> *settlement_choice = hometown->getSettlements();
      unsigned int settl_num = generator() % settlement_choice->size();
      Settlement *local = settlement_choice->at(settl_num);
      spawn->setAge(30);
      spawn->setLoc(local->getName());
      spawn->setProvId(hometown_index);
      spawn->setSettlId(settl_num);
      local->addUnit(spawn);
      
      spawn->addTrait(Util::getRandomTrait());
      hometown->applyStatMods(spawn, date[0]);
      player_units.push_back(spawn);
      std::cout << "[DEBUG]    NOW SPAWNING NEW UNIT " << given_name << std::endl;
    }
  }
  delete indices;
}

/* Precondition: none
 * Postcondition: Prompts user for Province choice; sends units to 
 *   destination province; updating unit properties; etc. */
void WMSL::invadeProvince() {
  /*      Ghost_WMSL *bg_data = new Ghost_WMSL(display,
   lower_left_canvas, lower_right_canvas,
   fc_luxirb_med, date_msg, true);*/
  SDL_Thread *bg_t = SDL_CreateThread(minimal_pause, "[Paused] World Map Strategy Layer", this/*bg_data*/);
  
  /* Now switch focus in the main thread to prompting the
   * use for the the DESTINATION (province) for this army. */
  /*      res = pending_battles->at(iter)->launch();*/
  /*      ALLEGRO_DISPLAY *secondary_disp = al_create_display(global::screen_width, global::screen_height);*/
  /*      SDL_Window *secondary_wind = SDL_CreateWindow("Province Invasion Selection", global::screen_width, 0, global::screen_width, global::screen_height, SDL_WINDOW_OPENGL|SDL_WINDOW_INPUT_GRABBED);*/
  /*      child_windows->push_back(secondary_wind);*/
  InvadeProvinceDisplay *separate_disp = new
  InvadeProvinceDisplay(/*secondary_wind, */window, (9*leftPanelBound/8), provinces,
                        num_provinces, map_img_surf, internal_map_surf);
  SDL_SetWindowTitle(window, "MultiGen Game [PAUSED]");
  size_t selected_prov = separate_disp->launch();
  SDL_SetWindowTitle(window, "MultiGen Game");
  size_t chosen_iter = 0;
  int pause_thread_res = 0;
  delete separate_disp;
  SDL_LockMutex(redraw_control);
  SDL_LockMutex(thread_control);
  redraw = true;
  thread_state = 0;
  SDL_UnlockMutex(thread_control);
  SDL_UnlockMutex(redraw_control);
  SDL_WaitThread(bg_t, &pause_thread_res);
  /*      SDL_DestroyWindow(secondary_wind);*/
  /*      delete bg_data;*/
  SDL_SetRenderTarget(display, nullptr);
  /*      SDL_SetRenderTarget(display, nullptr);*/
  
  if (selected_prov < num_provinces) {
    Province *dest = provinces[selected_prov];
    Field *field = dest->getField();
    /* My Priority here: avoid a data race!
     * In the future, if we ever have the GhostWMSL
     * that processes or displays unit data,
     * we don't want multiple threads modifying & acessing the data
     * at once. */
    /*        std::cout << "[DEBUG]    Just chose destination province & updated "
     << "prov/settl values." << std::endl;*/
    if (dest->getStatus() > 0) { /* If this province is NOT just player-owned... */
      /*          std::cout << "[DEBUG]    Got inside the \"dest status nonzero\" if!!! Good news." << std::endl;*/
      /* Get the object representing the "Field" area inside this province */
      unsigned int limit = chosen_ones.size();
      
      /* If the field is already occupied by enemy units... */
      if (field->getOccupier() != PLAYER_OWNED) {
        /*            std::cout << "[DEBUG]    Occupier not player owned. Good news?" << std::endl;*/
        /* Then, signal that a TMB needs to happen!
         * The 'Field' area is in fact contested. */
        
        std::stringstream map_name;
        int map_num = generator() % NUM_TMB_MAP_CHOICES;
        map_name << "res/maps/unsorted/map" << map_num << ".map";
        std::cout << "About to launch a Field battle... " << std::endl;
        std::cout << "[DEBUG]    # of player units, " << chosen_ones.size() << std::endl;
        std::cout << "[DEBUG]    # of enemy units, " << field->enemy_units().size() << std::endl;
        /* TODO maybe add invading units to the
         * SET of player units already there in the field? */
        RoutTMB *invasion = new RoutTMB
        (/*display, */map_name.str().c_str(), "res/terrains.conf",
         chosen_ones, field->enemy_units(), 0);
        
        ((TacticalMapBattle*) invasion)->setProvince(true);
        pending_battles->push_back(invasion);
        ((TacticalMapBattle*) invasion)->setSettlementName("Field");
        prov_ids->push_back(selected_prov);
        
        if (field->enemy_units().size() > 0) {
          field->setStatus(CONTESTED);
        }
        dest->setStatus(CONTESTED);
        
        /* Administrative updates -- set the army vectors to
         * new values */
        /*            chosen_ones = new std::vector<Unit *>();
         chosen_indices.clear();*/
      } else {
        /* The field IS player-owned (uncontested) */
/*        for (unsigned int transfer_ind=0; transfer_ind < limit; transfer_ind ++) {*/
        for (Unit *next_traveller : chosen_ones) {
          /*              Unit *next_traveller = chosen_ones.at(transfer_ind);*/
          int prov_loc = next_traveller->getProvId();
          
          /* TODO much of the following commented code can
           * be safely deleted.
           *
           * The only caveat is the following: */
          /* Assuming that, in order to invade a different province,
           * the unit must START IN A FIELD OF A NEIGHBORING PROVINCE,
           * (BUT IS THIS TRUE? double check with the client.)
           * If so - this variable is completely unnecessary.
           * It will always be -1. */
          /* So, maybe here we can VERIFY that the unit transfer is valid
           * safe that to a boolean variale that we check later. */
          int sett_loc = next_traveller->getSettlId();
          /*              unsigned int rm_ind, rm_max;*/
          /*              Settlement * / *Area ** / source = nullptr;*/
          if (sett_loc == (-1)) {
            /*                Field *source = provinces[prov_loc]->getField();*/
#ifdef DEBUG_MSG
            std::cout << "[DEBUG]        Source settlement IS equivalent to this field?" << std::endl;
#endif
            
            /*                std::vector<Unit *> *src_army = source->player_units();
             rm_max = source->player_units()->size();
             for (rm_ind = 0; rm_ind < rm_max; rm_ind ++) {
             if (src_army->at(rm_ind) == next_traveller) {
             src_army->erase(src_army->begin()+rm_ind);
             rm_ind = rm_max;
             }
             }*/
          } else {
            /*                Settlement *source = provinces[prov_loc]->getSettlements()->at(sett_loc);*/
#ifdef DEBUG_MSG
            std::cout << "[DEBUG]        Source settlement is DIFFERENT from this field" << std::endl;
#endif
            /*                std::vector<Unit *> *src_army = source->units();
             rm_max = source->units()->size();
             for (rm_ind = 0; rm_ind < rm_max; rm_ind ++) {
             if (src_army->at(rm_ind) == next_traveller) {
             src_army->erase(src_army->begin()+rm_ind);
             rm_ind = rm_max;
             }
             }
             next_traveller->setProvId(selected_prov);
             next_traveller->setSettlId(-1);*/
          }
        }
      }
    }
    while (chosen_iter < chosen_ones.size()) {
      Unit *troop = chosen_ones.at(chosen_iter);
      /* TODO instead of calling `remove()`,
       * figure out WHICH settlement this unit comes from
       * and delete it from THAT list. */
      provinces[troop->getProvId()]->remove(troop);
/*      provinces[troop->getProvId()]->getSettlements()->at()->remove(troop);*/
      troop->setProvId(selected_prov);
      troop->setSettlId(-1);
/*      std::stringstream loc_src;
      loc_src << "Field, " << dest->getName();
      troop->setLoc(loc_src.str());*/
      troop->setLoc(dest->getField()->getName());
      field->addUnit(troop);
      chosen_iter ++;
    }
  }
}

/* Postcondition: automatically updates province and settlement ownership
 * based on what units are present. 
 * This functionality was sadly abandoned after release version 0.0.1.,
 * as per ECO #1. RIP intelligent updates. */
void WMSL::intelligentlyUpdateMap() {
  unsigned int iter;
  for (iter=0; iter < num_provinces; iter ++) {
    int next_r, map_num;
    unsigned int settle_iter, num_settlements, friendly_src, enemy_src;
    Province *prov = provinces[iter];
    std::stringstream map_name;
    map_num = generator() % NUM_TMB_MAP_CHOICES;
    map_name << "res/maps/unsorted/map" << map_num << ".map";
    /* TODO more intelligient TMB map choice */
    if ((prov->getStatus() == 2)/* && (next_r < 10)*/) {
      /*          std::cout << "Comparing: is %d < %d?", next_r, 10);*/
      /* As long as this province IS IN FACT CONTESTED,
       * there is a possible SOURCE SETTLEMENT
       * from which we can choose Player Units to fight. */
      /* This code RANDOMLY SELECTS from these SOURCE SETTLEMENTS */
      friendly_src = -1;
      enemy_src = -1;
      num_settlements = prov->getSettlements()->size();
      settle_iter = 0;
      bool player_present = false;
      bool enemy_present = false;
      for (settle_iter = 0; settle_iter < num_settlements; settle_iter ++) {
        Settlement *n_settle = prov->getSettlements()->at(settle_iter);
        if (n_settle->getOccupier() == PLAYER_OWNED &&
            n_settle->units().size() > 0) {
          player_present = true;
        } else if (n_settle->getOccupier() == ENEMY_OWNED &&
                   n_settle->units().size() > 0) {
          enemy_present = true;
        }
      }
      if (player_present && (!enemy_present)) {
        prov->setStatus(0);
        for (settle_iter = 0; settle_iter < num_settlements; settle_iter ++) {
          prov->getSettlements()->at(settle_iter)->setStatus(0);
        }
      } else if (enemy_present && (!player_present)) {
        prov->setStatus(1);
        for (settle_iter = 0; settle_iter < num_settlements; settle_iter ++) {
          prov->getSettlements()->at(settle_iter)->setStatus(1);
        }
        /*          std::cout << "[DEBUG] [ENEMY PRESENT] Setting province status to 1, player is not present at " << iter << std::endl;*/
      } else if ((!enemy_present) && (!player_present)) {
        /*               ??? wtf to do here? no-op for now, assume player owns it?*/
        std::cout << "[Notice] Province " << iter << " is not occupied." << std::endl;
      } else {
        settle_iter = 0;
        while ((friendly_src == (unsigned int) -1) || (enemy_src == (unsigned int) -1)) {
          Settlement *n_settle = prov->getSettlements()->at(settle_iter);
          /*            std::cout << "\tsettlement being checked: " << n_settle->getName();
           std::cout << " (" << (int)n_settle->getOccupier() << ", " << n_settle->units()->size() << ")" << std::endl;*/
          if (n_settle->getOccupier() == PLAYER_OWNED &&
              n_settle->units().size() > 0 &&
              ((generator() % 101) < 30)) {
            friendly_src = settle_iter;
          } else if (n_settle->getOccupier() == ENEMY_OWNED &&
                     n_settle->units().size() > 0 &&
                     ((generator() % 101) < 30)) {
            enemy_src = settle_iter;
          }
          settle_iter = (settle_iter+1) % num_settlements;
        }
        /*            std::cout << "Settlement with friendly units is called " << prov->getSettlements()->at(friendly_src)->getName() << std::endl;*/
        std::vector<Unit *> friendlies = prov->getSettlements()->at(friendly_src)->units();
        std::vector<Unit *> enemies = prov->getSettlements()->at(enemy_src)->units();
        next_r = generator() % 101;
        /*              std::cout << "Also waiting for " << next_r << " to be less than 30" << std::endl;*/
        if (next_r < 30 && friendlies.size() > 0) {
          std::cout<<" BATTLE ANNOUNCED! "<<std::endl;
          std::cout<<"- In Province "<<iter<<" ("<<prov->getName()<<")"<<std::endl;
          RoutTMB/*TacticalMapBattle*/ *sub_battle = new RoutTMB
          /*    (map_name.str().c_str(), "res/halloween-terrains.conf", display2,*/
          (map_name.str().c_str(), "res/terrains.conf",/* display2,*/
           friendlies, enemies, 0);
          sub_battle->setSettlementName(prov->getSettlements()->at(friendly_src)->getName());
          pending_battles->push_back(sub_battle);
          prov_ids->push_back(iter);
        }
      }
    }
  }
}


/*****************
 Static Functions 
 *****************/

/*static*/ int WMSL::minimal_pause(/*SDL_Thread *thread, */void *arg_void) {
  WMSL *arg = (WMSL *) arg_void;
  SDL_Renderer *render = arg->display;
  SDL_SetRenderTarget(render, nullptr);
  FC_Font *font = arg->fc_luxirb_med;
  SDL_Texture *lower_left = arg->lower_left_canvas;
  SDL_Texture *lower_right = arg->lower_right_canvas;
  int status = arg->getThreadState();
  bool redraw = true, esc_men = false;
  int result;
  
//  /*static *//*constexpr*/ SDL_MessageBoxData cancel_box =
//  {SDL_MESSAGEBOX_INFORMATION, /*nullptr*/arg->window,
//    "Quit without saving?", "Are you sure you want to quit without saving?",
//    2, cancel_buttons, nullptr};
  
  SDL_Rect *frame = new SDL_Rect();
  frame->x = 0;
  frame->y = 0;
  frame->w = global::screen_width;
  frame->h = global::screen_height;
  
  while (status > 0) {
    SDL_LockMutex(arg->redraw_control);
    if (arg->getRedraw()) {
      /*            redraw = false;*/
      SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
      SDL_RenderClear(render);
      
      /* First: drawing the headers */
      /* TODO replace with image icons? Makes more space */
      frame->x = 0.08*global::screen_width;
      frame->y = 1;
      frame->w = 0.08*global::screen_width;
      frame->h = 41;
/*      SDL_RenderCopy(render, arg->mapText, nullptr, frame);*/
      FC_Draw(font, render, 0.08*global::screen_width, 1, "MAP");
      
/*      frame->x = 0.18*global::screen_width;*/
/*      frame->w = 0.11*global::screen_width;*/
/*      SDL_RenderCopy(render, arg->armyText, nullptr, frame);*/
      FC_Draw(font, render, 0.18*global::screen_width, 1, "ARMY");
      
/*      frame->x = 0.33*global::screen_width;*/
/*      frame->w = 0.21*global::screen_width;*/
/*      SDL_RenderCopy(render, arg->researchText, nullptr, frame);*/
      FC_Draw(font, render, 0.33*global::screen_width, 1, "RESEARCH");
      
/*      frame->x = 0.61*global::screen_width;*/
/*      frame->w = 0.18*global::screen_width;*/
/*      SDL_RenderCopy(render, arg->homefrontText, nullptr, frame);*/
      FC_Draw(font, render, 0.61*global::screen_width, 1, "HOMEFRONT");
      
      /*          frame->x = ;
       frame->y = ;
       frame->w = ;
       frame->h = ;
       SDL_RenderCopy(render, espionageText, nullptr, frame);*/
      
      /* Then draw the lower_right canvas image */
      SDL_QueryTexture(lower_right, nullptr, nullptr, &(frame->w), &(frame->h));
      frame->x = leftPanelBound;
      frame->y = dy;
      SDL_RenderCopy(render, lower_right, nullptr, frame);
      SDL_QueryTexture(lower_left, nullptr, nullptr, &(frame->w), &(frame->h));
      frame->x = 1;
      frame->y = dy;
      SDL_RenderCopy(render, lower_left, nullptr, frame);
      
      /* Then draw the horizontal and vertical borders for
       * each quadrants */
      SDL_SetRenderDrawColor(render, 2, 2, 2, 255);
      SDL_RenderDrawLine(render, 1, dy, global::screen_width, dy);
      SDL_RenderDrawLine(render, 1, 40, global::screen_width, 40);
      SDL_RenderDrawLine(render, leftPanelBound, 1, leftPanelBound, global::screen_height);
      /*          FC_Draw(font, render, 13.0f*global::screen_width/16, 1, arg->getTimeMsg().c_str());*/
      FC_Draw(font, render, 13.8f*global::screen_width/16, 35, arg->date_msg.c_str());
      if (esc_men) {
        /*            show_esc_menu(font);*/
      }
      SDL_RenderPresent(render);
      /* check for updates on game state */
      status = arg->getThreadState();
      /*            std::cout << "[DEBUG]    Test 1" << std::endl;*/
    }
    /*          std::cout << "[DEBUG]    Test 2" << std::endl;*/
    SDL_UnlockMutex(arg->redraw_control);
    /*          std::cout << "[DEBUG]    Test 3" << std::endl;*/
    //        }
  }
  std::cout << "[DEBUG] Minimal-pause mini-thread ending" << std::endl;
  /*      SDL_SetRenderTarget(render, nullptr);*/
  delete frame;
  return 0;
}

/* Precondition: 
 * Postcondition: will print each unit in the vector,
 *   in a neat table with its essential stats */
void WMSL::make_table(const std::vector<Unit *> &units, SDL_Renderer *renderer,
    size_t init_ind, FC_Font *font, SDL_Rect *rframe) {
#ifdef HALLOWEEN
  SDL_SetRenderDrawColor(renderer, 204, 204, 204, 250);
#else
  SDL_SetRenderDrawColor(renderer, 250, 250, 250, 250);
#endif
  SDL_RenderClear(renderer);
  FC_Draw(font, renderer, 20, 0, "Name - Class");
  FC_Draw(font, renderer, LR_QUAD_AGE_HDR_X, 0, "AGE");
  FC_Draw(font, renderer, LR_QUAD_LEVEL_HDR_X, 0, "LEVEL");
  FC_Draw(font, renderer, LR_QUAD_LOCATION_HDR_X, 0, "LOCATION");
#ifdef NEW_UI
  FC_Draw(font, renderer, LR_QUAD_CHOSEN_HDR_X, 0, "SORT BY");
  FC_Draw(font, renderer, LR_QUAD_CHOSEN_HDR_X+10, TABLE_ROW_HEIGHT-5, "CHOSEN");
#else
  FC_Draw(font, renderer, LR_QUAD_CHOSEN_HDR_X, 0, "CHOSEN");
#endif
  
  size_t ind = init_ind;
  size_t print_ind = 0;
  /*        std::cout << "[DEBUG]    Now ind=" << ind << " and print_ind="
   << print_ind << std::endl;*/
  /*        std::cout << "[DEBUG]    Player list size is equal to "
   << player_list.size() << std::endl;*/
  while ((ind < units.size()) && (print_ind < NUM_UNITS_PER_PAGE)) {
    /* Here, collect each player unit's data
     * and present it in a ROW of the table. */
    /* Can do this using sprintf() or a stringstream...
     * or perhaps a bit of both - a "SPRING STEEN" !
     * Haha. Contact me (chocorho) if you read that joke.
     *  e a s t e r  e g g #2
     * It's getting unfathomably lonely in here. */
    Unit *player = units.at(ind);
    int y_coordinate = (2*TABLE_ROW_HEIGHT)+(TABLE_ROW_HEIGHT*print_ind);
    if (player->isUp()) {
      if (player->isChosen()) {
        SDL_SetRenderDrawColor(renderer, 55, 240, 1, 20);
        rframe->x = 2;
        rframe->y = ((2+print_ind)*TABLE_ROW_HEIGHT)+1;
        rframe->w = global::screen_width-leftPanelBound-1;
        rframe->h = TABLE_ROW_HEIGHT;
        SDL_RenderFillRect(renderer, rframe);
      } else {
      }
      std::stringstream unit_data_buffer;
      FC_Draw(font, renderer, 20, y_coordinate, player->getName().c_str());
      unit_data_buffer.str(std::string());
      unit_data_buffer << player->getAge();
#ifdef NEW_UI
      FC_Draw(font, renderer, 270, y_coordinate, unit_data_buffer.str().c_str());
#else
      FC_Draw(font, renderer, 380, y_coordinate, unit_data_buffer.str().c_str());
#endif
      unit_data_buffer.str(std::string());
      unit_data_buffer << player->getLevel();
#ifdef NEW_UI
      FC_Draw(font, renderer, 340, y_coordinate, unit_data_buffer.str().c_str());
#else
      FC_Draw(font, renderer, 460, y_coordinate, unit_data_buffer.str().c_str());
#endif
      unit_data_buffer.str(std::string());
      unit_data_buffer << player->getLoc();
#ifdef NEW_UI
      FC_Draw(font, renderer, 420, y_coordinate, unit_data_buffer.str().c_str());
#else
      FC_Draw(font, renderer, 540, y_coordinate, unit_data_buffer.str().c_str());
#endif
      SDL_SetRenderDrawColor(renderer, 4, 4, 4, 255);
      SDL_RenderDrawLine(renderer, 1,
                         (2*TABLE_ROW_HEIGHT)+(TABLE_ROW_HEIGHT*(print_ind+1)),
#ifdef NEW_UI

                         rightPanelBound-leftPanelBound,
#else
                         global::screen_width-leftPanelBound,
#endif
                         (2*TABLE_ROW_HEIGHT)+ (TABLE_ROW_HEIGHT*(print_ind+1)));
    }
    ind ++;
    print_ind ++;
  }
  int y_pos = global::screen_height-dy-30;
  /*        float perc = (unit_disp_ind) / (float) (player_units.size());*/
  if (units.size() > NUM_UNITS_PER_PAGE) {
    float perc = (init_ind) / (float) (units.size() - NUM_UNITS_PER_PAGE);
    /*          std::cout << "percentage is %u / (%lu-%d) => %u / %lu => %f\n",
     unit_disp_ind, player_units.size(), NUM_UNITS_PER_PAGE,
     unit_disp_ind, (player_units.size() - NUM_UNITS_PER_PAGE), perc);*/
    int y_p = (2*TABLE_ROW_HEIGHT)+(perc*NUM_UNITS_PER_PAGE*TABLE_ROW_HEIGHT);
    SDL_SetRenderDrawColor(renderer, 4, 4, 4, 255);
    SDL_RenderDrawLine(renderer, global::screen_width-leftPanelBound-5, 2*TABLE_ROW_HEIGHT, global::screen_width-leftPanelBound-5, y_p);
/* TODO fix the scroll button, if you want.
 * the refactor into a new function broke this :,(    */
//    SDL_QueryTexture(scroll_b, nullptr, nullptr, &(rframe->w), &(rframe->h));
//    rframe->x = global::screen_width-leftPanelBound-11;
//    rframe->y = y_p;
//    SDL_RenderCopy(renderer, scroll_b, nullptr, rframe);
    SDL_RenderDrawLine(renderer, global::screen_width-leftPanelBound-5, y_p, global::screen_width-leftPanelBound-5, (NUM_UNITS_PER_PAGE+2)*TABLE_ROW_HEIGHT);
    SDL_RenderDrawLine(renderer, 15, 0, 15, global::screen_height-dy);
  }
}

/* A function that will generalize the generation of the visual
 * (image, text, etc.) that appears in the
 * lower-right portion of the screen. */
/*static*/ int WMSL::draw_lr_quad(WMSL *instance
/*SDL_Renderer *render,
  SDL_Texture *canvas, SDL_Texture *map,
  std::vector<Unit *> &player_list, std::vector<Unit *> &chosen_list,
  std::vector<unsigned int> &chosen_ids, unsigned int disp_ind,
  int code, FC_Font *text_font*/) {
  SDL_Renderer *render = instance->display;
  SDL_Texture *canvas = instance->lower_right_canvas;
  SDL_Texture *map = instance->map_img;
  std::vector<Unit *> player_list = instance->player_units;
  std::vector<Unit *> retired_list = instance->retired;
  unsigned int disp_ind = instance->unit_disp_ind;
  unsigned int retired_ind = instance->retired_disp_ind;
  int code = instance->visual_type;
  FC_Font *text_font = instance->fc_luxirb_sm;
  int wid = 0, hgt = 0;
  SDL_QueryTexture(map, nullptr, nullptr, &wid, &hgt);
  SDL_Rect *frame = new SDL_Rect();
  
  SDL_SetRenderTarget(render, canvas);
  if (code == MAP) {
/*    SDL_SetRenderDrawColor(render, 0, 167, 233, 255);*/
/* FOR world-map-2.png, this needs to be slightly different 
 * (yea, don't ask.) */
    SDL_SetRenderDrawColor(render, 0, 162, 232, 255);
    SDL_RenderClear(render);
    float ratio = wid / ((float) hgt);
#ifdef NEW_UI
    float std_ratio = (rightPanelBound - leftPanelBound) / ((float) (global::screen_height - dy));
#else
    float std_ratio = (global::screen_width - leftPanelBound) / ((float) (global::screen_height - dy));
#endif
    if (std_ratio < ratio) {
      /* Scale the world map bitmap based on WIDTH   */
#ifdef NEW_UI
      float scale = ((float)rightPanelBound-leftPanelBound)/wid;
#else
      float scale = ((float)global::screen_width-leftPanelBound)/wid;
#endif
      instance->scale_factor = scale;
      int new_wid = scale*wid;
      int new_height = scale*hgt;
#ifdef DEBUG_MSG
      std::cout << "ORIGINAL width " << wid << ", height "
          << hgt << std::endl;
      std::cout << "[DEBUG] [if] BY WIDTH Scale factor = "
          << scale << std::endl;
      std::cout << "\tnew width " << new_wid << "\tnew height "
          << new_height << std::endl;
#endif
      global::y_adjust = (global::screen_height - dy - new_height) / 2;
      const SDL_Rect dt = {0, global::y_adjust, new_wid, new_height};
      SDL_RenderCopy(render, map, nullptr, &dt);
    } else {
      /* Scale the world map bitmap based on HEIGHT  */
      float scale = (static_cast<float>(global::screen_height-dy))/hgt;
      instance->scale_factor = scale;
      int new_wid = scale*wid;
      int new_height = scale*hgt;
#ifdef DEBUG_MSG
      std::cout << "[DEBUG] [else] BY HEIGHT. scale_factor = "
          << scale << std::endl;
      std::cout << "\tNew width = " << new_wid << "\t"
          << new_height << std::endl;
#endif
#ifdef NEW_UI
      global::x_adjust = (rightPanelBound - leftPanelBound - new_wid) / 2;
#else
      global::x_adjust = (global::screen_width - leftPanelBound - new_wid) / 2;
#endif
      const SDL_Rect dt = {global::x_adjust, 0, new_wid, new_height};
      SDL_RenderCopy(render, map, nullptr, &dt);
    }
  } else if (code == ARMY) { // there's a BTS joke in here somewhere.
//    unsigned int ind, print_ind/*, select_ind*/;
    make_table(player_list, render, disp_ind, text_font, frame);
    int y_pos = static_cast<int>(global::screen_height-dy-30);
    if (instance->unit_disp_ind > 0) {
      SDL_SetRenderDrawColor(render, 250, 250, 4, 252);
      frame->x = 20;
      frame->y = y_pos;
      frame->w = 30;
      frame->h = static_cast<int>(global::screen_height-dy-10-y_pos);
      SDL_RenderFillRect(render, frame);
      const SDL_Rect dt = {20, y_pos, 30, 20};
      SDL_RenderCopy(render, instance->arrow_imgs[0], nullptr, &dt);
    } else { // cannot scroll upward any more
      SDL_SetRenderDrawColor(render, 220, 220, 220, 255);
      const SDL_Rect rect_locate = {
          20, y_pos, 30,
          static_cast<int>(global::screen_height-dy-10-y_pos)
      };
      SDL_RenderFillRect(render, &rect_locate);
      const SDL_Rect dt = {20, y_pos, 30, 20};
      SDL_RenderCopy(render, instance->arrow_imgs[1], nullptr, &dt);
    }
    if (disp_ind+NUM_UNITS_PER_PAGE+2 < player_list.size()) {
      SDL_SetRenderDrawColor(render, 250, 250, 4, 252);
      frame->x = 70;
      frame->y = y_pos;
      frame->w = 30;
      frame->h = static_cast<int>(global::screen_height-dy-10-y_pos);
      SDL_RenderFillRect(render, frame);
      const SDL_Rect dt = {70, y_pos, 30, 20};
      SDL_RenderCopy(render, instance->arrow_imgs[2], nullptr, &dt);
    } else { // cannot scroll downward any more
      SDL_SetRenderDrawColor(render, 220, 220, 220, 255);
      const SDL_Rect rect_locate = {70, y_pos, 30, static_cast<int>(global::screen_height-dy-10-y_pos)};
      SDL_RenderFillRect(render, &rect_locate);
      const SDL_Rect dt = {70, y_pos, 30, 20};
/*      std::cout<<"[DEBUG]  arrow image at 3 (mt down arrow) should be displayed on the right"<<std::endl;*/
      SDL_RenderCopy(render, instance->arrow_imgs[3], nullptr, &dt);
    }
    const SDL_Rect dt = {
#ifdef NEW_UI
        static_cast<int>(rightPanelBound-leftPanelBound-70),
#else
        static_cast<int>(global::screen_width-leftPanelBound-70),
#endif
        y_pos, 70, 20
    };
    SDL_RenderCopy(render, instance->invade_province_img, nullptr, &dt);
  } else if (code == RESEARCH) {
    SDL_SetRenderDrawColor(render, 250, 250, 250, 255);
    SDL_RenderClear(render);
    FC_Draw(text_font, render, 10, (global::screen_height/2)-dy, "RESEARCH WILL BE DISPLAYED HERE SOMEHOW");
  } else if (code == HOMEFRONT) {
    SDL_SetRenderDrawColor(render, 250, 250, 250, 250);
    SDL_RenderClear(render);

    /* Now that this code block is being called twice,
     * refactor it into a separate `draw_table()` function */
    /* HMM but.
     * We don't want the table to draw EXACTLY the same headers
     * this time... so how do we account for that? Well:
     * TODO use a bitmask of some sort, in order to be
     * able to bitwise-AND together some numbers 
     * corresponding to the headers we want to display. */
    make_table(retired_list, render, retired_ind, text_font, frame);
  } else {
/*     SDL_SetRenderDrawColor(render, 2, 2, 2, 255);
     SDL_RenderDrawLine(render, 1, dy, global::screen_width, dy);
     SDL_RenderDrawLine(render, 1, 40, global::screen_width, 40);
     SDL_RenderDrawLine(render,
         leftPanelBound, 1,
         leftPanelBound, global::screen_height);*/
    SDL_SetRenderTarget(render, nullptr);
    return -1;
  }
  SDL_SetRenderTarget(render, nullptr);
  delete frame;
  return 0;
}

/* Precondition: general is a non-null Unit ptr.
 * Postcondition: processes the enemy unit's debilitation.
 *   This may include a pop-up announcement, event, etc.
 * TODO fill in this method definition, by consulting a lookup table.  */
/*static!*/ void WMSL::processDead(Unit *general) {
  
}

/* Represents the WMSL AI, which, you'll note, has been shut down
 * and deactivated by default, in versions 0.0.2-0.0.4.
 * A moment of silence for the WMSL AI. :,(   */
/*static*/ Uint32 WMSL::processWmslAI(Uint32 step, void *args) {
  return step;
}

/*static!*/ void/*Uint32*/ WMSL::launchAllBattles(void *args) {
  WMSL *bag = (WMSL *) args;
  SDL_mutex *list_mut = bag->battle_control;
  SDL_mutex *prov_mut = bag->prov_control;
  std::vector<Unit *> &p_units = bag->player_units;
  SDL_Renderer *renderer = bag->display;
  Province **provs = bag->provinces;
  std::vector<int> *prov_tags = bag->prov_ids;
  
  SDL_LockMutex(list_mut);
  SDL_LockMutex(prov_mut);
  std::vector<TacticalMapBattle *> &battles = *(bag->pending_battles);
  size_t num_battles = battles.size();
  Uint32 iter = 0;
  while (iter < num_battles) {
    TMB_RESULT res/*, id*/;
    unsigned int iterator = 0;
    int pause_res;
    /*        Ghost_WMSL *bg_data = new Ghost_WMSL(renderer,
     lower_left_canvas, lower_right_canvas,
     fc_luxirb_med, bag->getDateMsg(), true);*/
    SDL_Thread *bg_t = SDL_CreateThread(minimal_pause,
                                        "[Paused] World Map Strategy Layer", bag);
    /*        std::cout<<"[DEBUG] about to launch the goddamn tmb"<<std::endl;*/
    TacticalMapBattle *active_battle = battles.at(iter);
    
    res = active_battle->launch();
    bag->setThreadState(0);
    SDL_WaitThread(bg_t, &pause_res);
/*    delete bg_data;*/
/*    std::vector<Unit *> *alive = active_battle->getLivingPlayers();*/
    if (res == PLAYER_VICTORY) {
      /* TODO make judgment based on what TYPE of TMB just ended?
       * More specifically, we now treat FIELD battles
       * differently from SETTLEMENT battles */
      bool conquered = true;
      unsigned int num_settlements = provs
      [prov_tags->at(iter)]->getSettlements()->size();
      
      /* If this battle was a FIELD battle, then: */
      if (active_battle->isProvinceBattle()) {
        /* get the Field pointer, and check if it has been resolved */
        Field *fld = provs[prov_tags->at(iter)]->getField();
        if (fld->enemy_units().size() == 0) {
          std::cout << "[DEBUG]    Inside the inner if body, setting status to zero" << std::endl;
          fld->setStatus(0);
          fld->addUnits(active_battle->getLivingPlayers());
        } else if (fld->player_units().size() == 0) {
          std::cout << "[ ERR ]    After a Province Battle that the Player won, there are still enemy units in the field??? Check this out." << std::endl;
          fld->setStatus(1);
          fld->addUnits(active_battle->getLivingPlayers());
        }
        while (conquered && iterator < num_settlements) {
          Settlement *next_s = provs[prov_tags->at(iter)]->getSettlements()->at(iterator);
          
          if (next_s->getOccupier() != PLAYER_OWNED) {
            /* otherwise, if the settlement is not
             * Player-controlled, then note that the province
             * has not been definitively conquered. */
            
            /* ECO 1 (no more intelligent status updates)
             * comments this extra conditional out. */
            /*                if (next_s->units()->size() > 0) {*/
            conquered = false; // NOT conquered.
            /*                  std::cout << "Names " << next_s->getName() << " and " << *active_battle->getSettlementName() << " do not match and occupier is not player." << std::endl;*/
            /*} else {
             next_s->setStatus(0);
             }*/
          }
          iterator ++;
        }
      } else {
        /* Take a look at all the non-Field settlements in this Province. */
        while (/*conquered && */iterator < num_settlements) {
          Settlement *next_s = provs[prov_tags->at(iter)]->getSettlements()->at(iterator);
          /* If this was the site of the battle, */
          /* TODO ECO#1 ... change this to a simpler comparison
           * by adding a settlement_id instance variable
           * to each TMB? idk just an idea. */
          if (next_s->getName() == active_battle->getSettlementName()) {
            /* then update its status to PLAYER controlled */
            next_s->setStatus(0);
            
            /* TODO fix this and keep it consistent... */
            /* Long-run solution: move units into the
             *  Settlement AND delete them from the Province
             *  field object. Super short-run solution:
             *   comment the rest of this block out :P */
            Field *src_field = provs[active_battle->getLivingPlayers().at(0)->getProvId()]->getField();
            int unit_ind;
            int num_transfer = active_battle->getLivingPlayers().size();
            
            /* This is option #(2) described below.
             * Processing & deleting enemy units that are dead.
             * TODO Consider doing this even if the battle was
             * a province battle. */
            std::vector<Unit *> &settle_army = next_s->units();
            unsigned int en_iter = 0;
            for (; en_iter < settle_army.size(); en_iter ++) {
              Unit *next_d_e = settle_army.at(en_iter);
              if ((next_d_e->getTeam() == 1) &&
                  (! next_d_e->isUp())) {
                
                processDead(next_d_e);
                /* TODO Uncomment this after the Enemy-Settlement
                 *   Problem is resolved in tmb.cpp!*/
/*                delete next_d_e;*/ 
                settle_army.erase(settle_army.begin() + en_iter);
              }
            }
            
            /*                std::cout << "[DEBUG]    Num Living units identified as " << num_transfer << std::endl;*/
            for (unit_ind = 0; unit_ind < num_transfer; unit_ind ++) {
              /*                  std::cout << "[DEBUG]    Transferring unit #";
               std::cout << unit_ind << " into the destination settlement." << std::endl;*/
              Unit *n_transf = active_battle->getLivingPlayers().at(unit_ind);
              src_field->removePlayer(n_transf);
              next_s->addUnit(n_transf);
              n_transf->setSettlId(iterator);
              n_transf->setLoc(next_s->getName());
            }
            
            /* Careful with this line...
             * If pointers TO VECTORS are being 'shared'
             * (passed by value and copied),
             * then you may end up clearing another vector 
             * that you don't mean to clear. */
            /* Also, if the player's invading army is a PROPER
             * subset of the Field player units, then this
             * should be replaced by the remove_player call
             * in the above for-loop. */
            /*                src_field->player_units()->clear();*/
          } else if (conquered && (next_s->getOccupier() != PLAYER_OWNED)) {
            /* otherwise, if it is not the site of the battle
             * AND not Player-controlled, then note that the province
             * has not been definitively conquered. */
            
            /* ECO 1 (=> "no more intelligent status updates")
             * comments this extra conditional out. */
            /*                if (next_s->units()->size() > 0) {*/
            conquered = false; // NOT conquered.
            /*                  std::cout << "Names " << next_s->getName() << " and " << *active_battle->getSettlementName() << " do not match and occupier is not player." << std::endl;*/
            /*} else {
             next_s->setStatus(0);
             }*/
          }
          iterator ++;
        }
      }
      if (conquered) {
        unsigned int check_vc;
        provs[prov_tags->at(iter)]->setStatus(0);
        bag->setGameState(-1);
        for (check_vc = 0; check_vc < bag->num_provinces; check_vc ++) {
          if (provs[check_vc]->getStatus() != 0) {
            bag->setGameState(2);
          }
        }
      } else {
        provs[prov_tags->at(iter)]->setStatus(2);
        /*            std::cout << "[DEBUG] Setting province id " << prov_ids->at(iter) << " to status 2 (was that the right one?)" << std::endl;*/
      }
    } else if (res == ENEMY_VICTORY) {
      unsigned int num_settlements;
      bool conquered = true;
      num_settlements = provs[prov_tags->at(iter)]->getSettlements()->size();
      while (conquered && iterator < num_settlements) {
        Settlement *next_s = provs[prov_tags->at(iter)]->getSettlements()->at(iterator);
        /* TODO make judgment based on what TYPE of TMB just ended */
        if (next_s->getName() == active_battle->getSettlementName()) {
          next_s->setStatus(1);
        } else if (next_s->getOccupier() != ENEMY_OWNED) {
          // Opponent has NOT conquered.
          conquered = false;
        }
        iterator ++;
      }
      if (conquered) {
        provs[prov_tags->at(iter)]->setStatus(1);
      } else {
        provs[prov_tags->at(iter)]->setStatus(2);
      }
    } else {
      std::cerr << "Uh-oh... Error... Check out this TMB return value: " << res << std::endl;
    }
    std::vector<Unit *> deb_players = active_battle->getDebilPlayers();
    std::vector<Unit *> deb_enemies = active_battle->getDebilEnemies();
    unsigned int num_deb_pl = deb_players.size();
    unsigned int num_players = p_units.size();
    for (iterator = 0; iterator < num_players; iterator ++) {
      unsigned master_ind = 0;
      bool removed = false;
      while (master_ind < num_deb_pl) {
        if (deb_players.at(master_ind) == p_units.at(iterator)) {
          /* TODO delete these units entirely?
           * Or store their info in a log and then delete them?
           * You gotta reap that memory someday! */
          removed = true;
          p_units.erase(p_units.begin()+iterator);
          iterator --;
          num_players --;
          master_ind = num_deb_pl;
        }
        master_ind ++;
      }
      if (!removed) {
        p_units.at(iterator)->levelUp();
      }
    }
    
    /* TODO do a similar thing for enemy units - 
     * BUT only for the PERMANENT Enemy units.
     * Most are non-permanent. */
    /* TODO separate based on "General" class Units (permanents)
     * and non-permanenent / "as-needed" / expendable enemy Units...
     *
     * Possible solutions:
     * (1) maybe simply add a boolean for that in Unit class?
     * Long-run solution: make a virtual subclass of Unit
     * (2) simply traverse the settlement units() vector
     *  (which should only have permanents)
     *  and delete the Unit pointers from there...
     * (3) Super short-run solution: bite the bullet;
     *    delete none and suffer the memory leak */
    for (iterator = 0; iterator < deb_enemies.size(); iterator ++) {
      /*          delete deb_enemies->at(iterator);*/
      /*          if () {
       
       }*/
    }
    /* Option #2 is currently used. But it has been
     * relocated up. */
    
    /* ECO #1 consequence: because the enemy units are taken
     * DIRECTLY FROM THE SETTLEMENT ATTRIBUTE (shallow copy),
     * don't actually delete the thing here.
     * Just clear it. */
    deb_enemies.clear();
    /*        delete deb_enemies;*/
    delete active_battle;
    iter ++;
    bag->deepRepaint();
  }
  SDL_SetRenderTarget(renderer, nullptr);
  if (num_battles > 0) {
    /*        paused = true;*/
    battles.clear();
    prov_tags->clear();
  }
  SDL_UnlockMutex(prov_mut);
  SDL_UnlockMutex(list_mut);
}

/* Parmesian package parameter (appreciate the alliteration) */
/*static!*/ Uint32 WMSL::processBattles(Uint32 step, void *args) {
  /* TODO overhaul this battle generation system,
   * replacing it with a scheduling system ...
   * Apparently, according to the ambiguous """Spec,"""
   * each Province should have a number of date[2]s until
   * the next battle occurs ...
   * In practice obviously this will be stored differently
   * (not with an new member integer in the Province class),
   * but the effect should be the same: timed scheduling of battles,
   * rather than a pseudorandom generation of battles. */
  Uint32 iter;
  
  /* Open up your bag o' tricks, i.e.,
   * extract data from the parameter itself. */
  /*      TickerParams *bag = (TickerParams *) parms;*/
  WMSL *bag = (WMSL *) args;
  SDL_mutex *list_mut = bag->battle_control;
  SDL_mutex *prov_mut = bag->prov_control;
  SDL_mutex *date_mut = bag->date_control;
  std::vector<Unit *> &p_units = bag->player_units;
  SDL_Renderer *renderer = bag->display;
  Province **provs = bag->provinces;
  std::vector<int> *prov_tags = bag->prov_ids;
  int mutexOp;
  
  if (bag->isPaused()) {
    bag->repaint();
    return step;
  }
  SDL_LockMutex(date_mut);
  
  /* Advance the date by one tick */
  /*      tmp_str.str("");*/
  std::stringstream str_builder;
  int *daycal = bag->date;
  str_builder << daycal[0] << ": " << daycal[1] << "." << daycal[2];
  /*      str_builder >> *date_msg;*/
  //    bag->setDateMsg(str_builder.str());
  bag->date_msg = str_builder.str();
  str_builder.str("");
  /*      result = sprintf(date_msg, "%d: %d. %d", daycal[0], daycal[1], daycal[2]);*/
  if (daycal[2] == DAYS_PER_MONTH) {
    if (daycal[1] == MONTHS_PER_YEAR) {
      daycal[0] ++;
      daycal[1] = 1;
      daycal[2] = 1;
      /* TODO Do an autosave to a config file */
      /* TODO Find a way to do this loop more elegantly
       *  (but don't betoo hasty. It DOES modify p_units,
       *   so I'm led to believe that it cannot be as 
       *   simple as `for (const auto& next_player : p_units) {`)
       **/
      for (iter = 0; iter < p_units.size(); iter ++) {
        Unit *next_player = p_units.at(iter);
        next_player->addAge(1);
        int new_age = next_player->getAge();
        if (next_player->isUp() && new_age > RETIREMENT) {
          /* Process a retirement here: */
          if (next_player->isChosen()) {
            next_player->flipChosen();
            /* If the retiring unit had been chosen, then
             * remove appropriate entries from the 
             * `chosen_ones` vector: */
            for (auto remove_ind=bag->chosen_ones.begin();
                 remove_ind != bag->chosen_ones.end(); remove_ind++) {
              /* TODO for efficiency, change to a std::remove_if call? */
              //  Lookup erase-remove idiom  -T
              if ((*remove_ind) == next_player) {
                bag->chosen_ones.erase(remove_ind);
                remove_ind --;
              }
            }
          }
          bag->retired.push_back/*emplace*/(next_player);
          p_units.erase(p_units.begin()+iter);
            
          /* Here, again do something about the fact that this player is still
           *   (probably) listed in a Settlement in some Province... hmm. */
          size_t loc = next_player->getProvId();
          std::vector<Settlement *> *settlements = provs[loc]->getSettlements();
          
          int loc_int = next_player->getSettlId();
          if (loc_int == (-1)) {
            provs[loc]->getField()->removePlayer(next_player);
          } else {
            settlements->at(loc_int)->remove(next_player);
          }
          
//          bag->retired_disp_ind = 0; // probably actually unnecessary overkill
          bag->setPaused(true); // might also be overkill
          if (bag->visual_type == HOMEFRONT) {
            bag->deepRepaint();
          }
          /* Finally, BECAUSE we erased something from the vector
           *   over which we're iterating: */
          iter--;
        }
      }
      
      /* HERE I process retired units */
      for (iter = 0; iter < bag->retired.size(); iter ++) {
        Unit *next_player = bag->retired.at(iter);
        next_player->addAge(1);
        int new_age = next_player->getAge();
        if (new_age > OLD_AGE) {
          unsigned int excess = new_age - OLD_AGE;
          if (generator() % 101 < 10+(2*excess)) {
            unsigned int loc = -1;
            next_player->die();
            std::stringstream obit;
            obit << next_player->getName();
            obit << " (" << new_age << ")";
            obit << " is no longer with us.";
            int success = SDL_ShowSimpleMessageBox(
                                                   SDL_MESSAGEBOX_INFORMATION,
                                                   "EVENT - Unit is debilitated",
                                                   obit.str().c_str(),
                                                   bag->window
                                                   );
            bag->dead_pool.push_back(next_player);
            bag->retired.erase(bag->retired.begin()+iter);
            bag->unit_disp_ind = 0;
            bag->setPaused(true);
            bag->deepRepaint();
/*            if (result == 1) {
             paused = false;
            }*/
            iter--;
            /* Find the unit's SETTLEMENT
             * and remove it from that vector too. */
            loc = next_player->getProvId();
            std::vector<Settlement *> *settlements = provs[loc]->getSettlements();
            
            int loc_int = next_player->getSettlId();
            if (loc_int == (-1)) {
              provs[loc]->getField()->removePlayer(next_player);
            } else {
              settlements->at(loc_int)->remove(next_player);
            }
            
/*          for (ind = 0; ind < settlements/ *->at(ind)->units()* /->size(); ind ++) {
              if (settlements->at(ind)->getName().compare(next_player->getLoc()->substr(0, next_player->getLoc()->find_first_of(",", 0))) == 0) {
                std::vector<Unit *> *regiment = settlements->at(ind)->units();
                unsigned int regiment_size = regiment->size();
                unsigned int infantry_ind = 0;
                while (infantry_ind < regiment_size) {
                Unit *del_check = regiment->at(infantry_ind);
                if (del_check == next_player) {
                  regiment->erase(regiment->begin() + infantry_ind);
                  infantry_ind --;
                  regiment_size --;
                }
                infantry_ind ++;
              }
              ind = settlements->size();
            }
            }*/
            if (bag->retired.size() == 0) {
              bag->setGameState(-2);
            }/* else { }*/
          }
        }
      }
      if (bag->visual_type == ARMY) {
        SDL_SetRenderTarget(renderer, bag->lower_right_canvas);
        SDL_SetRenderDrawColor(renderer, 153, 75, 0, 150);
#ifdef HALLOWEEN
        SDL_SetRenderDrawColor(renderer, HL_OJ_R, HL_OJ_G, HL_OJ_B, 255);
#else
        SDL_SetRenderDrawColor(renderer, VS1_BG_R, VS1_BG_G, VS1_BG_B, VS1_BG_A);
#endif
        SDL_RenderClear(renderer);
        draw_lr_quad(bag/*renderer, lower_right_canvas, map_img, *player_units,
                         *chosen_ones, *chosen_indices, unit_disp_ind, 1, subfont_luxirb_small*/);
      }
    } else {
      daycal[1] ++;
      daycal[2] = 1;
      
      /* ~EVERY MONTH (except on new years) have new units spawn~ */
      /* First find number of new units */
      bag->spawnUnits();
    }
  } else {
    daycal[2] ++;
  }
  SDL_UnlockMutex(date_mut);
  
  /* Now set up the user event to process pending battles */
  if (SDL_TryLockMutex(list_mut) == 0) {
    if (SDL_TryLockMutex(prov_mut) == 0) {
      std::vector<TacticalMapBattle *> &battles = *(bag->pending_battles);
      unsigned int num_battles = battles.size();
      if (battles.size() > 0) {
        /* Push a new user event to notify the MGL thread
         * that there are new pending battles */
        SDL_Event nextEv;
        SDL_UserEvent nestedEvent;
        nestedEvent.type = SDL_USEREVENT;
        nestedEvent.code = 0;
        std::cout<<"[HOLY ]    Pushing user event into the queue!!!"<<std::endl;
        nestedEvent.data1 = (void *) &launchAllBattles;
        nestedEvent.data2 = args;
        
        nextEv.type = SDL_USEREVENT;
        nextEv.user = nestedEvent;
        SDL_PushEvent(&nextEv);
      }
      SDL_UnlockMutex(prov_mut);
    }
    SDL_UnlockMutex(list_mut);
  }
  bag->repaint();
  /*      std::cout<<"[SUCCESS]    Cool. Now finished processing the tick."<<std::endl;*/
  return step;
}

