#pragma once

#include <sstream>

/* Weapon class - a general superclass */
class Weapon {
  public:
    Weapon(int mI, int mD, int aR)
      : minDamage(mI)
      , maxDamage(mD)
      , armourPiercing(aR)
      , minRange(1)
      , maxRange(1)
      , accuracy(100)
    {
      // Make 1 the default range?
    }
    Weapon(int mI, int mD, int aR, int givenRange)
      : minDamage(mI)
      , maxDamage(mD)
      , armourPiercing(aR)
      , minRange(1)
      , maxRange(givenRange)
      , accuracy(100)
    {
    }
    Weapon(int mI, int mD, int aR, int givenMinR, int givenMaxR)
      : minDamage(mI)
      , maxDamage(mD)
      , armourPiercing(aR)
      , minRange(givenMinR)
      , maxRange(givenMaxR)
      , accuracy(100)
    {
    }
    virtual ~Weapon() { }

    /* Public accessors */
    int getMinDamage() const { return minDamage; }
    int getMaxDamage() const { return maxDamage; }
    int getAP() const { return armourPiercing; }
    int getMinRange() const { return minRange; }
    int getMaxRange() const { return maxRange; }

    /* Accuracy applies mainly to ranged Units...
     * Should we make a "class in the Middle"
     * (subclass of Unit, superclass of Archer) to represent this?
     * Probably. TODO.  */
    int getAccuracy() const {
      return accuracy;
    }
    void setAccuracy(int givenAcc) {
      accuracy = givenAcc;
    }

    /* A virtual function to be implemented uniquely in ALL subclasses...
     * can interpret it as "shooting" (for a bow), "swinging" (for a sword), etc.  */
    virtual bool fire() = 0;

    std::string toString() const {
      std::stringstream nameSetup/*= new std::stringstream()*/;
      nameSetup << "GENERIC-WEAPON, MAX-DMG" << maxDamage; /* arrows->getNumAvailable() << "/" << arrows->getMax() << ", ACC " << accuracy;*/
      nameSetup.flush();
      return nameSetup.str();
    }
  protected:
    int minDamage;
    int maxDamage;
    int armourPiercing;
    int minRange;
    int maxRange;
    int accuracy;
};

class Quiver {
  public:
    Quiver(int armourPiercing_, int cap)
      : armourPiercing(armourPiercing_)
      , max(cap)
      , available(cap)
    {
    }

    Quiver() 
      : armourPiercing(1)
      , max(12)
      , available(12)
    {
    }

    int replenish(int additional) {
      int newArrowCount = available+additional;
      if ((newArrowCount) > max) {
        available = max;
        return (newArrowCount-max);
      } else {
        available = newArrowCount;
        return 0;
      }
    }

    bool shoot() {
      if (available > 0) {
        available --;
        return true;
      } else {
        return false;
      }
    }

    int getNumAvailable() const {
      return available;
    }

    int getAP() const {
      return armourPiercing;
    }

    int getMax() const {
      return max;
    }
  
  private:
    int armourPiercing;
    int max;
    int available;
};

class Bow : public Weapon {
  public:
    // This tests whether the shot is successful or not
    // if the accuracy + any additons is less than that range, it will fail
    // it has a higher percentage chance of success the closer you are/more additions you have
    // after assessing if the arrow flies where it is suppossed to, it checks if the target dodges
    Bow(int acc, int minDamage, int maxDamage, Quiver *givenCache, int minRan, int maxRan)
        : Weapon(minDamage, maxDamage, givenCache->getAP(), minRan, maxRan)
        , arrows(givenCache)
    {
      setAccuracy(acc);
    }
    Bow(int acc, int minDamage, int maxDamage, Quiver *givenSet, int givenRange)
        : Weapon(minDamage, maxDamage, givenSet->getAP(), givenRange)
        , arrows(givenSet)
    {
      /* This "a" parameter used to be "mA"
       * ('modified attack' or perhaps 'modified accuracy'?)
       * (was this a mistsake, or do we need
       *  to modify the value of "a", before using it?)  */
      /*            arrows = new Quiver(arrow.getAP(), arrow.quiver);*/
      setAccuracy(acc);
    }
    Bow(int acc, int minDamage, int maxDamage, int aP, int capacity, int sRange)
        : Weapon(minDamage, maxDamage, aP, sRange)
        , arrows(new Quiver(aP, capacity))
    {
      setAccuracy(acc);
    }
    ~Bow() {
      /* Free memory that it owns...
       * Technically this could be done in the "Archer" unit subclass,
       * but I thought the design would work better if the FIRST / IMMEDIATE
       * OWNER (the Bow itself) frees it. */
      delete arrows;
    }

    Quiver *getQuiver() const {
      return arrows;
    }

    bool fire() {
      return arrows->shoot();
    }

    std::string toString() const {
      /*   */
      std::stringstream nameSetup/*= new std::stringstream()*/;
      nameSetup << "BOW, " << arrows->getNumAvailable() << "/" <<
        arrows->getMax() << ", ACC " << accuracy;
      nameSetup.flush();
      return nameSetup.str();
    }
  
  private:
    Quiver *arrows;
};

/* Sword class (as used by a Soldier)
 * This governs the attack and how effective it is */
class Sword : public Weapon {
  public:
    Sword(int mI,int mD, int aR) : Weapon(mI, mD, aR) {
      /* Add more assignments here if Swords need more attributes */
    }

    std::string toString() const {
      std::stringstream nameSetup;
      nameSetup << "SWORD " << minDamage << " to " << maxDamage;
      nameSetup.flush();
      return nameSetup.str();
    }

    /* TODO IF WE EVER DECIDE THAT A SWORD WEAPON HAS "DURABILITY"
     * then factor that into the equation here...
     * maybe reduce the durability / strength of the weapon and
     * return true as long as it's still positive.
     */
    bool fire() {
      return true;
    }

  
};

